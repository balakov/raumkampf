#include "client.h"
#include "network/messages.h"

Client::Client()
{
	rakPeer = RakNet::RakPeerInterface::GetInstance();
	replicaManager = new ClientReplicaManager();
	sd.port = 0;
	sd.socketFamily = AF_INET; // Only IPV4 supports broadcast on 255.255.255.255
	// Start RakNet, up to 32 connections if the server
	rakPeer->Startup(1, &sd, 1);
	rakPeer->AttachPlugin(replicaManager);
	rakPeer->AttachPlugin(&flt);
	rakPeer->SetSplitMessageProgressInterval(9);
	//flt.AddCallback(&testFileListProgress);

	replicaManager->SetNetworkIDManager(&networkIdManager);

	rakPeer->SetMaximumIncomingConnections(1);
}
Client::~Client()
{



}
bool Client::connect(RakNet::SystemAddress address)
{
	this->serverAddress = address;
	rakPeer->Connect(address.ToString(false), address.GetPort(), 0, 0, 0);
	return true;
}

void Client::askServerToShutdown()
{
	BOOST_LOG_TRIVIAL(info) << "Asking Server to shut down";
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_FORCE_STOP);
	rakPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, serverAddress, false);
}
void Client::pushShip(std::string definitionfile)
{

	BOOST_LOG_TRIVIAL(info) << "Offering Ship definition " << definitionfile;
	mPushFile = definitionfile;
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_REQ_SET_SHIP_DEFINITION);
	rakPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, serverAddress, false);
}

void Client::processPackets()
{
	RakNet::Packet *packet;
	for (packet = rakPeer->Receive(); packet; rakPeer->DeallocatePacket(packet), packet = rakPeer->Receive())
	{
		switch (packet->data[0])
		{
		case ID_CONNECTION_ATTEMPT_FAILED:
			printf("ID_CONNECTION_ATTEMPT_FAILED\n");

			break;
		case ID_CONNECTION_REQUEST_ACCEPTED:
			printf("ID_CONNECTION_REQUEST_ACCEPTED\n");
			break;
		case ID_DISCONNECTION_NOTIFICATION:
			printf("ID_DISCONNECTION_NOTIFICATION\n");
			break;
		case ID_CONNECTION_LOST:
			printf("ID_CONNECTION_LOST\n");
			break;
		case ID_ACK_SET_SHIP_DEFINITION:
		{
			// Print sending progress notifications

			unsigned short id;
			RakNet::BitStream bsIn(packet->data, packet->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			bsIn.Read(id);
			BOOST_LOG_TRIVIAL(info) << "Received id= " << id << " for ship definition transfer";

			RakNet::RakString file = mPushFile.c_str();
			unsigned int fileLength = GetFileLength(file);
			if (fileLength == 0)
			{
				BOOST_LOG_TRIVIAL(error) << "No definition exists in" << file.C_String();
			}
			else
			{
				BOOST_LOG_TRIVIAL(info) << "Sending " << file.C_String();
				fileList.AddFile(file.C_String(), file.C_String(), 0, fileLength, fileLength, FileListNodeContext(0, 0, 0, 0), true);
				flt.Send(&fileList, rakPeer, serverAddress, id, HIGH_PRIORITY, 0, &incrementalReadInterface, 2000000);
			}
			
		}break;
		case ID_CHAT_MSG:
		{

			RakNet::BitStream bsIn(packet->data, packet->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			RakString msg;
			bsIn.Read(msg);
			std::cout << "got chat msg: " << msg;
			//roomgui->addChatMessage(msg.C_String());

		}break;
		case ID_SERVER_STATE:
		{
			std::cout << "Received server state";
		}break;
		case ID_SND_RECEIPT_LOSS:
		case ID_SND_RECEIPT_ACKED:
		{
			uint32_t msgNumber;
			memcpy(&msgNumber, packet->data + 1, 4);

			DataStructures::List<Replica3*> replicaListOut;
			replicaManager->GetReplicasCreatedByMe(replicaListOut);
			unsigned int idx;
			for (idx = 0; idx < replicaListOut.Size(); idx++)
			{
				;//((TestEntity*)replicaListOut[idx])->NotifyReplicaOfMessageDeliveryStatus(packet->guid,msgNumber, packet->data[0]==ID_SND_RECEIPT_ACKED);
			}
		}
			break;
		}
	}


}