#pragma once
#include "StringTable.h"
#include "RakPeerInterface.h"

#include <stdio.h>
#include "Kbhit.h"
#include <string.h>
#include "BitStream.h"
#include "MessageIdentifiers.h"
#include "ReplicaManager3.h"
#include "NetworkIDManager.h"
#include "RakSleep.h"
#include "FormatString.h"
//#include "RakString.h"
#include "GetTime.h"
#include "SocketLayer.h"
#include "Getche.h"
#include "Rand.h"
#include "VariableDeltaSerializer.h"
#include "Gets.h"
#include "network/serverreplicamanager.h"
#include "map/servermap.h"
#include "serveruser.h"
#include "FileListTransferCBInterface.h"

using namespace RakNet;
class MultiPlayerServer
{
public:
	MultiPlayerServer(RakNetGUID serverAuthority);
	virtual ~MultiPlayerServer();

	void start();
	void loop();
	void doMultiPlayer();
	void doRooms();
	void processPackets();
	void deleteAllEntities();
	ServerMap map;
	bool lateJoin;
	enum STATE { ROOMS, MULTI, SINGLE, COOP, SHUTDOWN, EXIT };
	STATE state;
protected:
private:
	void localConsole();
	//ServerUser* getUser(RakNetGUID);
	//void addUser(RakNetGUID);
	//void removeUser(RakNetGUID);
	void forceStart();




	//std::vector<ServerUser*> connectedUsers;
	RakNetGUID serverAuthority;
	NetworkIDManager networkIdManager;
	ServerReplicaManager replicaManager;
	RakPeerInterface *rakPeer;
	ServerGame* _game;
};
