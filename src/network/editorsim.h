#pragma once
#include "server.h"
#include "Box2D\Box2D.h"
#include "network\shipTransferInterface.h"
class EditorSim: public Server{
public:
	EditorSim(RakNet::RakNetGUID serverAuthority);
	~EditorSim();
	virtual void start();
	virtual bool step();
	virtual void stop();
private:
	b2World* world;
	ShipTransferInterface transfer;

};