#pragma once
#include "RakPeerInterface.h"
#include "FileListTransfer.h"
#include "RakSleep.h"

#include "MessageIdentifiers.h"
#include "FileListTransferCBInterface.h"
#include "FileOperations.h"
#include "SuperFastHash.h"
#include "RakAssert.h"
#include "BitStream.h"
#include "IncrementalReadInterface.h"
#include "PacketizedTCP.h"
#include "SocketLayer.h"
#include <stdio.h>
#include "Gets.h"
#include <iostream>
#include <fstream>
#include <string>
#include "serveruser.h"


class ShipTransferInterface : public RakNet::FileListTransferCBInterface
{
private:
	bool mhasNew = false;
	ServerShip* ship;
public:
	bool hasNewShip(){ return mhasNew; }
	ServerShip* getShip(){ mhasNew = false;  return ship; }

	bool OnFile(OnFileStruct *onFileStruct)
	{
		BOOST_LOG_TRIVIAL(info) << "Incoming Ship definition";
		printf("OnFile: %i. (100%%) %i/%i %s %ib / %ib\n",
			onFileStruct->setID,
			onFileStruct->fileIndex + 1,
			onFileStruct->numberOfFilesInThisSet,
			onFileStruct->fileName,
			onFileStruct->byteLengthOfThisFile,
			onFileStruct->byteLengthOfThisSet);

		
		FILE *fp = fopen(onFileStruct->senderGuid.ToString(), "wb");		
		fwrite(onFileStruct->fileData, onFileStruct->byteLengthOfThisFile, 1, fp);
		fclose(fp);

		ship = new ServerShip;
		ship->loadFromFile(onFileStruct->senderGuid.ToString());
		ship->owner = onFileStruct->senderGuid;
		mhasNew = true;
		return true;
	}

	virtual void OnFileProgress(FileProgressStruct *fps)
	{
		BOOST_LOG_TRIVIAL(info) << "FileProgress";
		printf("OnFileProgress: %i partCount=%i partTotal=%i (%i%%) %i/%i %s %ib/%ib %ib/%ib total\n",
			fps->onFileStruct->setID,
			fps->partCount, fps->partTotal, (int)(100.0*(double)fps->onFileStruct->bytesDownloadedForThisFile / (double)fps->onFileStruct->byteLengthOfThisFile),
			fps->onFileStruct->fileIndex + 1,
			fps->onFileStruct->numberOfFilesInThisSet,
			fps->onFileStruct->fileName,
			fps->onFileStruct->bytesDownloadedForThisFile,
			fps->onFileStruct->byteLengthOfThisFile,
			fps->onFileStruct->bytesDownloadedForThisSet,
			fps->onFileStruct->byteLengthOfThisSet,
			fps->firstDataChunk);
	}

	virtual bool OnDownloadComplete(DownloadCompleteStruct *dcs)
	{
		BOOST_LOG_TRIVIAL(info) << "Download complete";

		// Returning false automatically deallocates the automatically allocated handler that was created by DirectoryDeltaTransfer
		return false;
	}

}; 