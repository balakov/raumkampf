#include "network/multiplayerserver.h"
#include <FileListTransfer.h>
#include "PacketizedTCP.h"
#include "network/messages.h"
#include "util.h"
#include <iostream>
#include <boost/log/trivial.hpp>
#include "network/TestCB.h"
#include "entity/serverprojectile.h"
#include "servergame.h"
#include "settings.h"
#include <boost/log/trivial.hpp>

#define SERVER_PORT 12345
RakNet::FileListTransfer flt;

MultiPlayerServer::MultiPlayerServer(RakNetGUID serverAuthority)
{
	lateJoin = true;
	this->state = ROOMS;
	this->serverAuthority = serverAuthority;
	rakPeer = RakNet::RakPeerInterface::GetInstance();
	RakNet::SocketDescriptor sd;
	sd.socketFamily = AF_INET; // Only IPV4 supports broadcast on 255.255.255.255
	sd.port = SERVER_PORT;
	BOOST_LOG_TRIVIAL(info) << "Starting Server on port " << SERVER_PORT;
	//rakPeer->ApplyNetworkSimulator(0,200,5);
	rakPeer->Startup(32, &sd, 1);
	rakPeer->AttachPlugin(&replicaManager);
	rakPeer->AttachPlugin(&flt);
	rakPeer->SetSplitMessageProgressInterval(9);
	replicaManager.SetNetworkIDManager(&networkIdManager);
	replicaManager.SetAutoSerializeInterval(10);
	rakPeer->SetMaximumIncomingConnections(32);
	BOOST_LOG_TRIVIAL(info) << "Server GUID is " << rakPeer->GetMyGUID().ToString();
	BOOST_LOG_TRIVIAL(info) << "Server authority GUID is " << serverAuthority.ToString();

	_game = new ServerGame(); // must be instantiated AFTER network interface has started
	replicaManager.Reference(_game);


	map.preparse(Settings::getDataFolderBasePath() + "map/defaultmap/defaultmap.xml");
	map.loadTerrainMonolithic();

}

MultiPlayerServer::~MultiPlayerServer()
{
	BOOST_LOG_TRIVIAL(info) << "Shutting down...";
	rakPeer->Shutdown(100, 0);
	RakNet::RakPeerInterface::DestroyInstance(rakPeer);
}

void MultiPlayerServer::start()
{
	while (state != SHUTDOWN)
	{
		RakSleep(30);
		localConsole();

		processPackets();

		if (state == ROOMS)
		{
			doRooms();
		}
		if (state == MULTI)
		{
			doMultiPlayer();
		}
	}

}
void MultiPlayerServer::loop()
{

	RakSleep(30);
	localConsole();
	processPackets();

	if (state == ROOMS)
	{
		doRooms();
	}
	if (state == MULTI)
	{
		doMultiPlayer();
	}


}
void MultiPlayerServer::doRooms()
{
	;;

}
void MultiPlayerServer::processPackets(void)
{
	RakNet::Packet *packet;
	for (packet = rakPeer->Receive(); packet; rakPeer->DeallocatePacket(packet), packet = rakPeer->Receive())
	{
		// general packet processing
		switch (packet->data[0])
		{
		case ID_CONNECTION_ATTEMPT_FAILED:
			printf("ID_CONNECTION_ATTEMPT_FAILED\n");
			//quit=true;
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			printf("ID_NO_FREE_INCOMING_CONNECTIONS\n");
			//quit=true;
			break;
		case ID_NEW_INCOMING_CONNECTION:
			//addUser(packet->guid);
			break;
		case ID_DISCONNECTION_NOTIFICATION:
			printf("ID_DISCONNECTION_NOTIFICATION\n");
			break;
		case ID_CONNECTION_LOST:
			printf("ID_CONNECTION_LOST\n");
			break;

		case ID_ADVERTISE_SYSTEM:
			// The first conditional is needed because ID_ADVERTISE_SYSTEM may be from a system we are connected to, but replying on a different address.
			// The second conditional is because AdvertiseSystem also sends to the loopback
			if (rakPeer->GetSystemAddressFromGuid(packet->guid) == RakNet::UNASSIGNED_SYSTEM_ADDRESS &&
				rakPeer->GetMyGUID() != packet->guid)
			{
				printf("Connecting to %s\n", packet->systemAddress.ToString(true));
				rakPeer->Connect(packet->systemAddress.ToString(false), packet->systemAddress.GetPort(), 0, 0);
			}
			break;
		case ID_SND_RECEIPT_LOSS:
		case ID_SND_RECEIPT_ACKED:
		{
			uint32_t msgNumber;
			memcpy(&msgNumber, packet->data + 1, 4);

			DataStructures::List<Replica3*> replicaListOut;
			replicaManager.GetReplicasCreatedByMe(replicaListOut);
			unsigned int idx;
			for (idx = 0; idx < replicaListOut.Size(); idx++)
			{
				//						[idx]->NotifyReplicaOfMessageDeliveryStatus(packet->guid,msgNumber, packet->data[0]==ID_SND_RECEIPT_ACKED);
			}
		}
			break;

		case ID_GET_SERVER_STATE:
		{
			BOOST_LOG_TRIVIAL(info) << "Sending Server state";
			RakNet::BitStream bsOut;
			bsOut.Write((RakNet::MessageID)ID_SERVER_STATE);
			//bsOut.Write(id);
			rakPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
		}break;

		case ID_CHAT_MSG:
		{
			RakString msg;
			RakNet::BitStream bsIn(packet->data, packet->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			bsIn.Read(msg);
			BOOST_LOG_TRIVIAL(info) << "got chat msg" << msg;
			RakNet::BitStream bsOut;
			bsOut.Write((RakNet::MessageID)ID_CHAT_MSG);
			RakString bcmsg(packet->systemAddress.ToString());
			bcmsg += ": ";
			bcmsg += msg;
			bsOut.Write(bcmsg);
			rakPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
			//_game->setMap(msg.C_String());
		}break;
		case ID_FORCE_STOP:
		{
			if (packet->guid == this->serverAuthority)
				state = SHUTDOWN;
			else BOOST_LOG_TRIVIAL(info) << "Received shutdown command from unauthorized user";

		}break;
		}

		// process these only when in ROOMS mode
		if (state == ROOMS)
		{

			switch (packet->data[0])
			{

			case ID_SET_PLAYER_NAME:
				printf("ID_CONNECTION_LOST\n");
				break;
			case ID_FORCE_START:
			{
				if (packet->guid == serverAuthority)
				{
					forceStart();
				}
				else std::cout << "You (" << packet->guid.ToString() << ")are not server authority (" << serverAuthority.ToString() << "), imposter";
			}break;


			case ID_REQ_SET_SHIP_DEFINITION:
			{

				if (_game->hasUser(packet->guid))
				{
					TestCB* cb = new TestCB();
					ServerUser* usr = (ServerUser*)_game->getUserByGUID(packet->guid);

					if (usr->hasShip)
					{
						usr->hasShip = false;
						usr->ship->BroadcastDestruction();
						delete usr->ship;
					}
					cb->setUser(usr);
					unsigned short id = flt.SetupReceive(cb, false, packet->systemAddress);
					BOOST_LOG_TRIVIAL(info) << "Expecting Ship definition, File Transfer ID is " << id;
					RakNet::BitStream bsOut;
					bsOut.Write((RakNet::MessageID)ID_ACK_SET_SHIP_DEFINITION);
					bsOut.Write(id);
					rakPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
				}
				else BOOST_LOG_TRIVIAL(warning) << "Received REQ_SET_SHIP_DEFINITION from non-existant user";
			}break;


			case ID_JOIN_ROOM_SLOT:
			{
				RakNet::BitStream bsIn(packet->data, packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				int slotNumber;
				bsIn.Read(slotNumber);
				BOOST_LOG_TRIVIAL(info) << "User #" << packet->guid.ToString() << " wants to join slot " << slotNumber;
				//getUser(packet->guid)
				_game->join(slotNumber, packet->guid);

			}break;

			default: BOOST_LOG_TRIVIAL(warning) << "Unknown message: " << (int)packet->data[0] << " from " << packet->guid.ToString();
				break;
			}
		}



		// process these only when in multiplayer mode
		if (state == MULTI)
		{
			switch (packet->data[0])
			{
			case ID_DEVICE_STATE:
			{
				BOOST_LOG_TRIVIAL(trace) << "Receiving device state";
				std::vector<bool> state;
				int pSize = 0;
				RakNet::BitStream bsIn(packet->data, packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(pSize);

				ServerUser* usr = (ServerUser*)_game->getUserByGUID(packet->guid);
				if (usr->hasShip)
				{
					for (int idx = 0; idx < pSize; idx++)
					{
						bool state = bsIn.ReadBit();
						if (usr->ship->getHull()->getDevice(idx) != nullptr)
						{
							//std::cout << "slot"<<idx<<": " << bsIn.ReadBit();
							usr->ship->getHull()->setDeviceState(idx, state);
						}
						//else BOOST_LOG_TRIVIAL(warning) <<  "Device state for empty slot arrived";
					}
				}
				else BOOST_LOG_TRIVIAL(warning) << "User without ship sends device message";
			}
				break;
			}
		}
	}
}
void MultiPlayerServer::doMultiPlayer(void)
{

	for (int i = 0; i < 4; i++)
	{
		if (rakPeer->GetInternalID(RakNet::UNASSIGNED_SYSTEM_ADDRESS, 0).GetPort() != SERVER_PORT + i)
			rakPeer->AdvertiseSystem("255.255.255.255", SERVER_PORT + i, 0, 0, 0);
	}


	for (int idx = 0; idx < replicaManager.GetReplicaCount(); idx++)
	{

		NetworkEntity* e = (NetworkEntity*)replicaManager.GetReplicaAtIndex(idx);

		if (e->getIdentifier() == "Projectile")
		{
			ServerProjectile* tmp = ((ServerProjectile*)replicaManager.GetReplicaAtIndex(idx));
			if (tmp->markForDeletion)
			{
				std::cout << "\ntime to delete\n";
				map.removeDynamicBody(tmp);
				tmp->BroadcastDestruction();
				delete tmp;

			}
		}
		else if (e->getIdentifier() == "Ship")
		{
			ServerShip* ship = ((ServerShip*)replicaManager.GetReplicaAtIndex(idx));
			//std::cout << "Num srcs" << ship->getHull()->getPowerSources().size();
			if (ship->markForRespawn)
			{
				BOOST_LOG_TRIVIAL(info) << "Sending EXPLOSION event";
				RakNet::BitStream bsOut;
				bsOut.Write((RakNet::MessageID)ID_EXPLOSION);
				bsOut.Write(ship->getHull()->getPosition());

				rakPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, UNASSIGNED_SYSTEM_ADDRESS, true);
				ServerUser* usr = (ServerUser*)_game->getUserByGUID(ship->getOwnerOfKillingBlow());
				if (usr != 0)
				{
					usr->kills++;
					std::cout << "\nkilling blow: " << ship->getOwnerOfKillingBlow().ToString();
					std::cout << usr->guid.ToString();
					ServerUser* dusr = (ServerUser*)_game->getUserByGUID(ship->owner);
					dusr->shotdown++;

				}
				else {
					_game->getUserByGUID(ship->owner)->crashed++;
				}

				int slot = this->_game->getUserByGUID(ship->owner)->gameSlot;
				sf::Vector2f s(map.getSpawnPosition(slot).x, map.getSpawnPosition(slot).y);
				ship->getBody()->SetTransform(Util::toB2(s), -90);
				ship->getBody()->SetLinearVelocity(b2Vec2_zero);
				ship->getHull()->reset();
				ship->markForRespawn = false;
			}
		}
		else if (e->getIdentifier() == "User")
		{
			;;//((Ship*)replicaManager.GetReplicaAtIndex(idx))
		}
		else if (e->getIdentifier() == "Game")
		{
			;;//((Ship*)replicaManager.GetReplicaAtIndex(idx))
		}
		else BOOST_LOG_TRIVIAL(error) << "Unknown Replica in List: " << e->getIdentifier();


	}


	for (int i = 0; i < _game->getUsers().size(); i++)
	{
		ServerUser* temp = (ServerUser*)_game->getUsers()[i];
		if (temp->hasShip)
		{
			if (temp->ship->getHull()->hasEntities())
			{
				for (int n = 0; n < temp->ship->getHull()->getEntities().size(); n++)
				{
					PhysicalEntity* pe = temp->ship->getHull()->getEntities()[n];
					pe->owner = temp->guid; // keep this for ownership/collision checks							
					map.addDynamicBody(pe);
					replicaManager.Reference(pe);
				}
				temp->ship->clearEntities();
			}
			temp->ship->updatePhysics(1.0f / 60.0f);

		}
	}
	map.update();
}

void MultiPlayerServer::deleteAllEntities()
{
	std::cout << "deleting all" << std::endl;
	DataStructures::List<Replica3*> replicaListOut;
	// The reason for ClearPointers is that , 
	// I don't track which objects have and have not been allocated at the application level.
	// So ClearPointers will call delete on every object in the returned list,
	// which is every object that the application has created. Another way to put it is
	// 	A. Send a packet to tell other systems to delete these objects
	// 	B. Delete these objects on my own system
	replicaManager.GetReplicasCreatedByMe(replicaListOut);
	replicaManager.BroadcastDestructionList(replicaListOut, RakNet::UNASSIGNED_SYSTEM_ADDRESS);
	for (unsigned int i = 0; i < replicaListOut.Size(); i++)
	{
		map.removeDynamicBody((PhysicalEntity*)replicaListOut[i]);
		RakNet::OP_DELETE(replicaListOut[i], _FILE_AND_LINE_);
	}
}
void MultiPlayerServer::localConsole()
{
	if (kbhit())
	{
		char ch;
		ch = getch();
		if (ch == 'i' || ch == 'I')
		{
			std::cout << "\nNumber of Users: " << this->_game->getUsers().size();
			std::cout << "\nNumber Of Objects: " << replicaManager.GetReplicaCount() << std::endl;
			for (int i = 0; i < this->_game->getUsers().size(); i++)
			{
				this->_game->getUsers()[i]->print();
				//std::cout << "\nping to user #" << i << ": " << rakPeer->GetClockDifferential(users[i]->guid) << "ms";
			}
		}
		if (ch == 'f' || ch == 'F')
		{
			forceStart();
		}
		if (ch == 'd' || ch == 'D')
		{
			this->_game->setMap("test");
		}

		if (ch == 't' || ch == 'T')
		{
			ServerProjectile* p = new ServerProjectile(sf::Vector2f(10, 10), sf::Vector2f(0, 0));
			map.addDynamicBody(p);
			replicaManager.Reference(p);
		}
	}
}

void MultiPlayerServer::forceStart()
{

	BOOST_LOG_TRIVIAL(info) << "Forcing start ";
	state = MULTI;
	for (int i = 0; i < _game->getUsers().size(); i++)
	{
		BOOST_LOG_TRIVIAL(info) << "User " << i << "/" << _game->getUsers().size();
		if (_game->getUsers()[i]->hasShip)
		{
			ServerUser* usr = (ServerUser*)_game->getUsers()[i];
			map.addShip(usr->ship);
			int slot = _game->getUsers()[i]->gameSlot;
			sf::Vector2f s(map.getSpawnPosition(slot).x, map.getSpawnPosition(slot).y);
			usr->ship->getBody()->SetTransform(Util::toB2(s), Util::degToRad(-90));

			replicaManager.Reference(usr->ship);
			BOOST_LOG_TRIVIAL(info) << "creating ship";
		}
		BOOST_LOG_TRIVIAL(warning) << "\nuser without ship (observer?)";
	}

}
