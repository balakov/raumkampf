#include "editorsim.h"
#include "network/messages.h"
#include "network/TestCB.h"
#include "globals.h"

EditorSim::EditorSim(RakNet::RakNetGUID serverAuthority)
{
	BOOST_LOG_TRIVIAL(info) << "Starting Simulation Server";
	BOOST_LOG_TRIVIAL(info) << "Server authority GUID is " << serverAuthority.ToString();

	this->serverAuthority = serverAuthority;
	rakPeer = RakNet::RakPeerInterface::GetInstance();
	RakNet::SocketDescriptor sd;
	sd.socketFamily = AF_INET; // Only IPV4 supports broadcast on 255.255.255.255
	sd.port = 7777;
	rakPeer->Startup(32, &sd, 1);
	rakPeer->AttachPlugin(&replicaManager);	
	rakPeer->AttachPlugin(&flt);
	rakPeer->SetSplitMessageProgressInterval(9);
	replicaManager.SetNetworkIDManager(&networkIdManager);
	replicaManager.SetAutoSerializeInterval(10);
	rakPeer->SetMaximumIncomingConnections(10);
	BOOST_LOG_TRIVIAL(info) << "Server GUID is " << rakPeer->GetMyGUID().ToString();
	world = new b2World(b2Vec2(0, 0));

}
EditorSim::~EditorSim()
{



}
void EditorSim::start()
{



}

bool EditorSim::step()
{

	RakNet::Packet *packet;
	for (packet = rakPeer->Receive(); packet; rakPeer->DeallocatePacket(packet), packet = rakPeer->Receive())
	{

		// general packet processing
		switch (packet->data[0])
		{
		case ID_CONNECTION_ATTEMPT_FAILED:
			printf("ID_CONNECTION_ATTEMPT_FAILED\n");
			//quit=true;
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			printf("ID_NO_FREE_INCOMING_CONNECTIONS\n");
			//quit=true;
			break;
		case ID_NEW_INCOMING_CONNECTION:
			game.addUser(new ServerUser(packet->guid));
			game.join(0,packet->guid);
			//addUser(packet->guid);
			break;
		case ID_DISCONNECTION_NOTIFICATION:
			printf("ID_DISCONNECTION_NOTIFICATION\n");
			break;
		case ID_CONNECTION_LOST:
			printf("ID_CONNECTION_LOST\n");
			break;
		case ID_SND_RECEIPT_LOSS:
		case ID_SND_RECEIPT_ACKED:
		{
			uint32_t msgNumber;
			memcpy(&msgNumber, packet->data + 1, 4);

			DataStructures::List<Replica3*> replicaListOut;
			replicaManager.GetReplicasCreatedByMe(replicaListOut);
			unsigned int idx;
			for (idx = 0; idx < replicaListOut.Size(); idx++)
			{
				//						[idx]->NotifyReplicaOfMessageDeliveryStatus(packet->guid,msgNumber, packet->data[0]==ID_SND_RECEIPT_ACKED);
			}
		}
			break;

		case ID_GET_SERVER_STATE:
		{
			BOOST_LOG_TRIVIAL(info) << "Sending Server state";
			RakNet::BitStream bsOut;
			bsOut.Write((RakNet::MessageID)ID_SERVER_STATE);
			//bsOut.Write(id);
			rakPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
		}break;

		case ID_FORCE_STOP:
		{
			if (packet->guid == this->serverAuthority)
				return false;
			else BOOST_LOG_TRIVIAL(info) << "Received shutdown command from unauthorized user";

		}break;

		case ID_REQ_SET_SHIP_DEFINITION:
		{
			BOOST_LOG_TRIVIAL(info) << "Received request ro send ship definition";
			unsigned short id = flt.SetupReceive(&transfer, false, packet->systemAddress);
			BOOST_LOG_TRIVIAL(info) << "Expecting Ship definition, File Transfer ID is " << id;
			RakNet::BitStream bsOut;
			bsOut.Write((RakNet::MessageID)ID_ACK_SET_SHIP_DEFINITION);
			bsOut.Write(id);
			rakPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);

		}break;
		case ID_ADDDEVICE:
		{
			RakNet::BitStream bsIn(packet->data, packet->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			int slot = -1; bsIn.Read(slot);
			RakNet::RakString identifier = "transfer error";
			bsIn.Read(identifier);
			BOOST_LOG_TRIVIAL(info) << "Received request to add device " << identifier << " to slot " << slot;
			if (!this->game.hasUser(packet->guid))
			{
				BOOST_LOG_TRIVIAL(warning) << "User does not exist";
			}
			else {
				ServerUser* user = this->game.getUserByGUID(packet->guid);
				if (user->hasShip)
				{
					std::string str = identifier.C_String();
					Device* d = DeviceFactory::getByName(str);
					user->ship->getHull()->setDevice(d, slot);
					user->ship->getHull()->needsToSendDeviceUpdate = true;
				}
				else BOOST_LOG_TRIVIAL(warning) << "User has no ship";
			}
		}break;

		case ID_REMOVEDEVICE:
		{
			RakNet::BitStream bsIn(packet->data, packet->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			int slot = -1; bsIn.Read(slot);
			BOOST_LOG_TRIVIAL(info) << "Received request to remove device from slot " << slot;
			if (!this->game.hasUser(packet->guid))
			{
				BOOST_LOG_TRIVIAL(warning) << "User does not exist";
			}
			else {
				ServerUser* user = this->game.getUserByGUID(packet->guid);
				if (user->hasShip)
				{
					user->ship->getHull()->removeDevice(slot);
					user->ship->getHull()->needsToSendDeviceUpdate = true;
				}
				else BOOST_LOG_TRIVIAL(warning) << "User has no ship";
			}
		}break;
		case ID_RECONFIGUREDEVICE:
		{
			RakNet::BitStream bsIn(packet->data, packet->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			int slot = -1; bsIn.Read(slot);
			BOOST_LOG_TRIVIAL(info) << "Received request to configure device in slot " << slot;
			if (!this->game.hasUser(packet->guid))
			{
				BOOST_LOG_TRIVIAL(warning) << "User does not exist";
			}
			else {
				ServerUser* user = this->game.getUserByGUID(packet->guid);
				if (user->hasShip)
				{
					Device* d = user->ship->getHull()->getDevice(slot);
					d->readConfigFromBitstream(&bsIn);
				}
				else BOOST_LOG_TRIVIAL(warning) << "User has no ship";
			}
		}break;
		case ID_REMOVE_ALL_DEVICES:
		{
			RakNet::BitStream bsIn(packet->data, packet->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			BOOST_LOG_TRIVIAL(info) << "Received request to clear all devices";
			if (!this->game.hasUser(packet->guid))
			{
				BOOST_LOG_TRIVIAL(warning) << "User does not exist";
			}
			else {
				ServerUser* user = this->game.getUserByGUID(packet->guid);
				if (user->hasShip)
				{
					user->ship->getHull()->removeAllDevices();
					user->ship->getHull()->needsToSendDeviceUpdate = true;
				}
				else BOOST_LOG_TRIVIAL(warning) << "User has no ship";
			}
		}break;
		case ID_DEVICE_STATE:
		{
			BOOST_LOG_TRIVIAL(trace) << "Receiving device state";
			std::vector<bool> state;
			int pSize = 0;
			RakNet::BitStream bsIn(packet->data, packet->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			bsIn.Read(pSize);

			if (game.hasUser(packet->guid))
			{
				ServerUser* user = game.getUserByGUID(packet->guid);
				if (user->hasShip)
				{
					ServerShip* ship = user->ship;
					for (int idx = 0; idx < pSize; idx++)
					{
						bool state = bsIn.ReadBit();
						if (ship->getHull()->getDevice(idx) != nullptr)
						{
							ship->getHull()->setDeviceState(idx, state);
						}
						else BOOST_LOG_TRIVIAL(warning) << "Device state for empty slot arrived";
					}
				}
				else BOOST_LOG_TRIVIAL(warning) << "User without ship sends device message";
			}

		}break;


		default: BOOST_LOG_TRIVIAL(warning) << "Unknown message: " << (int)packet->data[0] << " from " << packet->guid.ToString();
			break;

		}

	}		RakSleep(50);

	if (transfer.hasNewShip())
	{
		BOOST_LOG_TRIVIAL(info) << "Transfer interface has a new Ship";
		ServerShip* newShip = transfer.getShip();
		
		if (game.hasUser(newShip->owner))
		{
			ServerUser* user = game.getUserByGUID(newShip->owner);
			if (user->hasShip)
			{
				BOOST_LOG_TRIVIAL(info) << user->ship->getBody()->GetAngle();
				BOOST_LOG_TRIVIAL(info) << "Removing old ship ";
				world->DestroyBody(user->ship->getBody());
				user->ship->BroadcastDestruction();
			}
			user->ship = newShip;
			replicaManager.Reference(newShip);
			BOOST_LOG_TRIVIAL(info) << "Map adding dynamic body ";
			b2Body* body = world->CreateBody(&(newShip->getBodyDefinition()));
			body->CreateFixture(&(newShip->getFixtureDefinition()));
			//body->SetUserData(newShip);
			body->SetTransform(b2Vec2(10, 10), Util::degToRad(-90));
			newShip->setBody(body);
			user->hasShip = true;
			
		}
		//else BOOST_LOG_TRIVIAL(warning) << "Ship from unknown User has arrived";

		//if (user->hasShip)
		//	replicaManager.Dereference(user->ship);




	}

	std::vector<ServerUser*> users = game.getUsers();
	for (ServerUser* user : users)
	{
		if (user->hasShip)
			user->ship->updatePhysics(10);
	}
	world->Step(1/60.0f,3,3);

		return true;
}
void EditorSim::stop()
{
	rakPeer->Shutdown(100);
}

