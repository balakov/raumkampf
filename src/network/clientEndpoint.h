#pragma once

#include "StringTable.h"
#include "RakPeerInterface.h"

#include <stdio.h>
#include "Kbhit.h"
#include <string.h>
#include "BitStream.h"
#include "MessageIdentifiers.h"
#include "ReplicaManager3.h"
#include "NetworkIDManager.h"
#include "RakSleep.h"
#include "FormatString.h"
#include "RakString.h"
#include "GetTime.h"
#include "SocketLayer.h"
#include "Getche.h"
#include "Rand.h"
#include "VariableDeltaSerializer.h"
#include "Gets.h"

#include "entity/clientprojectile.h"

#include "clientuser.h"
#include "clientgame.h"
#include "ship/clientship.h"
using namespace RakNet;

class ClientEndpoint : public Connection_RM3 {
private:

public:
	ClientEndpoint(const SystemAddress &_systemAddress, RakNetGUID _guid) : Connection_RM3(_systemAddress, _guid) { }
	virtual ~ClientEndpoint() {}

	// See documentation - Makes all messages between ID_REPLICA_MANAGER_DOWNLOAD_STARTED and ID_REPLICA_MANAGER_DOWNLOAD_COMPLETE arrive in one tick
	bool QueryGroupDownloadMessages(void) const {return true;}

	virtual Replica3 *AllocReplica(RakNet::BitStream *allocationId, ReplicaManager3 *replicaManager3)
	{
		RakNet::RakString typeName;
		allocationId->Read(typeName);

		if (typeName == "Ship")
		{
			ClientShip* ship = new ClientShip;
			return ship;
		}
		else if (typeName=="Projectile") return new ClientProjectile;
		else if (typeName == "User")
		{
			ClientUser* user = new ClientUser;
			return user;
		}
		else if (typeName == "Game") {
			std::cout << "allocating client game \n\n" << std::endl;
			return ClientGame::getInstance();
		}
		else std::cout << "Cannot allocate: " << typeName << std::endl;
		return 0;
	}
protected:


};