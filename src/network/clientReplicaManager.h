#pragma once

#include "ReplicaManager3.h"
#include "clientEndpoint.h"
#include "clientgame.h"
#include <boost/log/trivial.hpp>
using namespace RakNet;
class ClientReplicaManager : public ReplicaManager3
{

public:

	virtual Connection_RM3* AllocConnection(const SystemAddress &systemAddress, RakNetGUID rakNetGUID) const {	
		return new ClientEndpoint(systemAddress, rakNetGUID);
	}
	virtual void DeallocConnection(Connection_RM3 *connection) const {
		delete connection;
	}

};