#pragma once
#include "RakNetTypes.h"
#include "RakPeerInterface.h"
#include "FileListTransfer.h"
#include "network/serverreplicamanager.h"
#include "container/servergame.h"
class Server
{
    public:
		Server();
		~Server();

		virtual void start()=0;
		virtual bool step()=0;
		virtual void stop()=0;

    protected:
		RakNetGUID serverAuthority;
		NetworkIDManager networkIdManager;
		ServerReplicaManager replicaManager;
		RakPeerInterface *rakPeer;
		RakNet::FileListTransfer flt;
		
		ServerGame game;
    private:
		

};
