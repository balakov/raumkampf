#pragma once

#include "StringTable.h"
#include "RakPeerInterface.h"

#include <stdio.h>
#include "Kbhit.h"
#include <string.h>
#include "BitStream.h"
#include "MessageIdentifiers.h"
#include "ReplicaManager3.h"
#include "NetworkIDManager.h"
#include "RakSleep.h"
#include "FormatString.h"
#include "RakString.h"
#include "GetTime.h"
#include "SocketLayer.h"
#include "Getche.h"
#include "Rand.h"
#include "VariableDeltaSerializer.h"
#include "Gets.h"

#include <iostream>
#include "ship/servership.h"
#include "serveruser.h"
#include "servergame.h"
using namespace RakNet;

class ServerEndpoint : public Connection_RM3 {
public:
	RakNet::RakString name;

	ServerEndpoint(const SystemAddress &_systemAddress, RakNetGUID _guid) : Connection_RM3(_systemAddress, _guid)
	{
	}
	virtual ~ServerEndpoint() {}

	// See documentation - Makes all messages between ID_REPLICA_MANAGER_DOWNLOAD_STARTED and ID_REPLICA_MANAGER_DOWNLOAD_COMPLETE arrive in one tick
	bool QueryGroupDownloadMessages(void) const {return true;}

	virtual Replica3 *AllocReplica(RakNet::BitStream *allocationId, ReplicaManager3 *replicaManager3)
	{
		/*RakNet::RakString typeName;
		allocationId->Read(typeName);

		if (typeName=="Ship") return new ServerShip;
		if (typeName=="User") return new ServerUser;
		if (typeName=="Game") return new ServerGame;
		else std::cout << "Cannot allocate: " << typeName << std::endl;*/
		BOOST_LOG_TRIVIAL(warning) << "Should never be called";
		return 0;
	}
protected:
};