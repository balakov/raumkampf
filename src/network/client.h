#pragma once
#include "FileListTransfer.h"
#include "FileOperations.h"
#include "IncrementalReadInterface.h"
#include "ReplicaManager3.h"
#include "NetworkIDManager.h"
#include "network/clientReplicaManager.h"
#include "SocketLayer.h"
#include "network/fileProgress.h"

class Client{

public:
	Client();
	~Client();
protected:
	bool connect(RakNet::SystemAddress address);
	void processPackets();
	void pushShip(std::string definitionFile);
	void askServerToShutdown();
	RakNet::IncrementalReadInterface incrementalReadInterface;
	RakNet::FileListTransfer flt;
	RakNet::FileList fileList;

	NetworkIDManager networkIdManager;
	RakNet::RakPeerInterface *rakPeer;
	ClientReplicaManager* replicaManager;
	SystemAddress serverAddress;

	RakNet::SocketDescriptor sd;
	FileProgress fileListProgress;

	std::string mPushFile;
private:




};