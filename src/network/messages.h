#pragma once

#include "BitStream.h"
#include "RakNetTypes.h"  // MessageID
#include "MessageIdentifiers.h"

using namespace RakNet;

enum ClientMessage
{
	ID_DEVICE_STATE=ID_USER_PACKET_ENUM+1,	// sent by client to update state of his ships devices
	ID_SET_PLAYER_NAME,						// sent by client to identify himself
	ID_REQ_SET_SHIP_DEFINITION,				// sent by client to indicate it wants to sent the ship def file
	ID_ACK_SET_SHIP_DEFINITION,				// sent by server to indicate file transfer can start
	ID_FORCE_START,							//* sent by server_authority to force start game
	ID_SETUP_GAME,							//* sent by server_authority to set game mode
	ID_GOGOGO,								// sent by server to indicate game start
	ID_FORCE_STOP,							//* sent by server_authority to force server stop
	ID_SERVER_SHUTDOWN,						// sent by server to indicate it is shutting down
	ID_GAME_END,							// sent by server to indicate game ends
	ID_SERVER_STATE,
	ID_GET_SERVER_STATE,
	ID_CHAT_MSG,
	ID_JOIN_ROOM_SLOT,
	ID_SET_MAP,
	ID_ADDDEVICE,
	ID_REMOVEDEVICE,
	ID_REMOVE_ALL_DEVICES,
	ID_RECONFIGUREDEVICE,
	ID_EXPLOSION
};