#pragma once

#include "ReplicaManager3.h"
#include "NetworkIDManager.h"
#include "RakString.h"

using namespace RakNet;

struct NetworkEntity:public Replica3
{
	virtual RakNet::RakString getIdentifier(void) const {return "networkentity";}
	RakNet::RakNetGUID owner;
};