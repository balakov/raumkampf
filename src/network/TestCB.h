#pragma once
#include "RakPeerInterface.h"
#include "FileListTransfer.h"
#include "RakSleep.h"

#include "MessageIdentifiers.h"
#include "FileListTransferCBInterface.h"
#include "FileOperations.h"
#include "SuperFastHash.h"
#include "RakAssert.h"
#include "BitStream.h"
#include "IncrementalReadInterface.h"
#include "PacketizedTCP.h"
#include "SocketLayer.h"
#include <stdio.h>
#include "Gets.h"
#include <iostream>
#include <fstream>
#include <string>
#include "serveruser.h"


class TestCB : public RakNet::FileListTransferCBInterface
{
public:
	
	RakNet::RakString file;
	RakNet::RakString fileCopy;

	ServerUser* userHandle;
	void setUser(ServerUser* user){userHandle = user;}
	bool OnFile(
		OnFileStruct *onFileStruct)
	{
		std::cout << "incoming ship for guid: " << userHandle->guid.ToString();
		printf("OnFile: %i. (100%%) %i/%i %s %ib / %ib\n",
			onFileStruct->setID,
			onFileStruct->fileIndex+1,
			onFileStruct->numberOfFilesInThisSet,
			onFileStruct->fileName,
			onFileStruct->byteLengthOfThisFile,
			onFileStruct->byteLengthOfThisSet);


		FILE *fp = fopen("test.txt", "wb");
		fwrite(onFileStruct->fileData, onFileStruct->byteLengthOfThisFile, 1, fp);
		fclose(fp);	
		userHandle->ship = new ServerShip;
		userHandle->ship->loadFromFile("test.txt");
		userHandle->hasShip=true;
		userHandle->ship->owner=userHandle->guid;
		userHandle->replicaManager->Reference(userHandle->ship);
		// Make sure it worked
		/*unsigned int hash1 = SuperFastHashFile(file.C_String());
		if (RakNet::BitStream::DoEndianSwap())
			RakNet::BitStream::ReverseBytesInPlace((unsigned char*) &hash1, sizeof(hash1));
		unsigned int hash2 = SuperFastHashFile(fileCopy.C_String());
		if (RakNet::BitStream::DoEndianSwap())
			RakNet::BitStream::ReverseBytesInPlace((unsigned char*) &hash2, sizeof(hash2));
		RakAssert(hash1==hash2);
		*/
		
//		

		// Return true to have RakNet delete the memory allocated to hold this file.
		// False if you hold onto the memory, and plan to delete it yourself later

		return true;
	}
	
	virtual void OnFileProgress(FileProgressStruct *fps)
	{
		printf("OnFileProgress: %i partCount=%i partTotal=%i (%i%%) %i/%i %s %ib/%ib %ib/%ib total\n",
			fps->onFileStruct->setID,
			fps->partCount, fps->partTotal, (int) (100.0*(double)fps->onFileStruct->bytesDownloadedForThisFile/(double)fps->onFileStruct->byteLengthOfThisFile),
			fps->onFileStruct->fileIndex+1,
			fps->onFileStruct->numberOfFilesInThisSet,
			fps->onFileStruct->fileName,
			fps->onFileStruct->bytesDownloadedForThisFile,
			fps->onFileStruct->byteLengthOfThisFile,
			fps->onFileStruct->bytesDownloadedForThisSet,
			fps->onFileStruct->byteLengthOfThisSet,
			fps->firstDataChunk);
	}

	virtual bool OnDownloadComplete(DownloadCompleteStruct *dcs)
	{
		printf("Download complete.\n");

		// Returning false automatically deallocates the automatically allocated handler that was created by DirectoryDeltaTransfer
		return false;
	}

}; //transferCallback;