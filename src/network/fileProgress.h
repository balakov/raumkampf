#pragma once
#include "RakPeerInterface.h"
#include "FileListTransfer.h"
#include "RakSleep.h"

#include "MessageIdentifiers.h"
#include "FileListTransferCBInterface.h"
#include "FileOperations.h"
#include "SuperFastHash.h"
#include "RakAssert.h"
#include "BitStream.h"
#include "IncrementalReadInterface.h"
#include "PacketizedTCP.h"
#include "SocketLayer.h"
#include <stdio.h>
#include "Gets.h"
// Sender progress notification
class FileProgress : public RakNet::FileListProgress
{
    virtual void OnFilePush(const char *fileName, unsigned int fileLengthBytes, unsigned int offset, unsigned int bytesBeingSent, bool done, RakNet::SystemAddress targetSystem, unsigned short setID)
	{
		printf("Sending %s bytes=%i offset=%i\n", fileName, bytesBeingSent, offset);
	}

    virtual void OnFilePushesComplete( RakNet::SystemAddress systemAddress, unsigned short setID )
	{
		char str[32];
		systemAddress.ToString(true, (char*) str);
		RAKNET_DEBUG_PRINTF("File pushes complete to %s\n", str);	
	}
	virtual void OnSendAborted( RakNet::SystemAddress systemAddress )
	{
		char str[32];
		systemAddress.ToString(true, (char*) str);
		RAKNET_DEBUG_PRINTF("Send aborted to %s\n", str);	
	}

};
