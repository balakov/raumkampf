#pragma once

#include "ReplicaManager3.h"
#include "server_endpoint.h"

using namespace RakNet;
class ServerReplicaManager : public ReplicaManager3
{
	virtual Connection_RM3* AllocConnection(const SystemAddress &systemAddress, RakNetGUID rakNetGUID) const {
		return new ServerEndpoint(systemAddress,rakNetGUID);
	}
	virtual void DeallocConnection(Connection_RM3 *connection) const {
		delete connection;
	}
};