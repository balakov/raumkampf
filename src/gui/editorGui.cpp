#include "gui\editorGui.h"
#include "ship\clientship.h"
#include "settings.h"
#include "ship\devicefactory.h"
#include "globals.h"
EditorGUI::EditorGUI(Edit* controller)
{	
	BOOST_LOG_TRIVIAL(info) << "Building Editor Gui";
	this->m_controller = controller;
	this->selectedDeviceIdentifier = "undefined";
	this->selectedSlot = 0;
	m_availableDevices = DeviceFactory::getAvailableDevices();
	Globals::getGUI()->removeAllWidgets();

	generalInfoPanel = tgui::Panel::create();
	Globals::getGUI()->add(generalInfoPanel);

	hullInfoPanel = tgui::Panel::create();
	Globals::getGUI()->add(hullInfoPanel);

	deviceSelectionPanel = tgui::Panel::create();
	Globals::getGUI()->add(deviceSelectionPanel);
	
	deviceConfigPanel = tgui::Panel::create();
	Globals::getGUI()->add(deviceConfigPanel);

	controlConfigurationPanel = tgui::Panel::create();
	Globals::getGUI()->add(controlConfigurationPanel);

	workbenchPanel = tgui::Panel::create();
	Globals::getGUI()->add(workbenchPanel);

	infoPanel = tgui::Panel::create();
	Globals::getGUI()->add(infoPanel);

	menuPanel = tgui::Panel::create();
	Globals::getGUI()->add(menuPanel);

	rebuild();
}
void EditorGUI::setAvailableDevices(std::vector<std::string> deviceList)
{
	//this->m_availableDevices = deviceList;


}
void EditorGUI::rebuildGeneralInfoPanel()
{
	generalInfoPanel->removeAllWidgets();

	generalInfoPanel->setSize(Globals::getWindow()->getSize().x*0.25, Globals::getWindow()->getSize().y*0.4);
	generalInfoPanel->setPosition(Globals::getWindow()->getSize().x*0.75, Globals::getWindow()->getSize().y * 0.1);
	generalInfoPanel->setBackgroundColor(sf::Color::Magenta);

	// make Background
	tgui::TextBox::Ptr generalBG = tgui::TextBox::create(Settings::getDataFolderBasePath() + "gui/widgets/Editor.conf");
	generalBG->setSize(tgui::bindWidth(generalInfoPanel), tgui::bindHeight(generalInfoPanel));
	generalInfoPanel->add(generalBG);
	// make Info
	tgui::Grid::Ptr generalInfoGrid = tgui::Grid::create();
	generalInfoPanel->add(generalInfoGrid);
	generalInfoGrid->setSize(tgui::bindWidth(generalInfoPanel), tgui::bindHeight(generalInfoPanel));

	tgui::Label::Ptr name = tgui::Label::create(Settings::getDataFolderBasePath() + "gui/widgets/Editor.conf");
	name->setText("Name: ");
	name->setTextSize(Settings::getGuiTextSize());
	generalInfoGrid->add(name);
	generalInfoGrid->addWidget(name, 0, 0);

	nameEdit = tgui::EditBox::create(Settings::getDataFolderBasePath() + "gui/widgets/Editor.conf");
	nameEdit->setSize(tgui::bindWidth(generalInfoPanel, 0.75f), 32);
	if (m_controller->hasShip())
	{
		nameEdit->setText(m_controller->getShip()->getName());
	}
	else {
		nameEdit->setText("No Ship");
	}
	
	nameEdit->setTextSize(Settings::getGuiTextSize());
	nameEdit->connect("TextChanged", &EditorGUI::updateShipName, this);
	generalInfoGrid->add(nameEdit);
	generalInfoGrid->addWidget(nameEdit, 0, 1);

	tgui::Label::Ptr dryMassLabel = tgui::Label::copy(name);
	dryMassLabel->setText("Dry Mass:");
	generalInfoGrid->add(dryMassLabel);
	generalInfoGrid->addWidget(dryMassLabel, 1, 0);


}

void EditorGUI::updateShipName()
{
	if (m_controller->hasShip())
		m_controller->getShip()->setName(nameEdit->getText());
	else 	BOOST_LOG_TRIVIAL(warning) << "No ship was set, updating name is void";
}
void EditorGUI::rebuildMenuPanel()
{
	menuPanel->removeAllWidgets();
	menuPanel->setSize(Globals::getWindow()->getSize().x*0.25, Globals::getWindow()->getSize().y*0.1);
	menuPanel->setPosition(Globals::getWindow()->getSize().x*0.75, 0);
	menuPanel->setBackgroundColor(sf::Color::Magenta);

	tgui::Button::Ptr saveButton = tgui::Button::create(Settings::getDataFolderBasePath() + "gui/widgets/Editor.conf");
	saveButton->setText("Save");
	saveButton->setTextSize(Settings::getGuiTextSize());
	saveButton->setSize(tgui::bindWidth(menuPanel)*0.5, tgui::bindHeight(menuPanel)*0.5);
	menuPanel->add(saveButton);

	tgui::Button::Ptr backButton = tgui::Button::copy(saveButton);
	backButton->setText("Back");
	backButton->setPosition(tgui::bindWidth(menuPanel)*0.5, 0);
	backButton->connect("pressed", &Edit::back, m_controller);
	menuPanel->add(backButton);

	tgui::Button::Ptr testButton = tgui::Button::copy(saveButton);
	testButton->setText("Test");
	testButton->setPosition(0, tgui::bindHeight(menuPanel)*0.5);
	testButton->connect("pressed", &Edit::notifyModification, m_controller);
	menuPanel->add(testButton);

	tgui::Button::Ptr clearButton = tgui::Button::copy(saveButton);
	clearButton->setText("Clear");
	clearButton->setPosition(tgui::bindWidth(menuPanel)*0.5, tgui::bindHeight(menuPanel)*0.5);
	clearButton->connect("pressed", &Edit::sendClearAll, m_controller);
	menuPanel->add(clearButton);

	saveButton->connect("pressed", &Edit::saveShip, m_controller);

}
void EditorGUI::rebuildHullInfoPanel()
{
	hullInfoPanel->removeAllWidgets();
	hullInfoPanel->setSize(Globals::getWindow()->getSize().x*0.25, Globals::getWindow()->getSize().y*0.5);
	hullInfoPanel->setBackgroundColor(sf::Color::Magenta);
	hullInfoPanel->setPosition(Globals::getWindow()->getSize().x*0.75, Globals::getWindow()->getSize().y*0.5);
	// make Background
	tgui::TextBox::Ptr hullBG = tgui::TextBox::create(Settings::getDataFolderBasePath() + "gui/widgets/Editor.conf");
	hullBG->setSize(tgui::bindWidth(hullInfoPanel), tgui::bindHeight(hullInfoPanel));
	hullInfoPanel->add(hullBG);

	// make Info
	tgui::Grid::Ptr hullInfoGrid = tgui::Grid::create();
	hullInfoPanel->add(hullInfoGrid);
	hullInfoGrid->setSize(tgui::bindWidth(hullInfoPanel), tgui::bindHeight(hullInfoPanel));

	tgui::Label::Ptr name = tgui::Label::create(Settings::getDataFolderBasePath() + "gui/widgets/Editor.conf");
	if (m_controller->hasShip())
	{
		name->setText("Model: " + m_controller->getShip()->getHull()->getModelName());
	}
	else name->setText("Model: undefined");

	name->setTextSize(Settings::getGuiTextSize());
	hullInfoGrid->add(name);
	hullInfoGrid->addWidget(name, 0, 0);

	//;

}

void EditorGUI::rebuildDeviceSelectionPanel()
{
	deviceSelectionPanel->removeAllWidgets();
	deviceSelectionPanel->setBackgroundColor(sf::Color::Magenta);
	deviceSelectionPanel->setPosition(0, 0);
	deviceSelectionPanel->setSize(Globals::getWindow()->getSize().x*0.25, Globals::getWindow()->getSize().y*0.5);

	deviceList = tgui::ListBox::create(Settings::getDataFolderBasePath() + "gui/widgets/Editor.conf");
	deviceList->setSize(tgui::bindWidth(deviceSelectionPanel), tgui::bindHeight(deviceSelectionPanel));
	for (int i = 0; i < this->m_availableDevices.size(); i++)
	{

		deviceList->addItem(this->m_availableDevices[i]);
	}
	deviceList->setSelectedItemByIndex(0);
	this->selectedDeviceIdentifier = deviceList->getSelectedItem();
	deviceSelectionPanel->add(deviceList); 
	deviceList->connect("ItemSelected", &EditorGUI::deviceSelected, this );

}

void EditorGUI::rebuildInfoPanel()
{
	infoPanel->removeAllWidgets();
	infoPanel->setBackgroundColor(sf::Color::Magenta);
	infoPanel->setPosition(0, Globals::getWindow()->getSize().y*0.5);
	infoPanel->setSize(Globals::getWindow()->getSize().x*0.25, Globals::getWindow()->getSize().y*0.5);
	// make Background

	deviceInfoBox = tgui::TextBox::create(Settings::getDataFolderBasePath().append("gui/widgets/Editor.conf"));
	deviceInfoBox->setSize(tgui::bindWidth(infoPanel), tgui::bindHeight(infoPanel));
	deviceInfoBox->setTextSize(Settings::getGuiTextSize());
	addDeviceButton = tgui::Button::create(Settings::getDataFolderBasePath().append("gui/widgets/Editor.conf"));
	addDeviceButton->setPosition(deviceInfoBox->getSize().x - addDeviceButton->getSize().x, deviceInfoBox->getSize().y - addDeviceButton->getSize().y);
	addDeviceButton->setText("Add");

	removeDeviceButton = tgui::Button::copy(addDeviceButton);
	removeDeviceButton->setText("Remove");
	removeDeviceButton->connect("clicked", &EditorGUI::removeSelectedDevice, this);
	addDeviceButton->connect("clicked", &EditorGUI::addSelectedDevice, this);

	infoPanel->add(deviceInfoBox);
	infoPanel->add(addDeviceButton);
	infoPanel->add(removeDeviceButton);
	
	if (m_controller->hasShip())
	{
		if (m_controller->getShip()->getHull()->getDevice(this->selectedSlot) != nullptr)
		{
			addDeviceButton->hide();
		}
		else removeDeviceButton->hide();
	}
	else deviceInfoBox->setText("No Ship");

}
void EditorGUI::rebuildControlConfigurationPanel()
{
	controlConfigurationPanel->removeAllWidgets();
	controlConfigurationPanel->setBackgroundColor(sf::Color::Magenta);
	controlConfigurationPanel->setPosition(Globals::getWindow()->getSize().x*0.25, Globals::getWindow()->getSize().y*0.75);
	controlConfigurationPanel->setSize(Globals::getWindow()->getSize().x*0.5, Globals::getWindow()->getSize().y*0.25);
	// make Background

	tgui::TextBox::Ptr controlConfigurationBg = tgui::TextBox::create(Settings::getDataFolderBasePath().append("gui/widgets/Editor.conf"));
	controlConfigurationBg->setSize(tgui::bindWidth(controlConfigurationPanel), tgui::bindHeight(controlConfigurationPanel));
	controlConfigurationPanel->add(controlConfigurationBg);

	//tgui::Picture::Ptr controllerImage = tgui::Picture::create(Settings::getDataFolderBasePath().append("img/controller.png"));
	//controlConfigurationPanel->add(controllerImage);

	keyConfigButtonPositions.push_back(sf::Vector2f(0.75, 0.25));
	keyConfigButtonPositions.push_back(sf::Vector2f(0.65, 0.5));
	keyConfigButtonPositions.push_back(sf::Vector2f(0.75, 0.75));
	keyConfigButtonPositions.push_back(sf::Vector2f(0.85, 0.5));

	keyConfigButtonPositions.push_back(sf::Vector2f(0.25, 0.25));
	keyConfigButtonPositions.push_back(sf::Vector2f(0.15, 0.5));
	keyConfigButtonPositions.push_back(sf::Vector2f(0.25, 0.75));
	keyConfigButtonPositions.push_back(sf::Vector2f(0.35, 0.5));

	keyConfigButtonPositions.push_back(sf::Vector2f(0.45, 0.5));
	keyConfigButtonPositions.push_back(sf::Vector2f(0.55, 0.5));

	updateControlConfigurationPanel();
}

void EditorGUI::updateControlConfigurationPanel()
{
		for (int idx = 0; idx < Settings::getAssignableKeys().size(); idx++)
		{
			std::string kn = Settings::getDescriptionFromKeyCode(Settings::getAssignableKeys()[idx]);

			tgui::Checkbox::Ptr key = tgui::Checkbox::create(Settings::getDataFolderBasePath().append("gui/widgets/Editor.conf"));

			std::vector<int> as = m_controller->getKeyMap()->getActivatedSlotsFromKeyString(kn);
			for (int asl = 0; asl < as.size(); asl++)
			{
				if (as[asl] == this->selectedSlot)
					key->check();
			}
			//key->setText(kn);
			key->connect("Checked", &EditorGUI::keyChecked, this, idx, true );
			key->connect("UnChecked", &EditorGUI::keyChecked, this, idx, false);
			
			key->setPosition(keyConfigButtonPositions[idx].x*controlConfigurationPanel->getSize().x, keyConfigButtonPositions[idx].y*controlConfigurationPanel->getSize().y );
			controlConfigurationPanel->add(key);
		}

}
void EditorGUI::keyChecked(int assignableKeyIndex, bool state)
{
	m_controller->getKeyMap()->print();
	std::string keyDesc = Settings::getDescriptionFromKeyCode(Settings::getAssignableKeys()[assignableKeyIndex]);
	if(state)
		m_controller->getKeyMap()->addMapping(selectedSlot, keyDesc );
	else m_controller->getKeyMap()->removeMapping(selectedSlot, keyDesc);
	m_controller->getKeyMap()->print();

}

void EditorGUI::deviceSelected()
{
	if (this->selectedDeviceIdentifier.compare(deviceList->getSelectedItem()) == 0) //was the device already selected
		return;
	else
	{
		this->selectedDeviceIdentifier = deviceList->getSelectedItem();
		Device* device = DeviceFactory::getByName(selectedDeviceIdentifier);
		deviceInfoBox->setText(device->getDescription());
		delete device;

	}


}
void EditorGUI::removeSelectedDevice()
{
	m_controller->sendDeviceRemoved(this->selectedSlot);
	/*
	if (m_controller->hasShip())
	{
		BOOST_LOG_TRIVIAL(info) << "Removing device from slot" << this->selectedSlot;
		this->removeDeviceButton->hide();
		this->addDeviceButton->show();
		deviceConfigPanel->removeAllWidgets();
		rebuildDeviceConfigPanel();
		this->showDeviceSelectionPanel();
		m_controller->getShip()->getHull()->removeDevice(this->selectedSlot);
		m_controller->notifyModification();
	}
	else BOOST_LOG_TRIVIAL(warning) << "No ship to remove device from";*/
}

void EditorGUI::addSelectedDevice()
{
	BOOST_LOG_TRIVIAL(info) << "Adding" << this->selectedDeviceIdentifier << "into slot" << this->selectedSlot;

		this->addDeviceButton->hide();
		this->removeDeviceButton->show();
		this->showDeviceConfigPanel();
		m_controller->sendDeviceAdded(this->selectedSlot, this->selectedDeviceIdentifier);
		//m_controller->getShip()->getHull()->setDevice(d, this->selectedSlot);
		// setting new config widget
		//deviceConfigPanel->remove(configWidget);
		//configWidget = d->getConfigWidget(this->deviceConfigPanel->getSize());
		//deviceConfigPanel->add(configWidget);
		//m_controller->notifyModification();
	

}
void EditorGUI::rebuildDeviceConfigPanel()
{
	deviceConfigPanel->removeAllWidgets();
	deviceConfigPanel->setBackgroundColor(sf::Color::Magenta);
	deviceConfigPanel->setPosition(0,0);
	deviceConfigPanel->setSize(Globals::getWindow()->getSize().x*0.25, Globals::getWindow()->getSize().y*0.5);
	// make Background

	tgui::TextBox::Ptr deviceConfigBg = tgui::TextBox::create(Settings::getDataFolderBasePath() + "gui/widgets/Editor.conf");
	deviceConfigBg->setSize(tgui::bindWidth(deviceConfigPanel), tgui::bindHeight(deviceConfigPanel));
	deviceConfigPanel->add(deviceConfigBg);

}
void EditorGUI::rebuildWorkbenchPanel()
{
	BOOST_LOG_TRIVIAL(info) << "Rebuilding WorkbenchPanel";
	workbenchPanel->removeAllWidgets();
	slotButtons.clear();
	workbenchPanel->setBackgroundColor(sf::Color::Magenta);
	workbenchPanel->setPosition(Globals::getWindow()->getSize().x*0.25, 0);
	workbenchPanel->setSize(Globals::getWindow()->getSize().x*0.5, Globals::getWindow()->getSize().y*0.75);
	// make Background

	if (m_controller->hasShip())
	{
		std::string himg = Settings::getHullImagePath().append(m_controller->getShip()->getHull()->getSchematicFile());
		std::cout << himg;
		tgui::Picture::Ptr workBenchBg = tgui::Picture::create(himg);
		workBenchBg->setSize(tgui::bindWidth(workbenchPanel), tgui::bindHeight(workbenchPanel));
		workbenchPanel->add(workBenchBg);

		for (int i = 0; i < m_controller->getShip()->getHull()->getNumberOfSlots(); i++)
		{
			tgui::Button::Ptr slotButton = tgui::Button::create(Settings::getDataFolderBasePath().append("gui/widgets/Editor.conf"));
			slotButton->setSize(32, 32);
			

			slotButton->getRenderer()->setNormalImage(Settings::getDataFolderBasePath() + "gui/widgets/editorSlot.png", sf::IntRect(0, 0, 32, 32));
			
			slotButton->getRenderer()->setFocusedImage(Settings::getDataFolderBasePath() + "gui/widgets/editorSlot.png", sf::IntRect(32, 0, 64, 32), sf::IntRect(32, 0, 64, 32));
			slotButton->getRenderer()->setHoverImage(Settings::getDataFolderBasePath() + "gui/widgets/editorSlot.png", sf::IntRect(32, 0, 64, 32), sf::IntRect(32, 0, 64, 32));
			slotButton->getRenderer()->setDownImage(Settings::getDataFolderBasePath() + "gui/widgets/editorSlot.png", sf::IntRect(32, 0, 64, 32), sf::IntRect(32, 0, 64, 32));



			sf::Vector2f pos = m_controller->getShip()->getHull()->getSlotPosition(i);
			pos = convertSlotPositionToMainPanelPosition(pos);
			pos.x -= 32 * 0.5;
			pos.y -= 32 * 0.5;
			slotButton->setPosition(pos);
			slotButton->connect("pressed", &EditorGUI::slotSelected, this, i);
			workbenchPanel->add(slotButton);
			this->slotButtons.push_back(slotButton);
			if (i == selectedSlot)
			{
				//slotButton->getRenderer()->setNormalImage(Settings::getDataFolderBasePath() + "gui/widgets/editorSlot.png", sf::IntRect(32, 0, 64, 32), sf::IntRect(32, 0, 64, 32));
				slotSelected(i);

			}
		}
	}

}
void EditorGUI::slotSelected(int slot)
{
	BOOST_LOG_TRIVIAL(info) << "Setting " << selectedSlot << " unselected";

	slotButtons[selectedSlot]->getRenderer()->setNormalImage(Settings::getDataFolderBasePath() + "gui/widgets/editorSlot.png", sf::IntRect(0, 0, 32, 32));
	this->selectedSlot = slot;
	BOOST_LOG_TRIVIAL(info) << "Setting " << selectedSlot << " selected";
	slotButtons[selectedSlot]->getRenderer()->setNormalImage(Settings::getDataFolderBasePath() + "gui/widgets/editorSlot.png", sf::IntRect(32, 0, 64, 32), sf::IntRect(32, 0, 64, 32));

	updateControlConfigurationPanel();
	rebuildDeviceConfigPanel();
	Device * d = m_controller->getShip()->getHull()->getDevice(slot);
	if (d == nullptr)
	{
		BOOST_LOG_TRIVIAL(info) << "No device, showing add button";
		this->addDeviceButton->show();
		this->removeDeviceButton->hide();
		this->showDeviceSelectionPanel();
	}
	else
	{
		BOOST_LOG_TRIVIAL(info) << "has device, showing remove button";
		this->addDeviceButton->hide();
		this->removeDeviceButton->show();
		std::cout << "should show conf widget"; // ->createConfigWidget();

		configWidget = d->getConfigWidget(sf::Vector2f(200, 100));
		deviceConfigPanel->add(configWidget);
		this->showDeviceConfigPanel();
		deviceInfoBox->setText(d->getDescription());
	}


}
sf::Vector2f EditorGUI::convertSlotPositionToMainPanelPosition(sf::Vector2f slotPosition)
{
	sf::Vector2f scaleFactor(workbenchPanel->getSize().x / 64.0, workbenchPanel->getSize().y / 64.0);
	float x = slotPosition.y;
	x += 32;
	x *= scaleFactor.x;
	float y = -slotPosition.x;
	y += 32;
	y *= scaleFactor.y;
	return sf::Vector2f(x, y);	// magic CCW rotation
}
void EditorGUI::showDeviceSelectionPanel()
{
	deviceSelectionPanel->show();
	deviceConfigPanel->hide();
}

void EditorGUI::showDeviceConfigPanel()
{
	deviceSelectionPanel->hide();
	deviceConfigPanel->show();
}

void EditorGUI::drawDevices(sf::RenderTarget* target)
{

	if (m_controller->hasShip())
	{
		Hull* h = m_controller->getShip()->getHull();
		for (int i = 0; i < h->getNumberOfSlots(); i++)
		{
			Device* d = h->getDevice(i);
			if (d != nullptr)
			{
				sf::Vector2f position(this->workbenchPanel->getAbsolutePosition());
				sf::Vector2f temp = this->convertSlotPositionToMainPanelPosition(h->getSlotPosition(i));
				position.x += temp.x;
				position.y += temp.y;

				d->getEditorSprite()->setPosition(position);
				d->getEditorSprite()->setRotation(d->getLocalRotation() - 90);		// whole editor is 90deg CCW rotated :p	
				target->draw(*d->getEditorSprite());
			}

		}
	}
}
void EditorGUI::rebuild(void)
{

	rebuildGeneralInfoPanel();
	rebuildHullInfoPanel();
	rebuildDeviceSelectionPanel();
	rebuildDeviceConfigPanel();
	rebuildMenuPanel();
	rebuildWorkbenchPanel();
	rebuildInfoPanel();
	rebuildControlConfigurationPanel();
	//if (this->slotButtons.size()<this->selectedSlot)
	//	this->slotSelected(selectedSlot);
}
