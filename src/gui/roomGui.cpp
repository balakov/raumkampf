#include "gui/roomGui.h"	
#include "settings.h"
#include "globals.h"
#include "state/room.h"
RoomGUI::RoomGUI(Raumkampf::Room* model)
{
	
	this->model = model;
	BOOST_LOG_TRIVIAL(info) << "Building Room GUI";

	
	tgui::Panel::Ptr chatPanel = tgui::Panel::create();
	chatPanel->setSize((Settings::getResolution().x/3),(Settings::getResolution().y/4)*2);
	chatPanel->setPosition((Settings::getResolution().x/3)*2,0);

	playerPanel = tgui::Panel::create();
	playerPanel->setSize(Settings::getResolution().x *0.75, Settings::getResolution().y *0.3);
	playerPanel->setBackgroundColor(sf::Color::Black);

	mapPanel = tgui::Panel::create();	
	mapPanel->setSize(Settings::getResolution().x *0.75, Settings::getResolution().y*0.4);
	mapPanel->setPosition(0, Settings::getResolution().y *0.3);
	mapPanel->setBackgroundColor(sf::Color(10,10,10,255));

	 gameInfoPanel = tgui::Panel::create();	
	gameInfoPanel->setSize(Settings::getResolution().x *0.75, Settings::getResolution().y*0.3 );
	gameInfoPanel->setPosition(0, Settings::getResolution().y *0.7);
	gameInfoPanel->setBackgroundColor(sf::Color::Black);

	tgui::Panel::Ptr controlPanel = tgui::Panel::create();
	controlPanel->setSize((Settings::getResolution().x / 3), Settings::getResolution().y / 4);
	controlPanel->setBackgroundColor(sf::Color::Black);
	controlPanel->setPosition((Settings::getResolution().x / 3) * 2, (Settings::getResolution().y / 4) * 3);



	Globals::getGUI()->add(chatPanel);
	Globals::getGUI()->add(playerPanel);
	Globals::getGUI()->add(mapPanel);
	Globals::getGUI()->add(gameInfoPanel);
	Globals::getGUI()->add(controlPanel);



	tgui::ChatBox::Ptr chat = tgui::ChatBox::create();
	chat->setPosition(0,0);

	tgui::Button::Ptr chatSend = tgui::Button::create();
	chatSend->setSize(((Settings::getResolution().x/3)/4)*3, 50);
	chatSend->setTextSize(12);
	chatSend->setText("Send");
	chatSend->setPosition((Settings::getResolution().x/3)/4,Settings::getResolution().y/4);

	tgui::TextBox::Ptr chatEdit = tgui::TextBox::create(Settings::getDataFolderBasePath() + "gui/widgets/Room.conf");
	chatEdit->setText("bla");
	chatEdit->setPosition(0,Settings::getResolution().y/4);

	chatPanel->add(chatEdit, "chatedit");
	chatPanel->add(chatSend, "chatsend");

	
	tgui::Panel::Ptr connectedUsersListPanel = tgui::Panel::create();
	connectedUsersListPanel->setPosition((Settings::getResolution().x/3)*2,(Settings::getResolution().y/4)*2);
	connectedUsersListPanel->setSize((Settings::getResolution().x/3)*2,(Settings::getResolution().y/4));
	
	tgui::ListBox::Ptr userlist = tgui::ListBox::create(Settings::getDataFolderBasePath() + "gui/widgets/Room.conf");
	connectedUsersListPanel->add(userlist);
	Globals::getGUI()->add(connectedUsersListPanel);

	tgui::Button::Ptr forceStart = tgui::Button::create(Settings::getDataFolderBasePath() + "gui/widgets/Room.conf");
	forceStart->setText("Force Start");
	forceStart->connect("pressed", &Raumkampf::Room::forceStart, model);
	controlPanel->add(forceStart);

	restore();
	BOOST_LOG_TRIVIAL(info) << "..DONE";
	
}

void RoomGUI::rebuildUserPanel()
{

		BOOST_LOG_TRIVIAL(info) << "rebuilding user psanel";
		playerPanel->removeAllWidgets();
		BOOST_LOG_TRIVIAL(info) << "old panel cleared" << ClientGame::getInstance()->getNumberOfPlayerSlots() << "<- maxP";

		for (int i = 0; i < ClientGame::getInstance()->getNumberOfPlayerSlots(); i++)
		{

			BOOST_LOG_TRIVIAL(info) << "building button" << ClientGame::getInstance()->getNumberOfPlayerSlots();
			tgui::Button::Ptr joinButton = tgui::Button::create(Settings::getDataFolderBasePath() + "gui/widgets/Options.conf");

			joinButton->setTextSize(16);
			joinButton->setSize(128,64);
			joinButton->setPosition(128*i,0);
			joinButton->connect("pressed", &Raumkampf::Room::join, model, i);

			tgui::Button::Ptr shipButton = tgui::Button::create(Settings::getDataFolderBasePath() + "gui/widgets/Options.conf");
			shipButton->setText("no ship");
			shipButton->setSize(50,50);
			shipButton->setTextSize(16);
			shipButton->setPosition(100*i,50);
			shipButton->connect("pressed", &Raumkampf::Room::offerShip, model);

			playerPanel->add(joinButton,"join"+i);
			playerPanel->add(shipButton,"ship"+i);
			this->joinButtons.push_back(joinButton);
			this->shipButtons.push_back(shipButton);
		}
		BOOST_LOG_TRIVIAL(info) << "..done";

}

void RoomGUI::rebuildMapPanel()
{

		BOOST_LOG_TRIVIAL(info) << "rebuilding map panel";
		mapPanel->removeAllWidgets();
		tgui::Grid::Ptr mapGrid = tgui::Grid::create();
		mapGrid->setSize(tgui::bindWidth(mapPanel), tgui::bindHeight(mapPanel));
		mapPanel->add(mapGrid);

		mapName = tgui::Label::create(Settings::getDataFolderBasePath()+"gui/widgets/Options.conf");
		mapGrid->add(mapName);
		mapGrid->addWidget(mapName, 0, 0);

		mapName->setText("No Map");	mapName->setSize(200,200);


}

void RoomGUI::rebuildGameInfoPanel()
{

		BOOST_LOG_TRIVIAL(info) << "rebuilding info psanel";
		gameInfoPanel->removeAllWidgets();
		tgui::Grid::Ptr gameInfoGrid = tgui::Grid::create();
		gameInfoGrid->setSize(tgui::bindWidth(gameInfoPanel), tgui::bindHeight(gameInfoPanel));
		gameInfoPanel->add(gameInfoGrid);

		tgui::Label::Ptr nameLabel = tgui::Label::create(Settings::getDataFolderBasePath() + "gui/widgets/Options.conf");
		nameLabel->setTextSize(16);
		nameLabel->setText("Game Name: ");
		gameInfoGrid->add(nameLabel);
		gameInfoGrid->addWidget(nameLabel, 0, 0);

		gameName = tgui::Label::copy(nameLabel);
		gameName->setText("undefined");
		gameInfoGrid->add(gameName);
		gameInfoGrid->addWidget(gameName, 0, 1);

		tgui::Label::Ptr modeLabel = tgui::Label::copy(nameLabel);
		modeLabel->setText("Game Mode: ");
		gameInfoGrid->add(modeLabel);
		gameInfoGrid->addWidget(modeLabel, 1, 0);

}
void RoomGUI::update()
{

	// update player panel
	for(int i=0; i < ClientGame::getInstance()->getNumberOfPlayerSlots();i++)
	{

		tgui::Button::Ptr joinButton = joinButtons[i];
		tgui::Button::Ptr shipButton = shipButtons[i];
		//tgui::Button::Ptr shipButton = this->playerPanel->get("ship"+i);


		if (ClientGame::getInstance()->isEmptySlot(i))
		{
			//BOOST_LOG_TRIVIAL(info) << "empty slot";
			joinButton->setText("join");
			shipButton->setText("noShip");
		}
		else 
		{

			//BOOST_LOG_TRIVIAL(info) << "building used slot";
			ClientUser* usr = ClientGame::getInstance()->getUserBySlot(i);
			if (usr!=nullptr)
			{
				
				joinButton->setText(usr->guid.ToString());
				if (ClientGame::getInstance()->getUserBySlot(i)->hasShip)
				shipButton->setText("a ship");
			}

		}


		// update map panel
		if (ClientGame::getInstance()->hasMap())
		{
			this->mapName->setText(ClientGame::getInstance()->getMapName().C_String());
		}


	}



}


void RoomGUI::addChatMessage(std::string msg)
{
	//this->chat->addLine(msg);

}
std::string RoomGUI::getChatMessage()
{
	//std::string result = this->chatEdit->getText();
	//this->chatEdit->setText("");
	return "nope";

}

void RoomGUI::restore(void)
{
	rebuildUserPanel();
	rebuildMapPanel();
	rebuildGameInfoPanel();

}