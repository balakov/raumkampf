#pragma once

#include "TGUI/TGUI.hpp"
#include "state/hangar.h"
class HangarGUI
{
public:
	HangarGUI(Hangar* controller);
	void rebuild();

private:
	Hangar* controller;
	tgui::Grid::Ptr slotGrid;

	void buildSlot(std::string shipDefinitionFile, int row, int col);
};
