#include "gui\gui.h"
#include "settings.h"
#include "SFML\Audio.hpp"
GUI::GUI()
{

	buf0 = new sf::SoundBuffer();
	buf0->loadFromFile(Settings::getDataFolderBasePath().append("snd/CM139_FX20.wav"));
	menuForward = new sf::Sound(*buf0);
	menuForward->setVolume(Settings::getSoundVolume());
	buf1 = new sf::SoundBuffer();
	buf1->loadFromFile(Settings::getDataFolderBasePath().append("snd/CM139_FX21.wav"));
	menuReturn = new sf::Sound(*buf1);
	menuReturn->setVolume(Settings::getSoundVolume());
	buf2 = new sf::SoundBuffer();
	buf2->loadFromFile(Settings::getDataFolderBasePath().append("snd/CM139_FX11.wav"));
	menuNoCanDo = new sf::Sound(*buf2);
	menuNoCanDo->setVolume(Settings::getSoundVolume());
	buf3 = new sf::SoundBuffer();
	buf3->loadFromFile(Settings::getDataFolderBasePath().append("snd/CM139_FX11.wav"));
	importantChoice = new sf::Sound(*buf3);
	importantChoice->setVolume(Settings::getSoundVolume());
	buf4 = new sf::SoundBuffer();
	buf4->loadFromFile(Settings::getDataFolderBasePath().append("snd/CM139_FX16.wav"));
	click = new sf::Sound(*buf4);
	click->setVolume(Settings::getSoundVolume());


}