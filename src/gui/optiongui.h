#pragma once

#include "TGUI/TGUI.hpp"
#include "clientgame.h"
#include "state/options.h"
#include "gui\gui.h"
class OptionGUI : public GUI
{
public:
	OptionGUI(Options* options);
	//void update();
	void showAudioVideo();
	void showGeneral();
	void showProfile();
	void restore();
	void applyVideoSettings();
private:

	Options* model;
	tgui::Grid::Ptr subMenuSelection;
	tgui::Panel::Ptr audioVideoPanel, generalPanel, profilePanel;
	tgui::Button::Ptr audioVideo, general, profile, exit;

	tgui::Slider::Ptr musicVolume, sfxVolume;
	
	tgui::Checkbox::Ptr vSync;
	tgui::Checkbox::Ptr fullscreen;
	tgui::ComboBox::Ptr resolutions;

	bool hasFocus;
	void buildPanelSelection();
	void buildControlPanel();
	void buildAudioVideoPanel();
	void buildGeneralPanel();
	void buildNetworkPanel();	
	void buildProfilePanel();




};
