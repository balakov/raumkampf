#pragma once
#include "state\edit.h"
#include "ship\clientship.h"
class EditorGUI {

public:
	EditorGUI(Edit* controller);
	void setAvailableDevices(std::vector<std::string> deviceList);
	void drawDevices(sf::RenderTarget* target);
	//void setShip(ClientShip* ship);
	//void setKeyMap(KeyMap* keymap);
	void rebuild(void);
private:
	void removeSelectedDevice();
	void addSelectedDevice();
	void updateShipName();


	void rebuildDeviceSelectionPanel();
	void rebuildDeviceConfigPanel();
	void rebuildInfoPanel();
	void deviceSelected();
	void rebuildGeneralInfoPanel();
	void rebuildHullInfoPanel();
	void rebuildWorkbenchPanel();

	void rebuildControlConfigurationPanel();
	void updateControlConfigurationPanel();
	void keyChecked(int assignableKeyIndex, bool state);

	void rebuildMenuPanel();

	void showDeviceSelectionPanel();
	void showDeviceConfigPanel();
	sf::Vector2f convertSlotPositionToMainPanelPosition(sf::Vector2f slotPosition);
	void slotSelected(int slot);

	Edit* m_controller;
	std::vector<std::string> m_availableDevices;
	tgui::Panel::Ptr generalInfoPanel, hullInfoPanel, menuPanel, deviceSelectionPanel, deviceConfigPanel, infoPanel, controlConfigurationPanel, workbenchPanel;
	tgui::ListBox::Ptr deviceList;

	tgui::Widget::Ptr configWidget;

	tgui::TextBox::Ptr deviceInfoBox;
	std::string selectedDeviceIdentifier;
	tgui::Button::Ptr addDeviceButton, removeDeviceButton;
	std::vector<tgui::Button::Ptr> slotButtons;
	int selectedSlot;

	tgui::EditBox::Ptr nameEdit;
	std::vector<sf::Vector2f> keyConfigButtonPositions;
};