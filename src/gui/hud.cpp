﻿#include "gui/hud.h"
#include "settings.h"
#include <iostream>
#include <sstream>
#include <ship/liquidfuelthruster.h>
#include <ship/reactor.h>
#include <ship/massdriver.h>

Hud::Hud()
{
	hudView.setSize(sf::Vector2f(800, 150));
	hudView.setViewport(sf::FloatRect(0, 0.75, 1, 0.25));
	hudView.setCenter(400, 75);

	std::string mockup = Settings::getDataFolderBasePath().append("/msc/hudmockup.png");
    Hud::hudTexture.loadFromFile(mockup);
    Hud::hudSprite.setTexture(hudTexture);
	Hud::hudSprite.setPosition(0,480);

    font.loadFromFile(Settings::getDataFolderBasePath().append("font/Fixedsys500c.ttf"));
	time = 1;
	hasTarget=false;
}

void Hud::bind(ClientShip *s)
{
    target = s;
	hasTarget = true;
}

void Hud::draw(sf::RenderTarget * target)
{
	//target->draw(hudSprite);
	target->setView(hudView);
	if(hasTarget)
	{
		drawDeviceInfo(target);
		drawFuelInfo(target);
		drawEnergyInfo(target);
		drawHullInfo(target);
	}
}

void Hud::drawDebugInfo(sf::RenderTarget * target)
{
	target->setView(target->getDefaultView());
		//target->setView(target->getDefaultView());
		// end the current frame
		time = Clock.getElapsedTime().asSeconds();
        Clock.restart();
		float fps = 1.0f/time;
		std::stringstream ss (std::stringstream::in | std::stringstream::out);
		sf::Text text;
	    text.setFont(font);
		text.setCharacterSize(16);
		text.setPosition(Settings::getResolution().x-100,0);
	    ss << "Frames/Sec\n" << ceil(fps);
        text.setString(ss.str());
		text.setColor(sf::Color::Magenta);
        target->draw(text);	
		target->setView(hudView);
}
void Hud::drawScore(sf::RenderTarget * target, ClientGame* game)
{
	target->setView(target->getDefaultView());
	std::stringstream ss (std::stringstream::in | std::stringstream::out);
    sf::Text text;
    text.setString("bla");
    text.setFont(font);
	text.setColor(sf::Color::Magenta);
    text.setCharacterSize(16);
	for(int i=0; i < game->getUsers().size(); i++)
	{
		ss << "User #"<<i;
		ss << "kills: " <<game->getUsers()[i]->kills << "fragged: " <<game->getUsers()[i]->shotdown << "crashed: " << game->getUsers()[i]->crashed << "\n";
	}
    text.setString(ss.str());
    target->draw(text);
	target->setView(hudView);

}

void Hud::drawSpatialInfo(sf::RenderTarget * rndtarget)
{
	if (hasTarget)
	{
		sf::Text text;
		text.setColor(sf::Color::White);
		text.setFont(font);
		text.setCharacterSize(16);
		text.setPosition(target->getPosition().x, target->getPosition().y);
		std::stringstream txtbuffer(std::stringstream::in | std::stringstream::out);
		txtbuffer << "Position\n(" << target->getPosition().x << "|" << target->getPosition().y << ")";
		txtbuffer << "\nOrientation " << target->getRotation();
		txtbuffer << "\nLinear velocity\n(" << target->getLinearVelocity().x << "|" << target->getLinearVelocity().y << ")";
		txtbuffer << "\nAngular momentum\n" << target->getAngularMomentum();
		//txtbuffer << "Speed: " << target->getLinearVelocity()
		text.setString(txtbuffer.str());
		rndtarget->draw(text);
	}
}

void Hud::drawDeviceInfo(sf::RenderTarget * rndtarget)
{
	//rndtarget->setView(rndtarget->getDefaultView());
	sf::Text text;
	text.setColor(sf::Color::White);
    text.setFont(font);
    text.setCharacterSize(16);
	text.setPosition(300, 0);
	std::stringstream txtbuffer (std::stringstream::in | std::stringstream::out);
	std::vector<Device*> devices = target->getHull()->getDevices();
	for(int i=0; i < devices.size()/2; i++)
	{
		txtbuffer << devices[i]->getName();
		if(devices[i]->getType().compare("massdriver")==0)
		{
			MassDriver* temp = (MassDriver*)devices[i];
			txtbuffer << "\nCharge: " << temp->getChargePercent()*100 << "%";

		}
		if(devices[i]->getState())
			txtbuffer << " ON";
		else txtbuffer << " OFF";
		txtbuffer << "\n";
	}
    text.setString(txtbuffer.str());
    rndtarget->draw(text);
	txtbuffer.str("");
	txtbuffer.clear();
	text.setPosition(500,0);
	for(int i=devices.size()/2; i < devices.size(); i++)
	{
		txtbuffer << devices[i]->getName();
		if(devices[i]->getType().compare("massdriver")==0)
		{
			MassDriver* temp = (MassDriver*)devices[i];
			txtbuffer << "\nCharge: " << temp->getChargePercent()*100 << "%";

		}
		if(devices[i]->getState())
			txtbuffer << " ON";
		else txtbuffer << " OFF";
		txtbuffer << "\n";
	}
	text.setString(txtbuffer.str());
    rndtarget->draw(text);
}

void Hud::drawFuelInfo(sf::RenderTarget * rndtarget)
{
	std::stringstream ds (std::stringstream::in | std::stringstream::out);
	sf::Text dvc;
	dvc.setFont(font);
	dvc.setCharacterSize(16);
	dvc.setPosition(0,40);
	dvc.setColor(sf::Color::White);

	std::vector<Device*> dev = this->target->getHull()->getFuelSources();
	for(int i=0; i < dev.size();i++)
	{
		ds << dev[i]->getName();
		Tank* tank = (Tank*)dev[i];
		ds << "\nFuel " << tank->getCapacity() << "/" << tank->getFuelLeft() << "\n";
	}
	dvc.setString(ds.str());
	rndtarget->draw(dvc);


}
void Hud::drawEnergyInfo(sf::RenderTarget * rndtarget)
{
	std::stringstream ds (std::stringstream::in | std::stringstream::out);
	sf::Text dvc;
	dvc.setFont(font);
	dvc.setCharacterSize(16);
	dvc.setPosition(0,100);
	dvc.setColor(sf::Color::White);
	std::vector<Device*> dev = this->target->getHull()->getPowerSources();

	for(int i=0; i < dev.size();i++)
	{
		ds << dev[i]->getName();
		Reactor* reactor = (Reactor*)dev[i];
		ds << "\nCapacity " << reactor->getMaxOutput() << "MW,\nCurrent Load: "  << reactor->getCurrentLoadPercent()*100 << "%\n";
	}
	dvc.setString(ds.str());
	rndtarget->draw(dvc);
}
void Hud::drawHullInfo(sf::RenderTarget * rndtarget)
{
	std::stringstream ds (std::stringstream::in | std::stringstream::out);
	sf::Text dvc;
	dvc.setFont(font);
	dvc.setCharacterSize(16);
	dvc.setPosition(0,0);
	dvc.setColor(sf::Color::White);

	ds << "Armor: " << this->target->getHull()->getArmorLeft() << "/" << this->target->getHull()->getArmor();

	ds << "\nShip mass: " << this->target->getMass() << "kg";
	dvc.setString(ds.str());
	rndtarget->draw(dvc);
}