#pragma once

#include "TGUI/TGUI.hpp"
#include "clientgame.h"
#include "state/room.h"
#include "gui\gui.h"
class RoomGUI : public GUI
{
public:
	RoomGUI(Raumkampf::Room* model);
	void update();
	void addChatMessage(std::string msg);
	std::string getChatMessage();
	void restore(void);
private:
	Raumkampf::Room* model;

	void rebuildUserPanel();
	void rebuildMapPanel();
	void rebuildGameInfoPanel();
	tgui::Panel::Ptr playerPanel, mapPanel, gameInfoPanel;
	std::vector<tgui::Button::Ptr> joinButtons;
	std::vector<tgui::Button::Ptr> shipButtons;
	tgui::Label::Ptr mapName, gameName;
};
