#include "hangarGui.h"
#include "profilecontainer.h"
#include "settings.h"
#include "ship\clientship.h"
#include "globals.h"
HangarGUI::HangarGUI(Hangar* controller)
{
	this->controller = controller;
	rebuild();

}

void HangarGUI::rebuild()
{
	slotGrid = tgui::Grid::create();
	Globals::getGUI()->add(slotGrid);
	Profile* profile = ProfileContainer::getInstance()->getActiveProfile();

	for (int i = 0; i < 3; i++)
	{
		std::string path = Settings::getDataFolderBasePath().append("save/").append(profile->getPath());
		path.append("/slot" + std::to_string(i) +".xml");
		buildSlot(path, 0, i);

	}

}

void HangarGUI::buildSlot(std::string shipDefinitionFile, int row, int col)
{
	ClientShip ship;
	ship.loadFromFile(shipDefinitionFile);
	std::cout << shipDefinitionFile;
	tgui::Button::Ptr shipButton = tgui::Button::create(Settings::getDataFolderBasePath().append("gui/widgets/Black.conf"));	
	shipButton->setTextSize(16);
	shipButton->setText(ship.getName());
	shipButton->setSize(128, 128);

	shipButton->connect("pressed", &Hangar::toEditor, controller, shipDefinitionFile);
	slotGrid->add(shipButton);
	slotGrid->addWidget(shipButton, row, col);

}