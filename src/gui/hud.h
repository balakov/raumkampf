﻿#pragma once
#include "WindowsIncludes.h"
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <iostream>
#include "ship/clientship.h"
#include "clientgame.h"
#include "ship/tank.h"
class Hud
{

private:
    sf::Texture hudTexture;
    sf::RenderTexture testtex;
    sf::Sprite hudSprite;
	sf::Font font;
	sf::View hudView;
    ClientShip *target;
	bool hasTarget;
	sf::Clock Clock;
	float time;

	void drawHullInfo(sf::RenderTarget * rndtarget);

	void drawDeviceInfo(sf::RenderTarget * rndtarget);
	void drawFuelInfo(sf::RenderTarget * rndtarget);
	void drawEnergyInfo(sf::RenderTarget * rndtarget);
public:
    Hud();
    void bind(ClientShip *s);
	void drawSpatialInfo(sf::RenderTarget * rndtarget);
    void draw(sf::RenderTarget * target);
	void drawScore(sf::RenderTarget * target, ClientGame* game);
	void drawDebugInfo(sf::RenderTarget * target);
};
