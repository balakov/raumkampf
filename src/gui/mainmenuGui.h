#pragma once

#include "TGUI/TGUI.hpp"
#include "clientgame.h"
#include "state/mainmenu.h"
class MainMenuGUI
{
public:
	MainMenuGUI(MainMenu* model);
	void update();
	void rebuild();
	void showIPEntry();
	void showHostOptions();
private:
	MainMenu* model;

	// join menu
	tgui::TextBox::Ptr ipEntry;
	tgui::Button::Ptr ipEntered;

	// host menu
	tgui::Panel::Ptr hostPanel, joinPanel;
	tgui::ComboBox::Ptr mapSelection;
	tgui::EditBox::Ptr gameName;
	tgui::Button::Ptr startAndConnectButton, cancelButton;
};
