#pragma once
#include "SFML\Audio.hpp"


class GUI
{
public:
	GUI();
	virtual void restore() = 0;


protected:
	sf::SoundBuffer* buf0;
	sf::SoundBuffer* buf1;
	sf::SoundBuffer* buf2;
	sf::SoundBuffer* buf3;
	sf::SoundBuffer* buf4;
	sf::Sound* menuForward;
	sf::Sound* menuReturn;
	sf::Sound* menuNoCanDo;
	sf::Sound* importantChoice;
	sf::Sound* click;
};