#include "gui\optiongui.h"
#include "settings.h"
#include "globals.h"
#include <boost/log/trivial.hpp>
#include "profile.h"
#include "profilecontainer.h"
OptionGUI::OptionGUI(Options* model)
{
	this->model = model;
	restore();
	showAudioVideo();
}

void OptionGUI::buildPanelSelection()
{
	subMenuSelection = tgui::Grid::create();
	//subMenuSelection->setBackgroundColor(sf::Color::Green);
	subMenuSelection->setSize(Settings::getResolution().x, Settings::getResolution().y / 6);
	Globals::getGUI()->add(subMenuSelection);
	
	audioVideo = tgui::Button::create(Settings::getDataFolderBasePath()+"gui/widgets/Options.conf");
	//audio->load();	
	this->audioVideo->setSize(100, 50);
	this->audioVideo->setTextSize(Settings::getGuiTextSize());
	this->audioVideo->setText("Audio/Video");
	this->audioVideo->connect("pressed", &OptionGUI::showAudioVideo, this);
	subMenuSelection->add(audioVideo);
	subMenuSelection->addWidget(audioVideo, 0, 0);

	general = tgui::Button::create(Settings::getDataFolderBasePath()+"gui/widgets/Options.conf");
	//general->load();	
	this->general->setSize(100, 50);
	this->general->setTextSize(Settings::getGuiTextSize());
	this->general->setText("General");
	this->general->connect("pressed", &OptionGUI::showGeneral, this);
	subMenuSelection->add(general);
	subMenuSelection->addWidget(general, 0, 1);
	//Globals::getGUI()->add(subMenuSelection);

	profile = tgui::Button::create(Settings::getDataFolderBasePath() + "gui/widgets/Options.conf");
	this->profile->setSize(100, 50);
	this->profile->setTextSize(Settings::getGuiTextSize());
	this->profile->setText("Profile");
	this->profile->connect("pressed", &OptionGUI::showProfile, this);
	subMenuSelection->add(profile);
	subMenuSelection->addWidget(profile, 0, 2);

	exit = tgui::Button::create(Settings::getDataFolderBasePath() + "gui/widgets/Options.conf");
	this->exit->setSize(100, 50);
	this->exit->setTextSize(Settings::getGuiTextSize());
	this->exit->setText("Exit");
	this->exit->connect("pressed", &Options::toMainMenu, model);
	subMenuSelection->add(exit);
	subMenuSelection->addWidget(exit, 0, 3);
}
void OptionGUI::buildControlPanel()
{




}
void OptionGUI::buildAudioVideoPanel()
{
	float sizeX = Settings::getResolution().x;
	float sizeY = Settings::getResolution().y*0.9;
	float posX = 0;
	float posY = Settings::getResolution().y *0.1;

	this->audioVideoPanel = tgui::Panel::create();
	audioVideoPanel->setBackgroundColor(sf::Color::Blue);
	audioVideoPanel->setSize(sizeX,sizeY);
	audioVideoPanel->setPosition(posX,posY);

	Globals::getGUI()->add(audioVideoPanel);
	
	tgui::Grid::Ptr audioVideoGrid = tgui::Grid::create();// (*audioPanel);
	audioVideoGrid->setSize(100,200);
	audioVideoGrid->setPosition(40,40);
	//audioVideoGrid->setPosition((tgui::bindWidth(audioVideoPanel) - tgui::bindWidth(audioVideoGrid)) / 2.0f, (tgui::bindHeight(audioVideoPanel) - tgui::bindHeight(audioVideoGrid)) / 2.0f);
	audioVideoPanel->add(audioVideoGrid);	

	tgui::TextBox::Ptr audioVideoBackground = tgui::TextBox::create(Settings::getDataFolderBasePath() + "gui/widgets/Options.conf");
	audioVideoBackground->setSize(sizeX, sizeY);
	audioVideoPanel->add(audioVideoBackground);
	audioVideoBackground->moveToBack();
	audioVideoBackground->setReadOnly();

	this->musicVolume = tgui::Slider::create(Settings::getDataFolderBasePath()+"gui/widgets/Options.conf");
	musicVolume->setMinimum(0);
	musicVolume->setMaximum(100);
	musicVolume->setValue(Settings::getMusicVolume());
	musicVolume->setSize(300, 20);

	this->sfxVolume = tgui::Slider::create(Settings::getDataFolderBasePath()+"gui/widgets/Options.conf");
	sfxVolume->setMaximum(100);
	sfxVolume->setValue(Settings::getSoundVolume());
	sfxVolume->setSize(300, 20);

	tgui::Label::Ptr musicLabel = tgui::Label::create(Settings::getDataFolderBasePath()+"gui/widgets/Options.conf");
	musicLabel->setText("Music Volume");
	musicLabel->setTextSize(Settings::getGuiTextSize());
	musicLabel->setSize(100, 50);

	audioVideoGrid->add(musicLabel);
	audioVideoGrid->addWidget(musicLabel, 0, 0);
	audioVideoGrid->add(musicVolume);
	audioVideoGrid->addWidget(musicVolume, 0, 1);
	
	tgui::Label::Ptr sfxLabel = tgui::Label::copy(musicLabel);
	sfxLabel->setText("Sound Volume");
	audioVideoGrid->add(sfxLabel);
	audioVideoGrid->addWidget(sfxLabel, 1, 0);
	audioVideoGrid->add(sfxVolume);
	audioVideoGrid->addWidget(sfxVolume, 1, 1);

	tgui::Label::Ptr resLabel = tgui::Label::copy(musicLabel);
	resLabel->setText("Resolution");
	audioVideoGrid->add(resLabel);
	audioVideoGrid->addWidget(resLabel, 2, 0);

	resolutions = tgui::ComboBox::create(Settings::getDataFolderBasePath() + "gui/widgets/Options.conf");
	std::vector<sf::VideoMode> modes = sf::VideoMode::getFullscreenModes();
	resolutions->setSize(200, 20);

	int currentModeIndex = -1;
	BOOST_LOG_TRIVIAL(info) << "Enumerating Video modes ";
	for (int i = 0; i < modes.size(); i++)
	{
		std::stringstream ss(std::stringstream::in | std::stringstream::out);
		ss << modes[i].width << " X " << modes[i].height << "@" << modes[i].bitsPerPixel;
		resolutions->addItem(ss.str(), std::to_string(i));
		if (modes[i].width == Settings::getResolution().x && modes[i].height == Settings::getResolution().y)
			currentModeIndex = i;
	}
	resolutions->setSelectedItemById(std::to_string(currentModeIndex));
	audioVideoGrid->add(resolutions);
	audioVideoGrid->addWidget(resolutions, 2, 1);

	fullscreen = tgui::Checkbox::create(Settings::getDataFolderBasePath() + "gui/widgets/Options.conf");

	if (Settings::isFullscreen())
		fullscreen->check();
	else fullscreen->uncheck();
	fullscreen->setText("Fullscreen");
	fullscreen->setTextSize(Settings::getGuiTextSize());
	audioVideoGrid->add(fullscreen);
	audioVideoGrid->addWidget(fullscreen, 2, 2);

	vSync = tgui::Checkbox::copy(fullscreen);
	vSync->setText("VerticalSync");
	if (Settings::isVSync())
		vSync->check();
	else vSync->uncheck();
	audioVideoGrid->add(vSync);
	audioVideoGrid->addWidget(vSync, 2, 3);

	tgui::Button::Ptr apply = tgui::Button::create(Settings::getDataFolderBasePath() + "gui/widgets/Options.conf");
	apply->setTextSize(Settings::getGuiTextSize());
	//apply->setSize(100, 50);
	apply->setText("Apply");
	apply->connect("pressed", &OptionGUI::applyVideoSettings, this);
	audioVideoGrid->add(apply);
	audioVideoGrid->addWidget(apply, 2, 4);
}

void OptionGUI::buildGeneralPanel()
{
	// General
	this->generalPanel = tgui::Panel::create();
	generalPanel->setBackgroundColor(sf::Color::Cyan);
	generalPanel->setSize(Settings::getResolution().x, (Settings::getResolution().y/6)*5);
	generalPanel->setPosition(0,Settings::getResolution().y/6);
	Globals::getGUI()->add(generalPanel);
    //tgui::Grid::Ptr generalGrid(*generalPanel);

	

}
void OptionGUI::buildNetworkPanel()
{

}
void OptionGUI::buildProfilePanel()
{
	this->profilePanel = tgui::Panel::create();
	profilePanel->setBackgroundColor(sf::Color::Red);
	profilePanel->setSize(Settings::getResolution().x, Settings::getResolution().y *0.8);
	profilePanel->setPosition(0, Settings::getResolution().y *0.2);
	Globals::getGUI()->add(profilePanel);

	tgui::Label::Ptr nameLabel = tgui::Label::create(Settings::getDataFolderBasePath() + "gui/widgets/Options.conf");
	nameLabel->setText("Name: " + ProfileContainer::getInstance()->getActiveProfile()->getName());
	nameLabel->setTextSize(16);
	profilePanel->add(nameLabel);

}


void OptionGUI::showAudioVideo()
{
	this->generalPanel->hide();
	this->audioVideoPanel->show();
	this->profilePanel->hide();
}
void OptionGUI::showGeneral()
{
	this->generalPanel->show();
	this->audioVideoPanel->hide();
	this->profilePanel->hide();
}
void OptionGUI::showProfile()
{
	this->generalPanel->hide();
	this->audioVideoPanel->hide();
	this->profilePanel->show();
}

void OptionGUI::restore()
{
	Globals::getGUI()->removeAllWidgets();
	std::cout << "\nGUITEST0";
	buildPanelSelection();
	buildControlPanel(); std::cout << "\nGUITEST2";
	buildGeneralPanel();std::cout << "\nGUITEST3";
	buildAudioVideoPanel(); std::cout << "\nGUITEST4";
	buildProfilePanel();
}

void OptionGUI::applyVideoSettings()
{
	sf::VideoMode temp = sf::VideoMode::getFullscreenModes()[std::stoi(resolutions->getSelectedItemId().toAnsiString())];
	Settings::setResolution(sf::Vector2i(temp.width, temp.height));
	Settings::enableFullscreen(this->fullscreen->isChecked());
	Settings::enableVSync(this->vSync->isChecked());
	Globals::resetWindow();
	restore();
	showAudioVideo();


}
