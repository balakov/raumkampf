#include "gui/mainmenuGui.h"	
#include "settings.h"
#include "globals.h"
#include "map\mapcontainer.h"

MainMenuGUI::MainMenuGUI(MainMenu* model)
{


	BOOST_LOG_TRIVIAL(info) << "Building MainMenu GUI";
	//this->guiHandle = gui;
	this->model = model;
	rebuild();
	BOOST_LOG_TRIVIAL(info) << "..DONE";

}

void MainMenuGUI::update()
{



}

void MainMenuGUI::rebuild()
{

	tgui::Grid::Ptr grid = tgui::Grid::create();
	grid->setSize(Settings::getResolution().x, Settings::getResolution().y*0.25);
	grid->setPosition(0, Settings::getResolution().y*0.75);

	std::string test = Settings::getDataFolderBasePath() + "gui/widgets/Black.conf";
	tgui::Button::Ptr host = tgui::Button::create(test);
	host->setSize(100, 50);
	host->setTextSize(Settings::getGuiTextSize());
	host->setText("Host");

	tgui::Button::Ptr join = tgui::Button::copy(host);
	join->connect("pressed", &MainMenuGUI::showIPEntry, this);
	join->setText("Join");

	tgui::Button::Ptr hangar = tgui::Button::copy(host);
	hangar->setText("Hangar");
	hangar->connect("pressed", &MainMenu::toHangar, model);
	tgui::Button::Ptr options = tgui::Button::copy(host);
	options->setText("Options");
	options->connect("pressed", &MainMenu::toOptions, model);
	tgui::Button::Ptr profile = tgui::Button::copy(host);
	profile->setText("Profile");
	tgui::Button::Ptr quit = tgui::Button::copy(host);
	quit->setText("Quit");
	quit->connect("pressed", &MainMenu::toQuit, model);

	host->connect("pressed", &MainMenuGUI::showHostOptions, this);
	Globals::getGUI()->add(grid);
	grid->add(host);
	grid->add(join);
	grid->add(hangar);
	grid->add(options);
	grid->add(profile);
	grid->add(quit);
	grid->addWidget(host, 0, 0);
	grid->addWidget(join, 0, 1);
	grid->addWidget(hangar, 0, 2);
	grid->addWidget(options, 0, 3);
	grid->addWidget(profile, 0, 4);
	grid->addWidget(quit, 0, 5);

	// join options
	ipEntry = tgui::TextBox::create(Settings::getDataFolderBasePath() + "gui/widgets/Black.conf");
	ipEntry->addText("127.0.0.1");
	ipEntry->setPosition(tgui::bindWidth(*(Globals::getGUI()))*0.2, tgui::bindHeight(*(Globals::getGUI()))*0.2);
	ipEntry->hide();
	ipEntered = tgui::Button::create();;
	ipEntered->setText("Connect");
	ipEntered->connect("pressed", &MainMenu::toJoin, model,  ipEntered->getText());
	ipEntered->hide();

	Globals::getGUI()->add(ipEntry);
	Globals::getGUI()->add(ipEntered);

	// host options
	hostPanel = tgui::Panel::create();
	hostPanel->setPosition(200, 200);
	hostPanel->setSize(400, 200);
	Globals::getGUI()->add(hostPanel);

	tgui::TextBox::Ptr hostBg = tgui::TextBox::create(Settings::getDataFolderBasePath() + "gui/widgets/Black.conf");
	hostBg->setSize(tgui::bindWidth(hostPanel), tgui::bindHeight(hostPanel));
	hostPanel->add(hostBg);

	tgui::Grid::Ptr hostGrid = tgui::Grid::create();
	hostPanel->add(hostGrid);

	gameName = tgui::EditBox::create(Settings::getDataFolderBasePath() + "gui/widgets/Black.conf");
	gameName->setSize(tgui::bindWidth(hostPanel), 32);
	gameName->setDefaultText("Unnamed Game");
	hostGrid->add(gameName);
	hostGrid->addWidget(gameName, 0, 0);

	mapSelection = tgui::ComboBox::create(Settings::getDataFolderBasePath() + "gui/widgets/Black.conf");
	mapSelection->setSize(tgui::bindWidth(hostPanel), 32);
	std::vector<std::string> maps = MapContainer::getInstance()->getAvailableMaps();
	for (int i = 0; i < maps.size(); i++)
	{
		mapSelection->addItem(maps[i]);
		mapSelection->setSelectedItem(maps[i]);
	}

	hostGrid->add(mapSelection);
	hostGrid->addWidget(mapSelection, 1, 0);

	startAndConnectButton = tgui::Button::create(Settings::getDataFolderBasePath() + "gui/widgets/Black.conf");
	startAndConnectButton->setSize(tgui::bindWidth(hostPanel)*0.5, 32);
	startAndConnectButton->setText("Start");
	startAndConnectButton->connect("pressed", &MainMenu::toHost, model);
	startAndConnectButton->setTextSize(16);
	hostGrid->add(startAndConnectButton);
	hostGrid->addWidget(startAndConnectButton,2,0);

	cancelButton = tgui::Button::copy(startAndConnectButton);
	cancelButton->setText("Cancel");
	hostGrid->add(cancelButton);
	hostGrid->addWidget(cancelButton,2,1);

	hostPanel->hide();
}

void MainMenuGUI::showIPEntry()
{
	ipEntry->show();
	ipEntered->show();


}

void MainMenuGUI::showHostOptions()
{
	hostPanel->show();

}