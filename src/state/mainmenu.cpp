#include "state/mainmenu.h"
#include "state/ingame.h"
#include "state/options.h"
#include "state/edit.h"
#include "state/room.h"
#include "state/hangar.h"
#include "TGUI/TGUI.hpp"
#include "settings.h"
#include "gui/mainmenuGui.h"
#include "globals.h"
MainMenu::MainMenu(State* caller)
{

    mCaller = caller;
    loadTitleAnimation();
	bgmusic.openFromFile(Settings::getDataFolderBasePath()+"snd/test2.ogg");
	bgmusic.setVolume(Settings::getMusicVolume());
	bgmusic.play();


}


State * MainMenu::loop()
{
	bgmusic.play();
	Center_of_screen_X = Globals::getWindow()->getSize().x/2-100;
	Center_of_screen_Y = Globals::getWindow()->getSize().y/2+50;
    //create an empty black image onto which the starfield will be painted every frame
    sf::Image starsImage;
    starsImage.create(Settings::getResolution().x, Settings::getResolution().y, sf::Color::Black);

    sf::Texture starsTexture;
    starsTexture.loadFromImage(starsImage);
    starsTexture.setSmooth(false);

    sf::Sprite starsSprite;
    starsSprite.setTexture(starsTexture);
    starsSprite.setPosition(0, 0);

	Globals::getGUI()->removeAllWidgets();
	MainMenuGUI mm_gui(this);

    Globals::getWindow()->resetGLStates();
    sf::View view(sf::FloatRect(0, 0, Globals::getWindow()->getSize().x, Globals::getWindow()->getSize().y));
    view.setViewport(sf::FloatRect(0, 0, 1,1));
    Globals::getWindow()->setView(view);


    // Start the game loop
    while ( Globals::getWindow()->isOpen() )
    {
        // Process events
        sf::Event event;

        while (Globals::getWindow()->pollEvent( event ) )
        {

			Globals::getGUI()->handleEvent(event);
            // Close window : exit
            if ( event.type == sf::Event::Closed )
            {
                Globals::getWindow()->close();
            }
            if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Escape))
				Globals::getWindow()->close();



        }

		if (this->hasStateChanged)
		{
			hasStateChanged = false;	// The new state could attempt to jump back into the loop function
			return this->newState;
		}

        // Clear screen
        Globals::getWindow()->clear();
        drawTitleAnimation();

		Globals::getGUI()->draw();
        // Update the window
        Globals::getWindow()->display();
    }

    return nullptr;

}

void MainMenu::loadTitleAnimation()
{

    for(int i=0; i < 128; i++)
    {
		star_x[i]= rand() % 1000 + Globals::getWindow()->getSize().x;
        star_y[i] = rand() % 600 - Globals::getWindow()->getSize().y;
        star_z[i] = rand() % 900 + 100;
        star_zv[i]= -(rand() % 450)/100.0f - 0.5;
    }

    Center_of_screen_X=Globals::getWindow()->getSize().x/2;
    Center_of_screen_Y=Globals::getWindow()->getSize().y/2;

	t_title.loadFromFile(Settings::getDataFolderBasePath()+"img/title/title.png");
    t_masterMask.loadFromFile(Settings::getDataFolderBasePath()+"img/title/mask.png");
    t_planetSurface.loadFromFile(Settings::getDataFolderBasePath()+"img/title/planetSurface.png");
    t_planetSurface.setRepeated(true);
    t_planetMask.loadFromFile(Settings::getDataFolderBasePath()+"img/title/planetMask.png");
    t_ship.loadFromFile(Settings::getDataFolderBasePath()+"img/title/shp_0.png");

    title.setTexture(t_title);
    title.setPosition(sf::Vector2f(160,0));
    masterMask.setTexture(t_masterMask);


    planetSurface.setTexture(t_planetSurface);
    planetSurface.setPosition(sf::Vector2f(-100,100));
    planetSurface.setRotation(-15.5);

    planetMask.setPosition(sf::Vector2f(0,0));
    planetMask.setTexture(t_planetMask);
    ship_0.setTexture((t_ship));
    ship_1.setTexture(t_ship);
    ship_2.setTexture(t_ship);

    ship_0.setPosition(sf::Vector2f(400,100));
    ship_0.setScale(0.6f,0.6f);
    ;

    ship_1.setPosition(sf::Vector2f(300,250));
    ship_1.setScale(0.2f,0.2f);
    ship_1.setRotation(35.8);

    ship_2.setPosition(sf::Vector2f(260,210));
    ship_2.setScale(0.1f,0.1f);
    ship_2.setRotation(15.5);

    psx=1000000;
    psy=0;
}

void MainMenu::drawTitleAnimation()
{
    psx-=0.2;
    planetSurface.setTextureRect(sf::IntRect(psx, psy, 800, 600));
    Globals::getWindow()->draw(planetSurface);
    Globals::getWindow()->draw(planetMask);

    for(int i=0; i < 128; i++)
    {



/// 	  (move the star closer)
        star_z[i] = star_z[i] - star_zv[i];

        ///  (calculate screen coordinates)
        star_screenx[i] = star_x[i] / star_z[i] * 100;
        star_screeny[i] = star_y[i] / star_z[i] * 100 + Center_of_screen_Y;


        if ((star_screenx[i] < 220))
        {
            star_x[i]= rand() % 2000 + 1000;
            star_y[i] = rand() % 2000 - 1000;
            star_z[i] = rand() % 900 + 100;
            star_zv[i]= -(rand() % 450)/100.0f - 0.5;
        }


        sf::CircleShape circle(1, 4);
        circle.setOrigin(1,1);
        circle.setPosition(sf::Vector2f(star_screenx[i], star_screeny[i]));
        int b =  (255/5) * star_zv[i] * (1000.0f / star_z[i]);
        circle.setFillColor(sf::Color(255,255,255,b));
        Globals::getWindow()->draw(circle);

    }




    Globals::getWindow()->draw(masterMask);
    Globals::getWindow()->draw(ship_2);
    Globals::getWindow()->draw(ship_1);
    Globals::getWindow()->draw(ship_0);
    Globals::getWindow()->draw(title);


}
void MainMenu::toOptions()
{
	bgmusic.stop();
	BOOST_LOG_TRIVIAL(info) << "Switching to Options ";
	this->hasStateChanged = true;
	this->newState = new Options(this);
}
void MainMenu::toQuit()
{
	std::cout << "\n QUIT";
	this->hasStateChanged = true;
	this->newState = nullptr;
}
void MainMenu::toHost()
{
	BOOST_LOG_TRIVIAL(info) << "Switching to Room (as Host) ";
	Raumkampf::Room* room = new Raumkampf::Room(this);
	room->startServer();
	room->connect("127.0.0.1");
	this->hasStateChanged = true;
	this->newState = room;
}

void MainMenu::toHangar()
{

	BOOST_LOG_TRIVIAL(info) << "Switching to Hangar ";
	Hangar* hangar = new Hangar(this);
	this->hasStateChanged = true;
	this->newState = hangar;
}

void MainMenu::toJoin(std::string ip)
{
	RakNet::SystemAddress address;
	BOOST_LOG_TRIVIAL(info) << "Joining " << ip;
	Raumkampf::Room* room = new Raumkampf::Room(this);
	room->connect(ip);
	this->hasStateChanged = true;
	this->newState = room;
}