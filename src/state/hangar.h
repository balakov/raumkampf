#pragma once

#include <iostream>
#include "state.h"
#include "ship/clientship.h"


class Hangar:public State
{
public:
    Hangar(State* caller);

    State * loop();

	void toEditor(std::string shipToEdit);
private:

	sf::Music bgmusic;
	std::string selectedShip;
};
