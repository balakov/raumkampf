#pragma once

#include <iostream>
#include "state.h"
#include "ship/clientship.h"
#include "network\client.h"

class Edit:public State, Client
{
private:
	bool m_hasShip;
	KeyMap* m_keyMap;
	sf::Music bgmusic;
	ClientShip* ship;
	std::string shipDefinitionFile;

public:
    Edit(State* caller, std::string shipDefinitionFile);
	~Edit();
    State * loop();
	void back();
	void saveShip();
	void notifyModification();
	void sendDeviceAdded(int slot, std::string deviceNAme);
	void sendDeviceRemoved(int slot);
	void sendDeviceConfig(int slot);
	void sendClearAll();
	bool hasShip()
	{
		if (ship == nullptr)
		{
			BOOST_LOG_TRIVIAL(info) << "noship";
			return false;
		}
		else {  return true; }
	}

	ClientShip* getShip() { return ship; }
	KeyMap* getKeyMap(){return m_keyMap;}
	std::vector<std::string> getAvailableDevices();

};
