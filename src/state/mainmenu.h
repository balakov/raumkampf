#pragma once

#include <iostream>
#include "state.h"

class MainMenu: public State
{
public:

    MainMenu(State* caller);
    State *  loop();
	void toOptions();
	void toQuit();
	void toHost();
	void toHangar();
	void toJoin(std::string ip);
private:

    void loadTitleAnimation();
    void drawTitleAnimation();
    sf::Texture t_ship;
    sf::Texture t_title;
    sf::Texture t_planetSurface, t_planetMask, t_masterMask;
    sf::Sprite ship_0, ship_1, ship_2, planetSurface, planetMask, masterMask, title;

	sf::Music bgmusic;
    float psy,psx;

    float star_x[128];
    float star_y[128];
    float star_z[128];

///(location of stars on the screen)
    int star_screenx[128];
    int star_screeny[128];

///(velocity of stars)
    float star_zv[128];

    int Center_of_screen_X;
    int Center_of_screen_Y;
};
