#include "state/options.h"
#include "state/MainMenu.h"
#include "TGUI/TGUI.hpp"
#include "settings.h"
#include "globals.h"
#include <boost/log/trivial.hpp>
#include "gui\optiongui.h"
Options::Options(State* caller)
{
	mCaller = caller;
	bgmusic.openFromFile(Settings::getDataFolderBasePath()+"snd/newbit2.wav");
	bgmusic.setLoop(true);
	bgmusic.setVolume(Settings::getMusicVolume());
	bgmusic.play();


}

State * Options::loop()
{
	OptionGUI* optiongui = new OptionGUI(this);	

    while ( Globals::getWindow()->isOpen() )
    {

        // Process events
        sf::Event event;

        while (Globals::getWindow()->pollEvent( event ) )
        {
			Globals::getGUI()->handleEvent(event);

			if (hasStateChanged)
			{
				bgmusic.stop();
				return mCaller;
			}
            if (event.type == sf::Event::Closed)
                Globals::getWindow()->close();
			else if(event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
			{
				bgmusic.stop();
				return mCaller;
			}

        }
        Globals::getWindow()->clear();
		Globals::getGUI()->draw();
        Globals::getWindow()->display();
    }
    return mCaller;

}

void Options::toMainMenu()
{
	hasStateChanged = true;
}

