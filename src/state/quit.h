#pragma once

#include <iostream>
#include "state.h"

class Quit:public State
{
public:
    Quit(State* caller);
    State * loop();
};
