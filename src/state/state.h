#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <TGUI\TGUI.hpp>
#include "gui\gui.h"
class State
{

protected:
    
    State* mCaller;
	State* newState;

	bool hasStateChanged;

public:
	State();
	virtual ~State(){};
	//static tgui::Gui* gui;
	//static sf::Font* mFont;
	//static sf::RenderWindow* mWin;
    virtual State * loop()=0;
	//bool restoreWindow();
};


