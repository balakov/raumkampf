#pragma once

#include <iostream>
#include "state.h"
#include "TGUI/TGUI.hpp"
class Options:public State
{
private:

    State * mCaller;
	sf::Music bgmusic;	

public:

    Options(State* caller);
    State * loop();
	void toMainMenu();
};
