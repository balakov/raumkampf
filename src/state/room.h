#pragma once
#include "state.h"
#include "FileListTransfer.h"
#include "FileOperations.h"
#include "IncrementalReadInterface.h"
#include "ReplicaManager3.h"
#include "NetworkIDManager.h"
#include "network/clientReplicaManager.h"
#include "SocketLayer.h"
#include "network/fileProgress.h"

#include "TGUI/TGUI.hpp"
#include "clientgame.h"
#include "ingame.h"

namespace Raumkampf{
class Room:public State
{
public:
    Room(State* caller);
	~Room();
    State * loop();
	bool initGui();
	bool startServer();
	bool connect(std::string ip);
	bool setMap(std::string mapName);
	
	void forceStart();
	void join(int slot);
	void offerShip();
	void sendChatMessage(std::string msg);

	Ingame* ingameState;

	/*
	RakNet::IncrementalReadInterface incrementalReadInterface;
	RakNet::FileListTransfer flt;
	RakNet::FileList fileList;

	static const int SERVER_PORT=12345;
	NetworkIDManager networkIdManager;
	RakNet::RakPeerInterface *rakPeer;
	ClientReplicaManager* replicaManager;
	SystemAddress serverAddress;
	RakNet::SocketDescriptor sd;
	FileProgress testFileListProgress;

	*/
private:

	bool hasFocus;	





};
}