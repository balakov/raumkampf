
#include "BitStream.h"
#include "MessageIdentifiers.h"

#include "RakSleep.h"
#include "FormatString.h"
#include "RakString.h"
#include "GetTime.h"

#include "Getche.h"
#include "Rand.h"
#include "VariableDeltaSerializer.h"
#include "Gets.h"
#include <process.h>


#include "map/map.h"
#include "network/messages.h"
#include "keymap.h"
#include "ship/hullfactory.h"
#include "ship/devicefactory.h"
#include "settings.h"

#include "globals.h"
#include "state/room.h"
#include "state/ingame.h"
#include "state/mainmenu.h"
#include "gui/roomGui.h"
#include <boost/log/trivial.hpp>
using namespace RakNet;

Raumkampf::Room::Room(State* caller)
{

	ClientGame::reset(); // in case a previous game occupies the static instance
	hasFocus = true;
	hasStateChanged = false;
	mCaller = caller;
	ingameState = new Ingame(this, true);	
	// configure Ingame
	/*

	ingameState->rakPeer = RakNet::RakPeerInterface::GetInstance();
	ingameState->replicaManager = new ClientReplicaManager();
	ingameState->sd.port = 0;
	ingameState->sd.socketFamily = AF_INET; // Only IPV4 supports broadcast on 255.255.255.255
	// Start RakNet, up to 32 connections if the server
	ingameState->rakPeer->Startup(32, &ingameState->sd, 1);
	ingameState->rakPeer->AttachPlugin(ingameState->replicaManager);
	ingameState->rakPeer->AttachPlugin(&flt);
	ingameState->rakPeer->SetSplitMessageProgressInterval(9);
	flt.AddCallback(&testFileListProgress);
	
	replicaManager->SetNetworkIDManager(&networkIdManager);
	
	rakPeer->SetMaximumIncomingConnections(32);
	BOOST_LOG_TRIVIAL(info) << "Starting Network Service. GUID: " <<  rakPeer->GetMyGUID().ToString();
*/
	//gui->setGlobalFont(Settings::getDataFolderBasePath()+"font/FixedSysEx.ttf");

}

Raumkampf::Room::~Room()
{


}
void Raumkampf::Room::forceStart()
{
	hasStateChanged = true;
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_FORCE_START);
	ingameState->rakPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, ingameState->serverAddress, false);
	newState = ingameState;//new Ingame(this, this->rakPeer, this->replicaManager, serverAddress);

}
void Raumkampf::Room::join(int slot)
{
	BOOST_LOG_TRIVIAL(info) << "Joining slot " << slot;
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_JOIN_ROOM_SLOT);
	bsOut.Write(slot);
	ingameState->rakPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, ingameState->serverAddress, false);
}

void Raumkampf::Room::offerShip()
{
	BOOST_LOG_TRIVIAL(info) << "Offering Ship definition";
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_REQ_SET_SHIP_DEFINITION);
	ingameState->rakPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, ingameState->serverAddress, false);
}
void Raumkampf::Room::sendChatMessage(std::string msg)
{
	BOOST_LOG_TRIVIAL(info) << "Sending chat message: " << msg;
	RakNet::BitStream bsOut;
	RakString message(msg.c_str());
	bsOut.Write((RakNet::MessageID)ID_CHAT_MSG);
	bsOut.Write(message);
	ingameState->rakPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, ingameState->serverAddress, false);
}
bool Raumkampf::Room::startServer()
{

	BOOST_LOG_TRIVIAL(info) << "Starting Server";
	std::string temp = "Server.exe ";
	temp.append(" multiplayer");
	temp.append(" 123456 ");
	temp.append(ingameState->rakPeer->GetMyGUID().ToString());
	temp.append(" test");
	temp.append(" false");
	Util::startProcess(temp);

	RakSleep(1000); // Wait for server to start up
	ingameState->serverAddress.FromStringExplicitPort("127.0.0.1", ingameState->SERVER_PORT);

	return true; 
}

bool Raumkampf::Room::connect(std::string ip)
{
	BOOST_LOG_TRIVIAL(info) << "connecting to Server";
	ingameState->serverAddress.FromStringExplicitPort("127.0.0.1", ingameState->SERVER_PORT);
	ingameState->rakPeer->Connect("127.0.0.1", ingameState->SERVER_PORT, 0, 0, 0);
	return true;
}

State * Raumkampf::Room::loop()
{

	BOOST_LOG_TRIVIAL(info) << "Starting Room loop";
	// Enter infinite loop to run the system
	RakNet::Packet *packet;
	RoomGUI* roomgui = new RoomGUI(this);

	// run the program as long as the window is open
	while (Globals::getWindow()->isOpen())
	{

		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (Globals::getWindow()->pollEvent(event))
		{
			Globals::getGUI()->handleEvent(event);

			if (event.type == sf::Event::Closed)
				Globals::getWindow()->close();

			if (event.type == sf::Event::KeyPressed && event.key.code==sf::Keyboard::Escape)
			{
				RakNet::BitStream bsOut;
				bsOut.Write((RakNet::MessageID)ID_FORCE_STOP);
				ingameState->rakPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, ingameState->serverAddress, false);
				return new MainMenu(this);
			}

			if (event.type  == sf::Event::GainedFocus)			
				hasFocus=true;

			if (event.type  == sf::Event::LostFocus)
				hasFocus=false;
		}
		if (hasStateChanged)
		{
			hasStateChanged = false;
			delete roomgui;
			Globals::getGUI()->removeAllWidgets();
			return newState;
		}

		for (packet = ingameState->rakPeer->Receive(); packet; ingameState->rakPeer->DeallocatePacket(packet), packet = ingameState->rakPeer->Receive())
		{
			switch (packet->data[0])
			{
			case ID_CONNECTION_ATTEMPT_FAILED:
				printf("ID_CONNECTION_ATTEMPT_FAILED\n");
				break;
			case ID_NO_FREE_INCOMING_CONNECTIONS:
				printf("ID_NO_FREE_INCOMING_CONNECTIONS\n");
				break;
			case ID_CONNECTION_REQUEST_ACCEPTED:
				printf("ID_CONNECTION_REQUEST_ACCEPTED\n");
				break;
			case ID_NEW_INCOMING_CONNECTION: // message is received when new system connects
				printf("ID_NEW_INCOMING_CONNECTION from %s\n", packet->systemAddress.ToString());
				break;
			case ID_DISCONNECTION_NOTIFICATION:
				printf("ID_DISCONNECTION_NOTIFICATION\n");
				break;
			case ID_CONNECTION_LOST:
				printf("ID_CONNECTION_LOST\n");
				break;
			case ID_ADVERTISE_SYSTEM:
				// The first conditional is needed because ID_ADVERTISE_SYSTEM may be from a system we are connected to, but replying on a different address.
				// The second conditional is because AdvertiseSystem also sends to the loopback
				if (ingameState->rakPeer->GetSystemAddressFromGuid(packet->guid) == RakNet::UNASSIGNED_SYSTEM_ADDRESS &&
					ingameState->rakPeer->GetMyGUID() != packet->guid)
				{
					printf("Connecting to %s\n", packet->systemAddress.ToString(true));
					ingameState->rakPeer->Connect(packet->systemAddress.ToString(false), packet->systemAddress.GetPort(), 0, 0);
				}
				break;
			case ID_ACK_SET_SHIP_DEFINITION:
				{
								// Print sending progress notifications
				unsigned short id;
				RakNet::BitStream bsIn(packet->data,packet->length,false);
					bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
					bsIn.Read(id);
					std::cout << "got id: " <<id;
					std::string shipdef = Settings::getDataFolderBasePath().append("save/testprofile1/slot0.xml");
				
				RakNet::RakString file = shipdef.c_str();
				unsigned int fileLength = GetFileLength(file);
				if (fileLength==0)
				{
					printf("Test file %s not found.\n", file.C_String());
				}
				else printf("Test file %s found.\n", file.C_String());
				ingameState->fileList.AddFile(file.C_String(), file.C_String(), 0, fileLength, fileLength, FileListNodeContext(0, 0, 0, 0), true);
				ingameState->flt.Send(&ingameState->fileList, ingameState->rakPeer, ingameState->serverAddress, id, HIGH_PRIORITY, 0, &ingameState->incrementalReadInterface, 2000000);
				
				}break;
			case ID_CHAT_MSG:
				{

				RakNet::BitStream bsIn(packet->data,packet->length,false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				RakString msg;
				bsIn.Read(msg);
				std::cout << "got chat msg: " <<msg;
				roomgui->addChatMessage(msg.C_String());

				}break;
			case ID_SERVER_STATE:
				{
					std::cout << "Received server state";
				}break;
			case ID_SND_RECEIPT_LOSS:
			case ID_SND_RECEIPT_ACKED:
				{
					uint32_t msgNumber;
					memcpy(&msgNumber, packet->data+1, 4);

					DataStructures::List<Replica3*> replicaListOut;
					ingameState->replicaManager->GetReplicasCreatedByMe(replicaListOut);
					unsigned int idx;
					for (idx=0; idx < replicaListOut.Size(); idx++)
					{
						;//((TestEntity*)replicaListOut[idx])->NotifyReplicaOfMessageDeliveryStatus(packet->guid,msgNumber, packet->data[0]==ID_SND_RECEIPT_ACKED);
					}
				}
				break;
			}
		}

		Globals::getWindow()->clear();
		roomgui->update();
		Globals::getGUI()->draw();
		Globals::getWindow()->display();

	}
	delete Globals::getWindow();
	ingameState->rakPeer->Shutdown(100, 0);
	RakNet::RakPeerInterface::DestroyInstance(ingameState->rakPeer);
	return 0;

}

