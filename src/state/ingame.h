#pragma once
#include "state.h"
#include "FileListTransfer.h"
#include "FileOperations.h"
#include "IncrementalReadInterface.h"
#include "ReplicaManager3.h"
#include "NetworkIDManager.h"
#include "network/clientReplicaManager.h"
#include "SocketLayer.h"
#include "network/fileProgress.h"
#include "gui/hud.h"
class Ingame:public State
{
public:
	Ingame();
	~Ingame();
    Ingame(State* caller, bool hosting);
	Ingame(State* caller, RakNet::RakPeerInterface* rakInterface, ClientReplicaManager* replicaManager, SystemAddress serverAddress);	
    State * loop();

	RakNet::IncrementalReadInterface incrementalReadInterface;
	RakNet::FileListTransfer flt;
	RakNet::FileList fileList;

	bool host;
	bool hasFocus;
	Hud hud;

	unsigned int id;

	static const int SERVER_PORT=12345;
	NetworkIDManager networkIdManager;
	RakNet::RakPeerInterface *rakPeer;
	ClientReplicaManager* replicaManager;
	SystemAddress serverAddress;
	RakNet::SocketDescriptor sd;
	FileProgress testFileListProgress;

};
