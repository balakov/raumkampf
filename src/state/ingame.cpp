#include "globals.h"
#include "BitStream.h"
#include "MessageIdentifiers.h"

#include "RakSleep.h"
#include "FormatString.h"
#include "RakString.h"
#include "GetTime.h"

#include "Getche.h"
#include "Rand.h"
#include "VariableDeltaSerializer.h"
#include "Gets.h"
#include <process.h>


#include "map/clientmap.h"
#include "network/messages.h"
#include "keymap.h"
#include "ship/hullfactory.h"
#include "ship/devicefactory.h"
#include "settings.h"
#include "misc/event.h"

#include "state/ingame.h"
#include "state/mainmenu.h"
#include "Thor/Particles.hpp"
#include <Thor/Animations.hpp>
#include <Thor/Vectors/PolarVector2.hpp>
#include <Thor/Math/Distributions.hpp>
using namespace RakNet;
void addFrames(thor::FrameAnimation& animation, int y, int xFirst, int xLast, float duration = 0.2f)
{
	std::cout << "deep magc";
	const int step = (xFirst < xLast) ? +1 : -1;
	xLast += step; // so yLast is excluded in the range

	for (int x = xFirst; x != xLast; x += step)
		animation.addFrame(duration, sf::IntRect(64 * x, y, 64,64));
}
Ingame::Ingame()
{

}
Ingame::~Ingame()
{
	rakPeer->Shutdown(100, 0);
	RakNet::RakPeerInterface::DestroyInstance(rakPeer);

}
Ingame::Ingame(State* caller, bool hosting)
{
	hasFocus = true;
	mCaller = caller;
	rakPeer = RakNet::RakPeerInterface::GetInstance();
	replicaManager = new ClientReplicaManager();
	sd.port=0;
	sd.socketFamily=AF_INET; // Only IPV4 supports broadcast on 255.255.255.255
	// Start RakNet, up to 32 connections if the server
	rakPeer->Startup(32,&sd,1);
	rakPeer->AttachPlugin(replicaManager);	
	rakPeer->AttachPlugin(&flt);
	rakPeer->SetSplitMessageProgressInterval(9);
	flt.AddCallback(&testFileListProgress);
	replicaManager->SetNetworkIDManager(&networkIdManager);
	rakPeer->SetMaximumIncomingConnections(32);
	printf("\nMy GUID is %s\n", rakPeer->GetMyGUID().ToString());
	
}

Ingame::Ingame(State* caller, RakNet::RakPeerInterface* rakInterface, ClientReplicaManager* replicaManager, SystemAddress serverAddress)
{
	hasFocus = true;
	mCaller = caller;
	rakPeer = rakInterface;
	this->replicaManager = replicaManager;
	this->serverAddress = serverAddress;
}

State * Ingame::loop()
{
	//ClientShip ship;
	//ship.loadFromFile(Settings::getDataFolderBasePath()+"save/testprofile1/slot0.xml");
	KeyMap keys(Settings::getDataFolderBasePath() + "save/testprofile1/slot0.binding");

	std::cout << "Starting client";	
	char ch;
	// Enter infinite loop to run the system
	RakNet::Packet *packet;
	bool quit=false;

	Globals::getWindow()->setVerticalSyncEnabled(true);
	sf::View view;
	Globals::getWindow()->setView(view);

	view.setSize(sf::Vector2f(Settings::getResolution().x, (Settings::getResolution().y/4)*3));

	view.setViewport(sf::FloatRect(0, 0, 1, 0.75));
	ClientMap map;
	std::vector<RK::Event> events;
	map.preparse(Settings::getDataFolderBasePath()+"map/defaultmap/defaultmap.xml");
	map.print();
	map.loadMonolithic();

	// ********
	// Load texture
	sf::Texture texture;
	if (!texture.loadFromFile(Settings::getHullImagePath() + "rhineIcon.png"))
		std::cout << "\n\OYVEY!";

	// Create emitter
	thor::UniversalEmitter emitter;
	emitter.setEmissionRate(30.f);
	emitter.setParticleLifetime(sf::seconds(0.8f));
	emitter.setParticleVelocity(thor::Distributions::deflect(sf::Vector2f(0,0), 360.f));

	// Create particle system, add reference to emitter
	thor::ParticleSystem system;
	system.setTexture(texture);
	system.addEmitter(thor::refEmitter(emitter));

	// Build color gradient (green -> teal -> blue)
	thor::ColorGradient gradient;
	gradient[0.f] = sf::Color(255, 0, 0);
	gradient[0.2f] = sf::Color(200, 100, 0);
	gradient[1.f] = sf::Color(0, 0, 0);

	// Create color and fade in/out animations
	thor::ColorAnimation colorizer(gradient);
	thor::FadeAnimation fader(0.1f, 0.1f);

	// Add particle affectors
	system.addAffector(thor::AnimationAffector(colorizer));
	system.addAffector(thor::AnimationAffector(fader));
	system.addAffector(thor::TorqueAffector(100.f));
	system.addAffector(thor::ForceAffector(sf::Vector2f(0.f, 100.f)));

	// Attributes that influence emitter
	thor::PolarVector2f velocity(200.f, -90.f);
	bool paused = false;
	// Create clock to measure frame time
	sf::Clock frameClock;




	// ********

	// Load image that contains animation steps
	sf::Image image;
	if (!image.loadFromFile(Settings::getDataFolderBasePath() + "img/animations/explosion_0.png"))
		std::cout << "\n\OYVEY!";
	image.createMaskFromColor(sf::Color::White);

	// Create texture based on sf::Image
	sf::Texture atexture;
	if (!atexture.loadFromImage(image))
		std::cout << "\n\OYVEY!tex";

	// Create sprite which is animated
	sf::Sprite sprite(atexture);
	sprite.setPosition(100.f, 800.f);

	// Define walk animation
	thor::FrameAnimation walk;
	addFrames(walk, 0, 0, 7);			// Frames 0..7	Right leg moves forward
	addFrames(walk, 64, 0, 7);			// Frames 6..0	Right leg moves backward
	addFrames(walk, 128, 0, 4);			// Frames 6..0	Right leg moves backward
	thor::FrameAnimation none;
	addFrames(none, 128, 4, 4);			// Frames 6..0	Right leg moves backward
	// Register animations with their corresponding durations
	thor::Animator<sf::Sprite, std::string> animator;
	animator.addAnimation("explosion", walk, sf::seconds(3.f));
	animator.addAnimation("null", none, sf::seconds(3.f));
	std::cout << "anim crwate complete";

	//*********



	// run the program as long as the window is open
	while (Globals::getWindow()->isOpen())
	{

		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (Globals::getWindow()->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				Globals::getWindow()->close();

			if (event.type == sf::Event::KeyPressed && event.key.code==sf::Keyboard::Escape)
			{
				RakNet::BitStream bsOut;
				bsOut.Write((RakNet::MessageID)ID_FORCE_STOP);
				rakPeer->Send(&bsOut,HIGH_PRIORITY,RELIABLE_ORDERED,0,serverAddress,false);
				return new MainMenu(this);
			}

			if (event.type  == sf::Event::GainedFocus)			
				hasFocus=true;

			if (event.type  == sf::Event::LostFocus)
				hasFocus=false;
		}
		for (packet = rakPeer->Receive(); packet; rakPeer->DeallocatePacket(packet), packet = rakPeer->Receive())
		{
			switch (packet->data[0])
			{
			case ID_CONNECTION_ATTEMPT_FAILED:
				printf("ID_CONNECTION_ATTEMPT_FAILED\n");
				quit=true;
				break;
			case ID_NO_FREE_INCOMING_CONNECTIONS:
				printf("ID_NO_FREE_INCOMING_CONNECTIONS\n");
				quit=true;
				break;
			case ID_CONNECTION_REQUEST_ACCEPTED:
				printf("ID_CONNECTION_REQUEST_ACCEPTED\n");
				break;
			case ID_NEW_INCOMING_CONNECTION: // message is received when new system connects
				printf("ID_NEW_INCOMING_CONNECTION from %s\n", packet->systemAddress.ToString());
				break;
			case ID_DISCONNECTION_NOTIFICATION:
				printf("ID_DISCONNECTION_NOTIFICATION\n");
				break;
			case ID_CONNECTION_LOST:
				printf("ID_CONNECTION_LOST\n");
				break;
			case ID_ADVERTISE_SYSTEM:
				// The first conditional is needed because ID_ADVERTISE_SYSTEM may be from a system we are connected to, but replying on a different address.
				// The second conditional is because AdvertiseSystem also sends to the loopback
				if (rakPeer->GetSystemAddressFromGuid(packet->guid)==RakNet::UNASSIGNED_SYSTEM_ADDRESS &&
					rakPeer->GetMyGUID()!=packet->guid)
				{
					printf("Connecting to %s\n", packet->systemAddress.ToString(true));
					rakPeer->Connect(packet->systemAddress.ToString(false), packet->systemAddress.GetPort(),0,0);
				}
				break;
			case ID_ACK_SET_SHIP_DEFINITION:
				{
								// Print sending progress notifications
				unsigned short id;
				RakNet::BitStream bsIn(packet->data,packet->length,false);
					bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
					bsIn.Read(id);
					std::cout << "got id: " <<id;
				std::string shipdef = Settings::getShipPath();
				shipdef += "editortest.xml";
				RakNet::RakString file = shipdef.c_str();
				unsigned int fileLength = GetFileLength(file);
				if (fileLength==0)
				{
					printf("Test file %s not found.\n", file.C_String());
				}
				else printf("Test file %s found.\n", file.C_String());
				fileList.AddFile(file.C_String(), file.C_String(), 0, fileLength, fileLength, FileListNodeContext(0,0,0,0), true);
				flt.Send(&fileList,rakPeer,serverAddress,id,HIGH_PRIORITY,0, &incrementalReadInterface, 2000000);
				
				}
			case ID_SERVER_STATE:
				{
					std::cout << "Received server state";
				}
			case ID_SND_RECEIPT_LOSS:
			case ID_SND_RECEIPT_ACKED:
				{
					uint32_t msgNumber;
					memcpy(&msgNumber, packet->data+1, 4);

					DataStructures::List<Replica3*> replicaListOut;
					replicaManager->GetReplicasCreatedByMe(replicaListOut);
					unsigned int idx;
					for (idx=0; idx < replicaListOut.Size(); idx++)
					{
						;//((TestEntity*)replicaListOut[idx])->NotifyReplicaOfMessageDeliveryStatus(packet->guid,msgNumber, packet->data[0]==ID_SND_RECEIPT_ACKED);
					}
				}
				break;
				// temporarily until i have a better event mechanism
				case ID_EXPLOSION:
				{
					sf::Vector2f p;
					RakNet::BitStream bsIn(packet->data, packet->length, false);
					bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
					bsIn.Read(p);
					RK::Event e;
					e.type = RK::Event::EXPLOSION;
					e.position = p;
					events.push_back(e);
				}
					break;
				
			
			}

		}

	
		map.update();

		Globals::getWindow()->clear();
		Globals::getWindow()->setView(view);
		// draw everything here...
		map.draw(Globals::getWindow());
		for(int idx=0; idx < replicaManager->GetReplicaCount(); idx++)
		{
			NetworkEntity* ne = (NetworkEntity*) replicaManager->GetReplicaAtIndex(idx);
			if(ne->getIdentifier()=="Ship")
			{
				ClientShip* ship = ((ClientShip*)replicaManager->GetReplicaAtIndex(idx));

				ship->updateGraphics(1/60.0f);
				if(ship->owner == rakPeer->GetMyGUID())
				{
					hud.bind(ship);
					sf::Vector2f cam = ship->getHull()->getPosition();
					if (cam.x < Globals::getWindow()->getSize().x/2)
						cam.x =  Globals::getWindow()->getSize().x/2;
					else if (cam.x > map.getWidth()- Globals::getWindow()->getSize().x/2)
						cam.x = map.getWidth()- Globals::getWindow()->getSize().x/2;
					if (cam.y < (Globals::getWindow()->getSize().y*0.75)/2)
						cam.y =  (Globals::getWindow()->getSize().y*0.75)/2;
					else if (cam.y > map.getHeight()- (Globals::getWindow()->getSize().y*0.75)/2)
						cam.y = map.getHeight()- (Globals::getWindow()->getSize().y*0.75)/2;
					view.setCenter(cam);
				}
				Globals::getWindow()->setView(view);
				ship->draw(Globals::getWindow());

			}
			else if(ne->getIdentifier()=="Projectile")
			{
				((ClientProjectile*)replicaManager->GetReplicaAtIndex(idx))->draw(Globals::getWindow());
			}
			else if(ne->getIdentifier()=="User")
			{
				//std::cout << "\n\nkahdfglkhsdgfsdfg\n\n";
			}
			else if(ne->getIdentifier()=="Game")
			{
				hud.drawScore(Globals::getWindow(),(ClientGame*)replicaManager->GetReplicaAtIndex(idx));
			}
			else std::cout << "It's a " << ne->getIdentifier(); 
		}
		for (RK::Event e : events)
		{
			switch (e.type)
			{
				case RK::Event::EXPLOSION: std::cout << "Kablammo!!";
					sprite.setPosition(e.position);
					animator.playAnimation("explosion"); break;
					
				default: break;
			}
		}
		events.clear();

		if(hasFocus)
		{
		if(keys.hasKeyStateChanged())
			{	std::vector<bool> s = keys.getSlotStates();
				RakNet::BitStream bsOut;
				bsOut.Write((RakNet::MessageID)ID_DEVICE_STATE);
				bsOut.Write(s.size());
				std::cout <<"\ndevice state list size: " << s.size();
				for(int idx=0; idx < s.size(); idx++)
				{
					std::cout << "," << s[idx];
					if(s[idx])
						bsOut.Write1();
					else bsOut.Write0();
				}
				rakPeer->Send(&bsOut,HIGH_PRIORITY,RELIABLE_ORDERED,0,serverAddress,false);
			}	
		}
		const sf::Time frameTime = frameClock.restart();

			system.update(frameTime);

		// Set initial particle position and velocity, rotate vector randomly by maximal 10 degrees
		emitter.setParticlePosition(Globals::getWindow()->mapPixelToCoords(sf::Mouse::getPosition(*Globals::getWindow())));
		emitter.setParticleVelocity(thor::Distributions::deflect(velocity, 360.0f));	
		//Globals::getWindow()->draw(system);
		// If no other animation is playing, play stand animation
		if (!animator.isPlayingAnimation())
			animator.playAnimation("null");

		// Update animator and apply current animation state to the sprite
		animator.update(frameTime);
		animator.animate(sprite);
		Globals::getWindow()->draw(sprite);


		hud.drawSpatialInfo(Globals::getWindow());
		hud.draw(Globals::getWindow());
		hud.drawDebugInfo(Globals::getWindow());
		Globals::getWindow()->display();
	}
	delete Globals::getWindow();

	return 0;

}

