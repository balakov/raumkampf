#include "state/mainmenu.h"
#include "state/ingame.h"
#include "state/quit.h"
#include "state/edit.h"
#include "state/hangar.h"
#include <gl\GLU.h>
#include "util.h"
#include "tinyxml/tinyxml2.h"
#include "settings.h"
#include "gui/hangarGui.h"
#include "globals.h"
Hangar::Hangar(State* caller)
{
	bgmusic.openFromFile(Settings::getDataFolderBasePath()+"snd/newbit2.wav");
	bgmusic.setLoop(true);
	bgmusic.setVolume(Settings::getMusicVolume());
	bgmusic.play();
	mCaller = caller;
	
}

State * Hangar::loop()
{
	BOOST_LOG_TRIVIAL(info) << "Starting Hangar loop";
	HangarGUI hangarGui(this);
    Globals::getWindow()->resetGLStates();
    sf::View view(sf::FloatRect(0, 0, Globals::getWindow()->getSize().x, Globals::getWindow()->getSize().y));
    view.setViewport(sf::FloatRect(0, 0, 1,1));
    Globals::getWindow()->setView(view);

    // Start the game loop
    while ( Globals::getWindow()->isOpen() )
    {
        // Process events
        sf::Event event;

        while (Globals::getWindow()->pollEvent( event ) )
        {
			Globals::getGUI()->handleEvent(event);
			if (hasStateChanged)
			{
				hasStateChanged = false;
				//this->gui->removeAllWidgets();
				return new Edit(this, selectedShip);
			}
			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Escape))
			{
				bgmusic.stop();
				return new MainMenu(this);
			}
            // Close window : exit
            if ( event.type == sf::Event::Closed )
            {
                Globals::getWindow()->close();
            }
        }
        tgui::Callback callback;

        Globals::getWindow()->clear();
		Globals::getGUI()->draw();
        Globals::getWindow()->display();
    }

    return EXIT_SUCCESS;

}

void Hangar::toEditor(std::string shipToEdit)
{
	std::cout << "it works1" << shipToEdit;
	hasStateChanged = true;
	selectedShip = shipToEdit;
}