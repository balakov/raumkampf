#include "state/mainmenu.h"
#include "state/ingame.h"
#include "state/quit.h"
#include "state/hangar.h"
#include "state/edit.h"
#include "gui\editorGui.h"
#include <gl\GLU.h>
#include "util.h"
#include "tinyxml/tinyxml2.h"
#include "settings.h"
#include "network\messages.h"
#include "globals.h"
Edit::Edit(State* caller, std::string shipDefinitionFile)
{
	bgmusic.openFromFile(Settings::getDataFolderBasePath()+"snd/newbit2.wav");
	bgmusic.setLoop(true);
	bgmusic.setVolume(Settings::getMusicVolume());
	bgmusic.play();
	mCaller = caller;

	m_hasShip = false;
	this->shipDefinitionFile = shipDefinitionFile;
	ClientGame::reset();
	std::string cmdLine = "Server.exe Editorsimulation 7777 ";
	cmdLine.append(this->rakPeer->GetMyGUID().ToString());
	cmdLine.append(" testmap false");
	Util::startProcess(cmdLine);
	RakSleep(1000);

	serverAddress.FromStringExplicitPort("127.0.0.1", 7777);
	connect(serverAddress);
	/*
	//fugly keymap loading
	ClientShip * tempship = new ClientShip();
	tempship->loadFromFile(shipDefinitionFile);
	this->m_keyMap = tempship->getKeyMap();
	*/
	this->m_keyMap = new KeyMap(Settings::getDataFolderBasePath().append("save/testprofile1/slot0.binding"));

	ship = 0;
}

Edit::~Edit()
{
	
	this->askServerToShutdown();	
	delete ship;
	Globals::getGUI()->removeAllWidgets();
	rakPeer->Shutdown(100, 0);
	RakNet::RakPeerInterface::DestroyInstance(rakPeer);


}
void Edit::back()
{
	hasStateChanged = true;
	newState = mCaller;
	this->ship->writeToFile(this->shipDefinitionFile);
	this->m_keyMap->write();
}
void  Edit::saveShip()
{
	this->ship->writeToFile(this->shipDefinitionFile);
	this->m_keyMap->write();

}
void Edit::sendDeviceAdded(int slot, std::string deviceName)
{
	BOOST_LOG_TRIVIAL(info) << "sending ID_ADDEVICE";
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_ADDDEVICE);
	bsOut.Write(slot);
	bsOut.Write(RakNet::RakString(deviceName.c_str()));
	this->rakPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, this->serverAddress, false);
}
void Edit::sendDeviceRemoved(int slot)
{
	BOOST_LOG_TRIVIAL(info) << "sending ID_REMOVEDEVICE";
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_REMOVEDEVICE);
	bsOut.Write(slot);
	this->rakPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, this->serverAddress, false);

}
void Edit::sendDeviceConfig(int slot)
{
	BOOST_LOG_TRIVIAL(info) << "sending ID_RECONFIGUREDEVICE";
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_RECONFIGUREDEVICE);
	bsOut.Write(slot);
	this->ship->getHull()->getDevice(slot)->writeConfigToBitstream(&bsOut);
	this->rakPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, this->serverAddress, false);
}

void Edit::sendClearAll()
{
	BOOST_LOG_TRIVIAL(info) << "sending ID_REMOVE_ALL_DEVICES";
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_REMOVE_ALL_DEVICES);
	this->rakPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, this->serverAddress, false);
	this->m_keyMap->clear();
}
// resend definition to server
void Edit::notifyModification()
{
	//this->ship->writeToFile(this->shipDefinitionFile);
	BOOST_LOG_TRIVIAL(info) << "writing ship";
	this->ship->writeToFile(this->shipDefinitionFile);
	this->m_keyMap->write();
	BOOST_LOG_TRIVIAL(info) << "Pushing ship";
	pushShip(this->shipDefinitionFile);
	this->ship = 0;
	
}

State * Edit::loop()
{
	BOOST_LOG_TRIVIAL(info) << "Beginning Editor loop";
    Globals::getWindow()->resetGLStates();
    sf::View view(sf::FloatRect(0, 0, Globals::getWindow()->getSize().x, Globals::getWindow()->getSize().y));
    view.setViewport(sf::FloatRect(0, 0, 1,1));
    Globals::getWindow()->setView(view);

	sf::Vector2f simPos(300, 0);
	sf::Vector2f simSize(200, 200);
	sf::View simulationScreen(sf::FloatRect(0, 0, 200,200));
	simulationScreen.setViewport(sf::FloatRect(0.75, 0.25, 0.25, 0.25));

	EditorGUI editorGui(this);
	
	pushShip(this->shipDefinitionFile);
	BOOST_LOG_TRIVIAL(info) << "editor set up";
	//editorGui.setAvailableDevices(DeviceFactory::getAvailableDevices());

	sf::Texture simBg;
	simBg.loadFromFile(Settings::getDataFolderBasePath().append("img/simgrid.png"));
	simBg.setRepeated(true);
	sf::Sprite simSprite;
	simSprite.setTexture(simBg);
	int simX = 0; int simY = 0;
	simSprite.setTextureRect(sf::IntRect(simX, simY,200,200));


    while ( Globals::getWindow()->isOpen() )
    {

		if (hasStateChanged)
		{
			hasStateChanged = false;
			return new Hangar(this);
		}
        // Process events
        sf::Event event;

        while (Globals::getWindow()->pollEvent( event ) )
        {
			Globals::getGUI()->handleEvent(event);
            // Close window : exit
            if ( event.type == sf::Event::Closed )
            {
                Globals::getWindow()->close();
            }



        }
		processPackets();

        Globals::getWindow()->clear();
		Globals::getGUI()->draw();
		Globals::getWindow()->setView(view);

		editorGui.drawDevices(Globals::getWindow());

		//fugly config polling ////////
		if (this->hasShip())
		{
			for (int i = 0; i < this->ship->getHull()->getNumberOfSlots(); i++)
			{
				if (this->ship->getHull()->getDevice(i) != nullptr)
				{
					if (this->ship->getHull()->getDevice(i)->configNeedsSendFlag)
					{
						this->sendDeviceConfig(i);
						this->ship->getHull()->getDevice(i)->configNeedsSendFlag = false;
					}
				}
			}
		}
		///////////////////////////////

		for (int idx = 0; idx < replicaManager->GetReplicaCount(); idx++)
		{
			NetworkEntity* ne = (NetworkEntity*)replicaManager->GetReplicaAtIndex(idx);
			if (ne->getIdentifier() == "Ship")
			{
				ClientShip* ship = ((ClientShip*)replicaManager->GetReplicaAtIndex(idx));
				ship->updateGraphics(0);

				if (ship->owner == rakPeer->GetMyGUID())
				{

					// Didn't have a ship before
					if (this->hasShip() == false)
					{
						this->ship = ship;
						editorGui.rebuild();
					}
					// had one, is it new?
					else if (this->ship->GetNetworkID() != ship->GetNetworkID())
					{
						this->ship = ship;
						editorGui.rebuild();
					}
					// have devices been removed or added?
					else if (this->ship->getHull()->isDirty)
					{
						editorGui.rebuild();
						this->ship->getHull()->isDirty = false;
					}
					else this->ship = ship;
					sf::Vector2f cam = ship->getHull()->getPosition();
					simulationScreen.setCenter(cam);
					Globals::getWindow()->setView(simulationScreen);
					int px = ship->getHull()->getPosition().x;
					int py = ship->getHull()->getPosition().y;
					simSprite.setTextureRect(sf::IntRect(px, py, 200, 200));
					simSprite.setPosition(px-100, py-100);
					Globals::getWindow()->draw(simSprite);
				}

				ship->draw(Globals::getWindow());

			}
		}



		if (m_keyMap->hasKeyStateChanged())
		{
			std::vector<bool> s = m_keyMap->getSlotStates();
			RakNet::BitStream bsOut;
			bsOut.Write((RakNet::MessageID)ID_DEVICE_STATE);
			bsOut.Write(s.size());
			std::cout << "\ndevice state list size: " << s.size();
			for (int idx = 0; idx < s.size(); idx++)
			{
				std::cout << s[idx];
				if (s[idx])
					bsOut.Write1();
				else bsOut.Write0();
			}
			rakPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, serverAddress, false);
		}

        Globals::getWindow()->display();
    }

    return EXIT_SUCCESS;

}

