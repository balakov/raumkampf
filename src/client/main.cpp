#include "state/state.h"
#include "state/mainmenu.h"
#include "settings.h"
#include "entity/ship/clientship.h"
#include "keymap.h"
//#include "map\mapcontainer.h"
#include "profilecontainer.h"
#include "profile.h"
#include "map/map.h"
#include "tgui/TGUI.hpp"

#include "globals.h"
int main(int argc, char** argv)
{

	Settings::load(Settings::getDataFolderBasePath()+"config.xml");
	Globals::getWindow();
	Globals::getGUI();
	//Map* map = MapContainer::getInstance()->getNextMap();
	//map->print();

	Profile* p = ProfileContainer::getInstance()->getActiveProfile();
	std::cout << "\n" << p->getName() << p->getRank();

	State* state = new MainMenu(nullptr);

	while(state!=nullptr)
	{
		State* newstate = state->loop();
		delete state;
		state = newstate;
	}

	return 0;
}