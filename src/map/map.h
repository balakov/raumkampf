#ifndef MAP_H
#define MAP_H
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include "terrain.h"
#include "Box2D/Box2D.h"
class Map
{
public:
    Map();
    virtual ~Map();
	bool preparse(std::string filename);
	void print();

	std::string getName();
	int getWidth() {return size[0];}
	int getHeight() {return size[1];}
	int getNumberOfPlayers();
	sf::Vector2i getSpawnPosition(int gameSlot);
protected:
	float gravity;
private:
	Terrain* terrain;
	std::string name;
	std::string description;
	std::vector<int> size;
	std::vector<sf::Vector2i> spawnPositions;
	int numPlayers;
	float version;

};

#endif // MAP_H
