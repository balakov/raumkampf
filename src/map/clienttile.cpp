#include "map\clienttile.h"


ClientTile::ClientTile(std::string tilefile, sf::Vector2f position)
{
	Tile::pos = position;
    maskFile = tilefile;
    isReady =  false;


}

void ClientTile::load()
{ 
	i_mask.loadFromFile(maskFile);
    t_mask.loadFromImage(i_mask);
    mask.setTexture(t_mask);
    isReady=true;
}
void ClientTile::draw(sf::RenderTarget* target)
{
    mask.setPosition((pos));
    target->draw(mask);
}