#ifndef TERRAIN_H
#define TERRAIN_H
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include "map/clienttile.h"
class Terrain
{
public:
    Terrain();
    virtual ~Terrain();
    void rebuildCache();

    //ClientTile* cache[4*4];
protected:
	int getTileAt(sf::Vector2f location);
	int mapWidthInTiles;
	int mapHeightInTiles;
	int tileSize;
private:


};

#endif // TERRAIN_H
