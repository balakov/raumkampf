#pragma once
#include "map\map.h"

class MapContainer{

public:

static MapContainer* getInstance();

Map* getNextMap();
Map* getPreviousMap();
std::vector<std::string> getAvailableMaps();
private:
	static MapContainer* instance;
	std::vector<std::string> availableMaps;
	MapContainer();
	~MapContainer();
};