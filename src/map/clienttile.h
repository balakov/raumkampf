#pragma once
#include "map\tile.h"
#include <SFML/OpenGL.hpp>

class ClientTile: public Tile
{
	public:
    ClientTile(std::string tilefile, sf::Vector2f position);
	void draw(sf::RenderTarget* target);
	void load();

private:
	sf::Image i_mask;
    sf::Texture t_mask;
    sf::Sprite  mask;
};