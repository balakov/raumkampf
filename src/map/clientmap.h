#pragma once
#include "map/map.h"
#include "map/clientterrain.h"
#include <SFML/OpenGL.hpp>
class ClientMap:public Map
{
public:
	void draw(sf::RenderWindow* win);
	void update();
	bool init();
	bool loadMonolithic();
private:
	ClientTerrain* terrain;
};