#include "map/clientmap.h"
#include <boost/log/trivial.hpp>

void ClientMap::draw(sf::RenderWindow* win)
{
	this->terrain->draw(win);
	;;
}
void ClientMap::update()
{
	;;
}
bool ClientMap::init()
{
	BOOST_LOG_TRIVIAL(info) << "Loading Terrain ";
	this->terrain = new ClientTerrain();
	this->terrain->init();
	BOOST_LOG_TRIVIAL(info) << "..DONE";
	return true;
}

bool ClientMap::loadMonolithic()
{
	BOOST_LOG_TRIVIAL(info) << "Loading Terrain Monolithic";
	this->terrain = new ClientTerrain();
	this->terrain->loadMonolithic();
	BOOST_LOG_TRIVIAL(info) << "..DONE";
	return true;


}