#include "map/map.h"
#include <iostream>
#include <boost/log/trivial.hpp>
#include "tinyxml/tinyxml2.h"
Map::Map()
{

}

Map::~Map()
{
    //dtor
}

void Map::print()
{
	BOOST_LOG_TRIVIAL(info) << "Map name: "<< this->name;
	BOOST_LOG_TRIVIAL(info) << "Version: "<< version; 
	BOOST_LOG_TRIVIAL(info) << "Gravity: " << gravity << " Map description: "<< this->description;
	BOOST_LOG_TRIVIAL(info) << "Width: "<< size[0] << "px Height: " << size[1] << "px";
	for(int i = 0; i < spawnPositions.size(); i++)
	{
		BOOST_LOG_TRIVIAL(info) << i << "#: x:" << spawnPositions[i].x << " y:" << spawnPositions[i].y;
	}
}

bool Map::preparse(std::string filename)
{
	BOOST_LOG_TRIVIAL(info) << "Loading Map from file: "<<filename;
	tinyxml2::XMLDocument* mapdefinition = new tinyxml2::XMLDocument();
	 mapdefinition->LoadFile(filename.c_str());
	if(mapdefinition->Error())
		std::cout << mapdefinition->GetErrorStr1() << mapdefinition->GetErrorStr2();
    tinyxml2::XMLElement* root;
    root = mapdefinition->FirstChildElement("map");

    if(!root)
    {
        BOOST_LOG_TRIVIAL(error) << "Not a map definition file ";
        return false;
    }

    tinyxml2::XMLElement* tmpNode = root->FirstChildElement("version");
	this->version = tmpNode->FloatAttribute("n");
	BOOST_LOG_TRIVIAL(info) << "Map Version: " << version;
    tmpNode = tmpNode->NextSiblingElement("name");
    this->name = tmpNode->FirstChild()->Value();
	BOOST_LOG_TRIVIAL(info) << "Map Name: " << name;
    tmpNode = tmpNode->NextSiblingElement("description");
    this->description = tmpNode->FirstChild()->Value();
	BOOST_LOG_TRIVIAL(info) << "Description: " << description;
    
    tmpNode = tmpNode->NextSiblingElement("size");
	size.push_back(tmpNode->IntAttribute("x"));
	size.push_back(tmpNode->IntAttribute("y"));
	BOOST_LOG_TRIVIAL(info) << "Size: " << size[0] << "*" << size[1];
    tmpNode = tmpNode->NextSiblingElement("gravity");
	this->gravity = tmpNode->FloatAttribute("g");
	BOOST_LOG_TRIVIAL(info) << "Gravity strength: " << gravity << "m/s�";

	tmpNode = tmpNode->NextSiblingElement("spawn");
	tinyxml2::XMLElement* spawnNode = tmpNode;
	spawnNode = spawnNode->FirstChildElement();
	BOOST_LOG_TRIVIAL(error) << "Parsing spawn position";

	while(spawnNode)
	{

		sf::Vector2i spawn (spawnNode->FloatAttribute("x"),spawnNode->FloatAttribute("y"));
		spawnPositions.push_back(spawn);
		spawnNode = spawnNode->NextSiblingElement();
	}
	return true;
}

sf::Vector2i Map::getSpawnPosition(int gameSlot)
{
	return this->spawnPositions[gameSlot];
}