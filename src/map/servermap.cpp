#include "map/servermap.h"
#include "misc/debugrender.h"
#include "entity/contactListener.h"
#include <boost/log/trivial.hpp>
ServerMap::ServerMap()
{

	this->hasDebugDrawObjectSet = false;

}

ServerMap::~ServerMap()
{
    //dtor
}

bool ServerMap::loadTerrainMonolithic()
{
	BOOST_LOG_TRIVIAL(info) << "Loading Terrain ";
	ServerMap::terrain = new ServerTerrain();
	terrain->init();
	// Define the gravity vector.
	b2Vec2 gravity(0.0f, this->gravity);

	// Construct a world object, which will hold and simulate the rigid bodies.
	ServerMap::world = new b2World(gravity);
	ContactListener* c = new ContactListener;
	this->world->SetContactListener(c);

			b2BodyDef groundBodyDef2;
		groundBodyDef2.position.Set(0,0);
		b2Body* groundBody2 = world->CreateBody(&groundBodyDef2);
		std::vector<b2ChainShape*> chains2 = terrain->getWholeTerrainAsSingleChainShape();
		for(int i=0; i < chains2.size();i++)
			{
				groundBody2->CreateFixture(chains2[i], 0.0f);
			}
	

	return true;
}
bool ServerMap::loadTerrainTiled()
{
	BOOST_LOG_TRIVIAL(info) << "Loading Terrain ";
	ServerMap::terrain = new ServerTerrain();
	terrain->init();
	// Define the gravity vector.
	b2Vec2 gravity(0.0f, 3.89f);

	// Construct a world object, which will hold and simulate the rigid bodies.
	ServerMap::world = new b2World(gravity);
	ContactListener* c = new ContactListener;
	this->world->SetContactListener(c);
	// Call the body factory which allocates memory for the ground body
	// from a pool and creates the ground box shape (also from a pool).
	// The body is also added to the world.
	// Define the ground body.
	


	for(int c=0; c < 16;c++)
	{
		b2BodyDef groundBodyDef2;
		groundBodyDef2.position.Set(terrain->cache[c]->pos.x/10.0f,terrain->cache[c]->pos.y/10.0f);
		b2Body* groundBody2 = world->CreateBody(&groundBodyDef2);
		std::vector<b2ChainShape*> chains2 = terrain->cache[c]->getChainShape();
		for(int i=0; i < chains2.size();i++)
			{
				groundBody2->CreateFixture(chains2[i], 0.0f);
			}

	}

	spawnPositions.push_back(b2Vec2(50,20));
	spawnPositions.push_back(b2Vec2(10,10));
	return true;

}
void ServerMap::update()
{

	world->Step(1.0f/60.0f,2,2);
}

void ServerMap::addShip(PhysicalEntity* e)
{
	addDynamicBody(e);
}
void ServerMap::addDynamicBody(PhysicalEntity* e)
{
	BOOST_LOG_TRIVIAL(info) << "Map adding dynamic body ";
	b2Body* body = world->CreateBody(&e->getBodyDefinition());
	body->CreateFixture(&e->getFixtureDefinition());
	body->SetUserData(e);
	e->setBody(body);
	BOOST_LOG_TRIVIAL(info) << "..done ";
}
void ServerMap::removeDynamicBody(PhysicalEntity* e)
{
		BOOST_LOG_TRIVIAL(info) << "Map removes dynamic body ";
		world->DestroyBody(e->getBody());
		BOOST_LOG_TRIVIAL(info) << "..done";
}

void ServerMap::doDebugDraw(sf::RenderWindow* window)
{
	if(!this->hasDebugDrawObjectSet)
	{	DebugDraw* d = new DebugDraw(*window);
		d->SetFlags( DebugDraw::e_shapeBit|DebugDraw::e_centerOfMassBit|DebugDraw::e_aabbBit|DebugDraw::e_pairBit|DebugDraw::e_jointBit);
	//	d->SetFlags( DebugDraw::e_shapeBit|DebugDraw::e_centerOfMassBit|DebugDraw::e_pairBit|DebugDraw::e_jointBit);
		this->world->SetDebugDraw(d);
		this->hasDebugDrawObjectSet = true;
	}
	else this->world->DrawDebugData();

}