#include "map\mapcontainer.h"
#include "settings.h"

MapContainer* MapContainer::instance = 0;

MapContainer::MapContainer()
{
	availableMaps.push_back("defaultmap.xml");
}
Map* MapContainer::getNextMap()
{
	Map* map = new Map();
	map->preparse(Settings::getDataFolderBasePath()+"map/defaultmap/defaultmap.xml");
	return map;
}
Map* MapContainer::getPreviousMap()
{
	Map* map = new Map();
	map->preparse(Settings::getDataFolderBasePath()+"map/defaultmap/defaultmap.xml");
	return map;
}
std::vector<std::string> MapContainer::getAvailableMaps()
{
	return availableMaps;

}

MapContainer* MapContainer::getInstance()
{
	if(instance==0)
		instance = new MapContainer();
	return instance;
}