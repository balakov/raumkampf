#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include "map\tile.h"
#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "Box2D/Box2D.h"
class ServerTile: public Tile
{
public:
    ServerTile(std::string tilefile, sf::Vector2f position);
    ~ServerTile();
    void load();

	std::vector<b2ChainShape*> getChainShape();
 //   sf::Vector2f pos;
protected:
private:
/*
    std::string maskFile;
    bool isReady;
    sf::Image i_mask;
    sf::Texture t_mask;
    sf::Sprite  mask;
*/
	void computePolyShape(void);
	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Point>  apxcontour;
	std::vector<b2ChainShape*> chains;
};

