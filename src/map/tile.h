#ifndef TILE_H
#define TILE_H
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "Box2D/Box2D.h"
class Tile
{
public:
    Tile();
    Tile(std::string tilefile, sf::Vector2f position);
    ~Tile();
    //void load();
    //void draw(sf::RenderTarget* target);
	//std::vector<b2ChainShape*> getChainShape();
    sf::Vector2f pos;
protected:

    std::string maskFile;
    bool isReady;

private:

	/*

	void computePolyShape(void);
	cv::vector<cv::vector<cv::Point> > contours;
	cv::vector<cv::Point>  apxcontour;
	std::vector<b2ChainShape*> chains;*/
};

#endif // TILE_H
