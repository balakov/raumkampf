#include "map/clientterrain.h"
#include <boost/log/trivial.hpp>
#include "settings.h"
ClientTerrain::ClientTerrain()
			{
				;
		}
void ClientTerrain::draw(sf::RenderWindow* win)
{
	win->draw(this->monoLithicSprite);
	/*
    for(int i=0; i < 16; i++)
    {
        cache[i]->draw(win);

    }
	*/
}

bool ClientTerrain::init()
{
			//TODO load fron def file
    std::string temp(Settings::getDataFolderBasePath().append("../data/map/defaultmap/terrain/_"));
    std::ostringstream  ss;
	Terrain::tileSize=256;
	Terrain::mapWidthInTiles=8;
	Terrain::mapHeightInTiles=8;

    int mapwidth = 8;
    for(int y=0; y < 4; y++)
    {
        for(int x=0; x < 4; x++)
        {
            ss << temp ;
            ss << (y)*mapwidth+x+1 << ".png";
            //std::cout <<"\nfile:" <<ss.str();
            cache[(y)*4+x] = new ClientTile(ss.str(),sf::Vector2f(x*256,y*256));
            cache[(y)*4+x]->load();
            ss.str(std::string());


        }
    }

	return true;
}

bool ClientTerrain::loadMonolithic()
{
	BOOST_LOG_TRIVIAL(info) << "Loading Terrain Monolithic";
	this->monoLithicImage.loadFromFile(Settings::getDataFolderBasePath().append("map/defaultmap/terrain/full_color.png"));
	BOOST_LOG_TRIVIAL(info) << "Loading Texture Monolithic, huuuuuge Texture";
	this->monoLithicTexture.loadFromImage(this->monoLithicImage);
	this->monoLithicSprite.setTexture(this->monoLithicTexture);

	return true;
}