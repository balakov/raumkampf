#include "map/servertile.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>


ServerTile::ServerTile(std::string tilefile, sf::Vector2f position)
{
	Tile::pos = position;
    maskFile = tilefile;
    isReady =  false;


}

ServerTile::~ServerTile()
{


}
void ServerTile::load()
{
   // i_mask.loadFromFile(maskFile);
   // t_mask.loadFromImage(i_mask);
   // mask.setTexture(t_mask);
	computePolyShape();
    isReady=true;
}

std::vector<b2ChainShape*> ServerTile::getChainShape()
{

	return chains;
}
void ServerTile::computePolyShape(void)
{
	std::cout << "\nTESSDTHSTRHSRTHSTHRTH\n" << pos.x << pos.y; 
	cv::Mat canny_output;
	cv::Mat src_gray;
	cv::Mat mat;

	mat = cv::imread(maskFile);
	cv::cvtColor(mat, src_gray, cv::COLOR_BGR2GRAY);

	findContours(src_gray, contours, cv::RETR_LIST,cv::CHAIN_APPROX_TC89_KCOS, cv::Point(0, 0));
	for(int a=0; a<contours.size(); a++)
	{

		cv::approxPolyDP(contours[a], apxcontour, 1.0, true);

		b2ChainShape* temp = new b2ChainShape;
		std::vector<b2Vec2> points;
			for(int j=0; j < apxcontour.size(); j++)
			{
				points.push_back(b2Vec2(apxcontour[j].x/10.0f,apxcontour[j].y/10.0f));
			}
			temp->CreateLoop(&points[0],points.size());
			ServerTile::chains.push_back(temp);


	}
}