#include "map/serverterrain.h"
#include <sstream>
#include <iostream>
#include <opencv2\core.hpp>
#include <cv.h>
#include <highgui.h>
#include "settings.h"
ServerTerrain::ServerTerrain()
{
	//TODO load fron def file
 /*   std::string temp("../data/map/defaultmap/terrain/_");
    std::ostringstream  ss;
	ServerTerrain::tileSize=256;
	ServerTerrain::mapWidthInTiles=8;
	ServerTerrain::mapHeightInTiles=8;

    int mapwidth = 8;
    for(int y=0; y < 4; y++)
    {
        for(int x=0; x < 4; x++)
        {
            ss << temp ;
            ss << (y)*mapwidth+x+1 << ".png";
            std::cout <<"\nfile:" <<ss.str();
            cache[(y)*4+x] = new ServerTile(ss.str(),sf::Vector2f(x*256,y*256));
            cache[(y)*4+x]->load();
            ss.str(std::string());


        }
    }*/
}

ServerTerrain::~ServerTerrain()
{
    //dtor
}

bool ServerTerrain::init()
{
	/*	//TODO load fron def file
    std::string temp("../data/map/defaultmap/terrain/_");
    std::ostringstream  ss;
	ServerTerrain::tileSize=256;
	ServerTerrain::mapWidthInTiles=8;
	ServerTerrain::mapHeightInTiles=8;

    int mapwidth = 8;
    for(int y=0; y < 4; y++)
    {
        for(int x=0; x < 4; x++)
        {
            ss << temp ;
            ss << (y)*mapwidth+x+1 << ".png";
            std::cout <<"\nfile:" <<ss.str();
            cache[(y)*4+x] = new ServerTile(ss.str(),sf::Vector2f(x*256,y*256));
            cache[(y)*4+x]->load();
            ss.str(std::string());


        }
    }*/

	return true;
}

std::vector<b2ChainShape*> ServerTerrain::getShapePerimeterOf(b2Vec2 location)
{
	std::string temp = Settings::getDataFolderBasePath().append("map/defaultmap/terrain/_");
    std::ostringstream  ss;
	ss << temp ;
	ss << getTileAt(sf::Vector2f(location.x,location.y));
	ss << ".png";
	std::cout << "\n" <<  ss.str() << std::endl;
	ServerTile t(ss.str(),sf::Vector2f(location.x - ((int)location.x%tileSize), location.y-((int)location.y%tileSize)));
	t.load();
	return t.getChainShape();



}
std::vector<b2ChainShape*> ServerTerrain::getWholeTerrainAsSingleChainShape()
{

	cv::Mat canny_output;
	cv::Mat src_gray;
	cv::Mat mat;


	mat = cv::imread(Settings::getDataFolderBasePath().append("map/defaultmap/terrain/full.png"));
	cv::cvtColor(mat, src_gray, CV_BGR2GRAY);

	findContours(src_gray, contours, CV_RETR_LIST,CV_CHAIN_APPROX_TC89_KCOS, cv::Point(0, 0));
	for(int a=0; a<contours.size(); a++)
	{

		cv::approxPolyDP(contours[a], apxcontour, 1.0, true);

		b2ChainShape* temp = new b2ChainShape;
		std::vector<b2Vec2> points;
			for(int j=0; j < apxcontour.size(); j++)
			{
				points.push_back(b2Vec2(apxcontour[j].x/10.0f,apxcontour[j].y/10.0f));
			}
			temp->CreateLoop(&points[0],points.size());
			chains.push_back(temp);


	}	

	return chains;

}
int ServerTerrain::getTileAt(sf::Vector2f location)
{
	int row = (int)location.y/tileSize;
	int col = (int)location.x/tileSize;
	int tileNum = row*mapWidthInTiles + col;
	return tileNum+1;
}



