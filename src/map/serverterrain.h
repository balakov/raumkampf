#ifndef SERVERTERRAIN_H
#define SERVERTERRAIN_H

#include "Box2D/Box2D.h"
#include "map/servertile.h"

class ServerTerrain
{
public:
    ServerTerrain();
    virtual ~ServerTerrain();
	bool init();
    void rebuildCache();
	std::vector<b2ChainShape*> getShapePerimeterOf(b2Vec2 location);

	std::vector<b2ChainShape*> getWholeTerrainAsSingleChainShape();
    ServerTile* cache[4*4];
protected:
private:
	int getTileAt(sf::Vector2f location);
	int mapWidthInTiles;
	int mapHeightInTiles;
	int tileSize;

	// for monolithic loading
	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Point>  apxcontour;
	std::vector<b2ChainShape*> chains;
};

#endif // SERVERTERRAIN_H