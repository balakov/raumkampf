#pragma once
#include "map/terrain.h"
#include "map/servertile.h"
class ClientTerrain: public Terrain
{
public:
	ClientTerrain();
	void draw(sf::RenderWindow* win);
	bool init();
	bool loadMonolithic();
    ClientTile* cache[4*4];
protected:
	int getTileAt(sf::Vector2f location);
	int mapWidthInTiles;
	int mapHeightInTiles;
	int tileSize;

private:

	sf::Image monoLithicImage;
    sf::Texture monoLithicTexture;
    sf::Sprite  monoLithicSprite;

};