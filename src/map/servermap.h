#pragma once

#include <SFML/Graphics.hpp>
#include "misc/debugrender.h"
#include "serverterrain.h"
#include "Box2D/Box2D.h"
#include "entity/physicalEntity.h"
#include "map/map.h"
class ServerMap : public Map
{
public:
    ServerMap();
    virtual ~ServerMap();

	bool loadTerrainTiled();
	bool loadTerrainMonolithic();
    void update();

	void addShip(PhysicalEntity* e);
	void addDynamicBody(PhysicalEntity* e);
	void removeDynamicBody(PhysicalEntity* e);
	void doDebugDraw(sf::RenderWindow* window);

protected:
private:
	bool hasDebugDrawObjectSet;
	b2World* world;
	ServerTerrain* terrain;
	std::vector<b2Vec2> spawnPositions;
};
