#ifndef STAR_H
#define STAR_H

#include <SFML/Graphics.hpp>

class Star
{
    public:
        Star() {}
        Star(sf::Uint16, sf::Uint16);
        ~Star() {}
        sf::Uint16 getXPos();
        sf::Uint16 getYPos() const;
        void setXPos(sf::Uint16);
        void setYPos(sf::Uint16);
        void addYPos(sf::Uint16);

    private:
        sf::Uint16 xPos;
        sf::Uint16 yPos;
};

Star::Star(sf::Uint16 p_X_Pos, sf::Uint16 p_Y_Pos)
{
    xPos = p_X_Pos;
    yPos = p_Y_Pos;
}

sf::Uint16 Star::getXPos()
{
    return xPos;
}

sf::Uint16 Star::getYPos() const
{
    return yPos;
}

void Star::setXPos(sf::Uint16 x)
{
    xPos = x;
    return;
}

void Star::setYPos(sf::Uint16 y)
{
    yPos = y;
    return;
}

void Star::addYPos(sf::Uint16 y)
{
    xPos -= y;
    return;
}
#endif // STAR_H
