#pragma once
#include "RakNetTypes.h"
#include "SFML\Graphics.hpp"
namespace RK {

	struct Event {
		enum TYPE { EXPLOSION, NONE };
		TYPE type;
		sf::Vector2f position;
		RakNet::RakNetGUID originator;
	};
}