#include <SFML/Graphics.hpp>
#include "map/servermap.h"
#include <iostream>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "misc/debugrender.h"


#include "StringTable.h"
#include "RakPeerInterface.h"

#include <stdio.h>
#include "Kbhit.h"
#include <string.h>
#include "BitStream.h"
#include "MessageIdentifiers.h"
#include "ReplicaManager3.h"
#include "NetworkIDManager.h"
#include "RakSleep.h"
#include "FormatString.h"
#include "RakString.h"
#include "GetTime.h"
#include "SocketLayer.h"
#include "Getche.h"
#include "Rand.h"
#include "VariableDeltaSerializer.h"
#include "Gets.h"
#include "settings.h"
#include "network/clientReplicaManager.h"

#include "util.h"
#include "network/messages.h"
#include "network/multiplayerserver.h"
#include "network/editorsim.h"
using namespace RakNet;

int main(int argc, char** argv)
{
	bool showDebugWindow;
	std::string map;
	std::string mode;
	std::string port;
	RakNetGUID authority;
	std::cout << "Raumkampf Server" << std::endl;
	if (argc <= 4)
	{
		std::cout << "Error, Too few arguments in call\n\n";
		std::cout << "usage:";
		std::cout << "\n\targ0 = server mode (Editorsimulation|Multiplayer|MultiplayerCampaign|Singleplayer)";
		std::cout << "\n\targ1 = server port (Integer)";
		std::cout << "\n\targ2 = GUID of server authority (RakNetGUID);";
		std::cout << "\n\targ3 = map (filename)";
		std::cout << "\n\targ4 = show Debug Window (true|false)";
		std::cout << "\nPress return to exit.";
		getchar(); 
		return 0;
	}
	else {
		for (int i = 0; i < argc; i++)
		{
			std::cout << "Arg" << i <<": " << argv[i] << std::endl;
			
		}
		mode = argv[1];
		port = argv[2];
		authority.FromString(argv[3]);
		map = argv[4];
		if (std::string(argv[5]).compare("true") == 0)
			showDebugWindow = true;
		else showDebugWindow = false;
	}

	if (mode.compare("Editorsimulation") == 0)
	{
		BOOST_LOG_TRIVIAL(info) << "Starting Editorsimulation";
		EditorSim server(authority);
		server.start();
		while (server.step()) { ;; }
		BOOST_LOG_TRIVIAL(info) << "Stopping Server";
		server.stop();
		BOOST_LOG_TRIVIAL(info) << "Exiting";
		return 1;
	}

	BOOST_LOG_TRIVIAL(info) << "Starting Multiplayerserver";
	Settings::load(Settings::getDataFolderBasePath()+"config.xml");
	// Server should be silent, else devices will play their sound.
	Settings::setMusicVolume(0);
	Settings::setSoundVolume(0);
	MultiPlayerServer server(authority);
	server.start();	// comment for debug screen
	std::cout << authority.ToString();
	std::stringstream ss (std::stringstream::in | std::stringstream::out);
    sf::Text text;
    sf::Font font;
    font.loadFromFile("../data/font/FixedSysEx.ttf");
    text.setString("bla");
    text.setFont(font);
	text.setColor(sf::Color::White);
    text.setCharacterSize(16);
	sf::Clock Clock;
	float time = Clock.getElapsedTime().asSeconds();

	sf::RenderWindow* window = new sf::RenderWindow(sf::VideoMode(1024, 768), "Server Debug Screen");
	window->setVerticalSyncEnabled(true);
	sf::View view;
	window->setView(view);
	view.setSize(sf::Vector2f(1024, 768));
	view.setCenter(512, 384);

	//DebugDraw* debugDraw = new DebugDraw(*window);
	//server.map.world->SetDebugDraw(debugDraw);



	while (window->isOpen() && server.state!=server.SHUTDOWN)
	{	


		sf::Event event;
		while (window->pollEvent(event))
		{
			// "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
				window->close();

			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Right))
			{
				view.move(sf::Vector2f(10, 0));
			}
			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Left))
			{
				view.move(sf::Vector2f(-10, 0));

			}
			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Up))
			{
				view.move(sf::Vector2f(0,-10));
			}
			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Down))
			{
				view.move(sf::Vector2f(0,10));
			}

		}

		RakSleep(30);
		server.loop();

		window->setView(view);
		window->clear();
		// draw everything here...
		server.map.doDebugDraw(window);
		time = Clock.getElapsedTime().asSeconds();
        Clock.restart();
		float fps = 1.0f/time;
        ss << "Frames/Sec\n" << ceil(fps);
        text.setString(ss.str());
        window->draw(text);
        ss.str(" ");
		window->display();		
	}
	delete window;
	return 0;
}