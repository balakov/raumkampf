#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "entity.h"


class Projectile : public Entity
{
public:
    Projectile(sf::Vector2f start, sf::Vector2f muzzle);
    virtual ~Projectile();
    void draw(sf::RenderTarget * target);
    void update(float elapsedTimeInMicroSeconds);
protected:
private:
    sf::CircleShape circle;
    int lastUpdate;
};

#endif // PROJECTILE_H
