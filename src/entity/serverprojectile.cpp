#include "entity/serverprojectile.h"
#include <iostream>

ServerProjectile::ServerProjectile(sf::Vector2f start, sf::Vector2f muzzle)
{
    this->position = start;
    this->impulse = muzzle;
    circle = sf::CircleShape(2, 4);
    circle.setOrigin(1,1);
    circle.setFillColor(sf::Color::Yellow);
    lastUpdate=0;

	bodyDef.type = b2_dynamicBody;
    bodyDef.angularDamping = 1.1f;
    bodyDef.fixedRotation = false;
    bodyDef.position.Set(start.x, start.y);
	bodyDef.linearVelocity = b2Vec2(muzzle.x,muzzle.y);
	b2CircleShape* circle = new b2CircleShape();
	circle->m_p.Set(0,0);
	circle->m_radius = 0.01f;
    fixDef.shape = circle;
    fixDef.density = 1.0f;
	fixDef.friction = 0.1f;
	fixDef.restitution = 0.9f;
	//fixDef.filter.groupIndex=-1;
}

ServerProjectile::~ServerProjectile()
{
    //dtor
}

void ServerProjectile::draw(sf::RenderTarget* target)
{
    circle.setPosition(position);
    target->draw(circle);
}

void ServerProjectile::update(float deltaT)
{
	position.x = body->GetPosition().x*10;
	position.x = body->GetPosition().y*10;
}
