#pragma once
#include "Box2D/Box2D.h"
#include "network/networkEntity.h"
class PhysicalEntity : public NetworkEntity
{
public:
	bool markForDeletion;
	bool bodySet;
	b2Body* body;
	b2BodyDef bodyDef;
    b2FixtureDef fixDef;
    b2Fixture* fixture;
    b2PolygonShape collisionShape;
	PhysicalEntity(){markForDeletion=false;}

	virtual bool hasBody(){return true;}
	virtual b2Body* getBody(){return body;}
	virtual b2BodyDef getBodyDefinition(){return bodyDef;}
	virtual b2FixtureDef getFixtureDefinition(){return fixDef;}
	virtual void setBody(b2Body* body){this->body=body;}


};