#pragma once
#include "network/networkEntity.h"
#include <SFML/Graphics.hpp>
#include <boost/log/trivial.hpp>
#include "entity/physicalEntity.h"

class ServerProjectile : public PhysicalEntity
{
public:
    ServerProjectile(sf::Vector2f start, sf::Vector2f muzzle);
    virtual ~ServerProjectile();
    void draw(sf::RenderTarget * target);
    void update(float elapsedTimeInMicroSeconds);

		/*/ Replica Implementations /*/
	virtual RakNet::RakString getIdentifier(void) const {return "Projectile";}

	void WriteAllocationID(RakNet::Connection_RM3 *destinationConnection, RakNet::BitStream *allocationIdBitstream) const {
		allocationIdBitstream->Write(getIdentifier());
	}
	virtual RM3ConstructionState QueryConstruction(RakNet::Connection_RM3 *destinationConnection, ReplicaManager3 *replicaManager3) {

		return QueryConstruction_ServerConstruction(destinationConnection,true);
	}
	virtual bool QueryRemoteConstruction(RakNet::Connection_RM3 *sourceConnection) {
		return QueryRemoteConstruction_ServerConstruction(sourceConnection,true);
	}
	virtual RM3QuerySerializationResult QuerySerialization(RakNet::Connection_RM3 *destinationConnection) {
		return QuerySerialization_ServerSerializable(destinationConnection,true);
	}
	virtual RM3ActionOnPopConnection QueryActionOnPopConnection(RakNet::Connection_RM3 *droppedConnection) const {
		return QueryActionOnPopConnection_Server(droppedConnection);
	}
	virtual void SerializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *destinationConnection)
	{
		BOOST_LOG_TRIVIAL(info) << "Serializing Construction";
		RakString name = "test";
		constructionBitstream->Write(name);
	}
	virtual bool DeserializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *sourceConnection)
	{
		BOOST_LOG_TRIVIAL(warning) << "Why is server deserializing projectile Construction";
		return true;
	}
	
	RM3SerializationResult Serialize(SerializeParameters *serializeParameters)	{
		sf::Vector2f position;
		position.x = this->body->GetPosition().x*10;
		position.y = this->body->GetPosition().y*10;
		serializeParameters->outputBitstream->Write(position);
		return RM3SR_SERIALIZED_ALWAYS;
	}
	void Deserialize(RakNet::DeserializeParameters *deserializeParameters) {
		BOOST_LOG_TRIVIAL(warning) << "why is server deserializing clients projectile?";
	}

	virtual void SerializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *destinationConnection){}
	virtual bool DeserializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *sourceConnection){return true;}

	void DeallocReplica(RakNet::Connection_RM3 *sourceConnection) {
		delete this;
	}


protected:
private:
    sf::CircleShape circle;
    int lastUpdate;
    sf::Vector2f position;
    float rotation;
    sf::Vector2f impulse;
};

