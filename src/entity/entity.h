#pragma once
#include <SFML/Graphics.hpp>

class Entity
{

public:
    Entity()
    {

    }

    Entity(sf::Vector2f position, sf::Vector2f impulse, float rotation)
    {
        this->position = position;
        this->impulse = impulse;
        this->rotation = rotation;
    };

    virtual void setPosition(sf::Vector2f newPosition)
    {
        position=newPosition;
    }
    sf::Vector2f getPosition()
    {
        return position;
    };

    virtual void setRotation(float newRotation)
    {
        rotation=newRotation;
    }
    float getRotation()
    {
        return rotation;
    };

    virtual void setImpulse(sf::Vector2f newImpulse)
    {
        impulse=newImpulse;
    }
    sf::Vector2f getImpulse()
    {
        return impulse;
    }

    virtual void draw(sf::RenderTarget * target)
    {
        ;
    };
    virtual void update(float elapsedTimeInMicroSeconds)
    {
        ;
    };



protected:
    sf::Vector2f position;
    float rotation;
    sf::Vector2f impulse;
};
