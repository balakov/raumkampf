#pragma once

#include "network/networkEntity.h"
#include <SFML/Graphics.hpp>
#include <boost/log/trivial.hpp>

class ClientProjectile : public NetworkEntity
{
public:
	    sf::Vector2f position;
	RakNet::RakString getIdentifier(void) const {return "Projectile";}
    ClientProjectile();
    virtual ~ClientProjectile();
    void draw(sf::RenderTarget * target);
    void update(float elapsedTimeInMicroSeconds);
	/*/ Replica Implementations /*/
	void WriteAllocationID(RakNet::Connection_RM3 *destinationConnection, RakNet::BitStream *allocationIdBitstream) const {
		allocationIdBitstream->Write(getIdentifier());
	}
	virtual RM3ConstructionState QueryConstruction(RakNet::Connection_RM3 *destinationConnection, ReplicaManager3 *replicaManager3) {

		return QueryConstruction_ServerConstruction(destinationConnection,false);
	}
	virtual bool QueryRemoteConstruction(RakNet::Connection_RM3 *sourceConnection) {
		return QueryRemoteConstruction_ServerConstruction(sourceConnection,false);
	}
	virtual RM3QuerySerializationResult QuerySerialization(RakNet::Connection_RM3 *destinationConnection) {
		return QuerySerialization_ServerSerializable(destinationConnection,false);
	}
	virtual RM3ActionOnPopConnection QueryActionOnPopConnection(RakNet::Connection_RM3 *droppedConnection) const {
		return QueryActionOnPopConnection_Server(droppedConnection);
	}
	virtual void SerializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *destinationConnection)
	{
		BOOST_LOG_TRIVIAL(warning) << "Client Serializing Construction of Projectile, why?";
	}
	virtual bool DeserializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *sourceConnection)
	{
		BOOST_LOG_TRIVIAL(info) << "Deserializing Construction of Projectile";

		BOOST_LOG_TRIVIAL(info) << "Deserialization complete";
		return true;
	}
	
	RM3SerializationResult Serialize(SerializeParameters *serializeParameters)	{
		BOOST_LOG_TRIVIAL(warning) << "Client trying to serializing a Projectile, why?";
		return RM3SR_DO_NOT_SERIALIZE;
	}
	void Deserialize(RakNet::DeserializeParameters *deserializeParameters) {
			sf::Vector2f pos;	
			deserializeParameters->serializationBitstream->Read(pos);
			this->position = pos;
	}

	virtual void SerializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *destinationConnection){}
	virtual bool DeserializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *sourceConnection){		BOOST_LOG_TRIVIAL(trace) << "Deserializing Projectile destruction";return true;}

	void DeallocReplica(RakNet::Connection_RM3 *sourceConnection) {
		//delete this;
		BOOST_LOG_TRIVIAL(warning) << "Deallocating Projectile";
	}

protected:
private:
    sf::CircleShape circle;
    int lastUpdate;

    float rotation;
    sf::Vector2f impulse;
};
