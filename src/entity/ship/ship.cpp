#include "ship/ship.h"
#include "util.h"
#include "tinyxml/tinyxml2.h"
#include "ship/hullfactory.h"
#include <boost/log/trivial.hpp>
#include "ship/devicefactory.h"
#include "settings.h"

bool Ship::loadDevices(std::string filename)
{
	BOOST_LOG_TRIVIAL(info) << "Loading Devices from file: "<<filename;
	tinyxml2::XMLDocument* shipdefinition = new tinyxml2::XMLDocument();
	shipdefinition->LoadFile(filename.c_str());

    tinyxml2::XMLElement* root;
    root = shipdefinition->FirstChildElement("ship");

    if(!root)
    {
        BOOST_LOG_TRIVIAL(error) << "Not a ship definition file ";
        return false;
    }

    tinyxml2::XMLElement* tmpNode = root->FirstChildElement("name");
    this->name = tmpNode->FirstChild()->Value();

    tmpNode = tmpNode->NextSiblingElement("description");
    this->description = tmpNode->FirstChild()->Value();

    /// loading hull node
    tmpNode = tmpNode->NextSiblingElement("hull");
    tinyxml2::XMLElement* hullNode = tmpNode;
    std::string hullIdentifier = hullNode->FirstChild()->Value();
    BOOST_LOG_TRIVIAL(info) << "Hull name: "<<hullIdentifier;

    /// loading device node
    tmpNode = tmpNode->NextSiblingElement("devices");
    tinyxml2::XMLElement* deviceNode = tmpNode->FirstChildElement("device");

    DeviceFactory* deviceFactory = new DeviceFactory;

    /// creating devices
    while(deviceNode)
    {
		std::string deviceName = deviceNode->FirstChildElement()->GetText();
		int slot = deviceNode->IntAttribute("slot");
		BOOST_LOG_TRIVIAL(info) << "Loading "<<deviceName<<" into slot " << slot;
		Device* device = DeviceFactory::getByName(deviceName);
		
		BOOST_LOG_TRIVIAL(info) << this->getHull()->getModelName();
		this->getHull()->setDevice(device, slot);
        device->setLocalRotation(deviceNode->FirstChildElement()->NextSiblingElement()->IntAttribute("deg"));
		device->config(deviceNode->FirstChildElement()->NextSiblingElement()->NextSiblingElement("config"));

        deviceNode = deviceNode->NextSiblingElement();
    }

   	BOOST_LOG_TRIVIAL(trace) << "Device generation finished";
	return true;
}
bool Ship::preparse(std::string filename)
{
	BOOST_LOG_TRIVIAL(info) << "Loading Ship from file: "<<filename;
	this->definitionFilePath = filename;
	tinyxml2::XMLDocument* shipdefinition = new tinyxml2::XMLDocument();
	shipdefinition->LoadFile(filename.c_str());

    tinyxml2::XMLElement* root;
    root = shipdefinition->FirstChildElement("ship");

    if(!root)
    {
        BOOST_LOG_TRIVIAL(error) << "Not a ship definition file ";
        return false;
    }

    tinyxml2::XMLElement* tmpNode = root->FirstChildElement("name");
    this->name = tmpNode->FirstChild()->Value();

    tmpNode = tmpNode->NextSiblingElement("description");
    this->description = tmpNode->FirstChild()->Value();

    /// loading hull node
    tmpNode = tmpNode->NextSiblingElement("hull");
    tinyxml2::XMLElement* hullNode = tmpNode;
	this->hullModelFile = hullNode->FirstChild()->Value();
    BOOST_LOG_TRIVIAL(info) << "Hull name: "<<this->hullModelFile;
   
	// device node
	tmpNode = tmpNode->NextSiblingElement("devices");
    //tinyxml2::XMLElement* hullNode = tmpNode;

	// keymap node
	/*tmpNode = tmpNode->NextSiblingElement("keymap");
    tinyxml2::XMLElement* mapNode = tmpNode;
	std::string str = Settings::getDataFolderBasePath().append("save/testprofile1/");
	str.append(mapNode->GetText());*/
	//this->keyMapFile = str;
	//BOOST_LOG_TRIVIAL(info) << "Keymap: "<<this->keyMapFile;
	return true;
}
bool Ship::writeToFile(std::string filename)
{
	  tinyxml2::XMLDocument doc;
	  std::string shipFile = filename;

  BOOST_LOG_TRIVIAL(info) << "writing ship config to " <<shipFile;
  if (doc.LoadFile(shipFile.c_str()) == tinyxml2::XML_SUCCESS)
   {
	   doc.Clear();
	   tinyxml2::XMLText* txt; 
      tinyxml2::XMLNode *root = doc.NewElement("ship");
	  doc.InsertFirstChild(root);
		tinyxml2::XMLElement *name = doc.NewElement("name");
		root->InsertFirstChild(name);
			txt = doc.NewText(this->getName().c_str());
			name->InsertFirstChild(txt);
		tinyxml2::XMLElement *desc = doc.NewElement("description");
		root->InsertEndChild(desc);
			txt = doc.NewText(this->getDescription().c_str());
			desc->InsertFirstChild(txt);
		tinyxml2::XMLElement *hull = doc.NewElement("hull");
		root->InsertEndChild(hull);
			txt  = doc.NewText(this->getHull()->getModelName().c_str());
			hull->InsertEndChild(txt);
		tinyxml2::XMLElement *deviceList = doc.NewElement("devices");
		root->InsertEndChild(deviceList);
	
		for(int i=0; i < this->getHull()->getNumberOfSlots(); i++)
		{
			if(this->getHull()->getDevice(i)!=0)
			{
				tinyxml2::XMLElement *device = doc.NewElement("device");
				device->SetAttribute("slot",i);
					tinyxml2::XMLElement *base = doc.NewElement("base");
					 BOOST_LOG_TRIVIAL(info) << this->getHull()->getDevice(i)->getName().c_str();
					txt = doc.NewText(this->getHull()->getDevice(i)->getName().c_str());
					base->InsertEndChild(txt);
				
					tinyxml2::XMLElement *rot = doc.NewElement("rotation");
					rot->SetAttribute("deg",this->getHull()->getDevice(i)->getLocalRotation());
					
					tinyxml2::XMLElement *conf = doc.NewElement("config");
					this->getHull()->getDevice(i)->writeConfig(conf);

					tinyxml2::XMLElement *input = doc.NewElement("input");
					txt = doc.NewText("normal");
					input->InsertEndChild(txt);
					device->InsertEndChild(base);
					device->InsertEndChild(rot);
					device->InsertEndChild(conf);
				device->InsertEndChild(input);
				deviceList->InsertEndChild(device);
			}
			else  BOOST_LOG_TRIVIAL(info) << "Empty Slot " << i;
		}
		/*tinyxml2::XMLElement *keyMapNode = doc.NewElement("keymap");
		txt = doc.NewText("slot0.binding");
		keyMapNode->InsertEndChild(txt);
		root->InsertEndChild(keyMapNode);
		this->getKeyMap()->write();*/

		doc.SaveFile(shipFile.c_str());
	return true;
  }
  else 
  {  BOOST_LOG_TRIVIAL(info) << "Could not open " << filename;
		  return false;
  }
}
std::string Ship::getName()
{
    return name;
}

std::string Ship::getDescription()
{
    return description;
}
void Ship::printDescription()
{
    std::cout <<"\nName: " << name;
    std::cout <<"\nDescription: "<< description;
}
int Ship::getMass()
{
    int mass = this->getHull()->getMass();
	std::vector<Device*> devices = this->getHull()->getDevices();
    for(int i=0; i < devices.size(); i++)
    {
        mass+=devices[i]->getMass();
    }
    return mass;
}

void Ship::setPosition(sf::Vector2f newPosition)
{
	this->getHull()->setPosition(newPosition);
}
void Ship::setRotation(float rotation)
{
	this->getHull()->setRotation(rotation);
}
sf::Vector2f Ship::getPosition()
{
	return this->getHull()->getPosition();
}
float Ship::getRotation()
{
	return this->getHull()->getRotation();
}
/*
KeyMap* Ship::getKeyMap()
{
	return this->keymap;
}
void Ship::setKeyMap(KeyMap* keymap)
{
	this->keymap = keymap;
}*/

b2Vec2 Ship::getCenterOfMass()
{/*
	std::vector<Device*> devices = hull->getDevices();
    b2Vec2 ctr(Util::toB2(hull->getCenterOfMass()));
    std::cout << "\nhullctr" << ctr.x << ":" << ctr.y;
    ctr.x *= hull->getMass();
    ctr.y *= hull->getMass();

	for(int i=0; i < hull->slotPositions.size(); i++)
    {
        b2Vec2 locPos = Util::toB2(devices[i]->getLocalPosition());
        ctr.x+= devices[i]->getMass() * locPos.x;
        ctr.y+= devices[i]->getMass() * locPos.y;
        std::cout << "\n" << locPos.x << ":" << locPos.y <<" mass: " << devices[i]->getMass();
    }
    std::cout << "\nshipctr ||" << ctr.x << ":" << ctr.y;
    ctr.x/=getMass();
    ctr.y/=getMass();
    std::cout << "\nshipctr ||" << ctr.x << ":" << ctr.y;
    */return b2Vec2(0,0);

}