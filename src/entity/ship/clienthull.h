#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include "ship/Device.h"
#include "Box2D/Box2D.h"
#include "entity/physicalEntity.h"
#include "hull.h"

#define MAXDEVICES 32

class ClientHull : public Hull
{
private:

    sf::Texture* hulltex;
    sf::Sprite* hullsprite;

public:
    ClientHull();
    ~ClientHull();

	void load(std::string hullModel);
	std::string schematic;

    void draw(sf::RenderTarget * target);
	void updateGraphics(float time, sf::Vector2f linVel, float angVel);

	sf::Texture* getHullTexture(){return hulltex;} 
    friend class HullFactory;
protected:

};
