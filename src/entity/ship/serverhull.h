#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include "ship/Device.h"
#include "Box2D/Box2D.h"
#include "entity/physicalEntity.h"
#include "hull.h"

#define MAXDEVICES 32

class ServerHull : public Hull
{
private:
	bool isBodySet;
public:
    ServerHull();
    ~ServerHull();

	void load(std::string hullModelName);

	std::vector<PhysicalEntity*> entities;
	bool hasEntities(void){if (entities.size()>0)return true; else return false;}
	std::vector<PhysicalEntity*> getEntities(){return entities;}
	void clearEntities(){entities.clear();}

	bool hasBody(){return isBodySet;}
	b2Body*getBody(){return body;}
	
	void updatePhysics(float time);
	void handleCollision(std::string impactorType, b2Vec2 impactorVelocity, float impactorMass);
	void powerDevices();
	void fuelDevices();

	b2Body* body;   b2BodyDef bodyDef;
    b2FixtureDef fixDef;
    b2Fixture* fixture;
    b2PolygonShape collisionShape;
    friend class HullFactory;
protected:

};

