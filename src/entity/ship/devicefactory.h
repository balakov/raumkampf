#ifndef DEVICEFACTORY_H
#define DEVICEFACTORY_H
#include "device.h"
#include "tinyxml/tinyxml2.h"

class DeviceFactory
{
public:
    DeviceFactory();
    static Device* get(tinyxml2::XMLElement* deviceNode);
	static Device* getByName(std::string name);
	static std::vector<std::string> getAvailableDevices();
protected:
private:

    static Device* loadThruster(tinyxml2::XMLElement* thrustNode);
    static Device* loadMassDriver(tinyxml2::XMLElement* drvNode);
    static Device* loadReactor(tinyxml2::XMLElement* drvNode);
    static Device* loadTank(tinyxml2::XMLElement* drvNode);
};

#endif // DEVICEFACTORY_H
