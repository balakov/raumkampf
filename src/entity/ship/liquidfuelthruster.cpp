#include "ship/liquidfuelthruster.h"
#include <iostream>
#include "entity/serverprojectile.h"
#include "util.h"
#include "settings.h"
#include <boost/log/trivial.hpp>
#include "settings.h"
#include "globals.h"
LiquidFuelThruster::LiquidFuelThruster()
{
    this->accumulatedForce.SetZero();

    this->hasPlume = false;
    this->exhaustTex.loadFromFile(Settings::getDataFolderBasePath().append("img/devices/exhaust/plume_0.png"));
    this->exhaustPlume.setTexture(exhaustTex);
    this->exhaustPlume.setOrigin(32,32);

    buffer = new sf::SoundBuffer();
	buffer->loadFromFile(Settings::getDataFolderBasePath().append("snd/thrst.wav"));
    sound = new sf::Sound(*buffer);

	sound->setVolume(Settings::getSoundVolume());
    sound->setLoop(true);//ctor

	burnTimeSinceLastPoll=0 ;
	this->type = "thruster";
}

LiquidFuelThruster::~LiquidFuelThruster()
{

}
void LiquidFuelThruster::config(tinyxml2::XMLElement* configNode)
{
	BOOST_LOG_TRIVIAL(info) << "Configuring LFT";
	if(configNode !=nullptr)
	{
		//BOOST_LOG_TRIVIAL(info) << "Configure foo: "<<configNode->IntAttribute("foo");
		//BOOST_LOG_TRIVIAL(info) << "Configure bar: "<<configNode->IntAttribute("bar");
	}
	else BOOST_LOG_TRIVIAL(warning) << "Config Node invalid";
}

void LiquidFuelThruster::writeConfig(tinyxml2::XMLElement* configNode)
{
	tinyxml2::XMLDocument* doc = configNode->GetDocument();
	tinyxml2::XMLElement* foo = doc->NewElement("Foo");
	foo->SetAttribute("n", 1);
	tinyxml2::XMLElement* bar = doc->NewElement("Bar");
	bar->SetAttribute("n", 0);
	configNode->InsertEndChild(foo);
	configNode->InsertEndChild(bar);
	BOOST_LOG_TRIVIAL(info) << "Foo/Bar persisted";
}

tgui::Widget::Ptr LiquidFuelThruster::getConfigWidget(sf::Vector2f size, bool forceRebuild)
{
	if (!wasWidgetCreated || forceRebuild)
	{
		this->configWidget = Device::getConfigWidget(size, true);

	}

	return this->configWidget;

}


void LiquidFuelThruster::signal(bool enabled)
{

 //float time = clock.getElapsedTime().asSeconds();
    //clock.restart();
    if(enabled)
    {
        hasPlume = true;

        if(this->state==true)
        {   
			
            float xComp = cos((rotInDeg)*3.14159265359/180);
            float yComp = sin((rotInDeg)*3.14159265359/180);
            this->accumulatedForce.x += xComp;
            this->accumulatedForce.y += yComp;

            return;
        }
        else
        {	
			clock.restart();
            sound->play();	
            sound->setLoop(true);
            state=true;
            ///std::cout << "\nLFE started";
            return;
        }
    }
    else
    {

        if(this->state==false)
        {

            return;
        }
        else
        {
			this->burnTimeSinceLastPoll+=clock.getElapsedTime().asMicroseconds();
			//std::cout << "\n\n test: " << clock.getElapsedTime().asMicroseconds();
			sound->stop();
            state = false;
			hasPlume = false;
            ///std::cout << "\nLFE stopped";
        }
    }
}

PhysicalEntity* LiquidFuelThruster::pollPhysicalSubEntity()
{
    this->hasSE=false;

    sf::Vector2f tmp(0,0);
    return new ServerProjectile(position, tmp);
}
b2Vec2 LiquidFuelThruster::pollImpulseModification()
{
	//b2Vec2 tmp( accumulatedForce.x,accumulatedForce.y);
      b2Vec2 tmp(cos((rotInDeg)*3.14159265359/180),sin((rotInDeg)*3.14159265359/180));
	  if (!state || !isFuelled)
	  {
		  tmp.SetZero(); // accumulatedForce.SetZero();
		  state = false;
	  }
	  this->isFuelled = false;
    return tmp;
}
int LiquidFuelThruster::pollEnergyModification(void)
{
	return 0;
}
int LiquidFuelThruster::pollFuelModification(void)
{

	if(state==true)
	{
		burnTimeSinceLastPoll += clock.getElapsedTime().asMicroseconds();
		clock.restart();
	}
	int fuelUsed = this->burnTimeSinceLastPoll/10000;
	return fuelUsed;
}

void LiquidFuelThruster::setFuelled()
{
	this->burnTimeSinceLastPoll = 0;
	this->isFuelled = true;
}
void LiquidFuelThruster::draw(sf::RenderTarget * target, sf::Vector2f worldLocation, float worldRotation)
{

    if(hasPlume)
    {
		sf::CircleShape circle(2);
		circle.setPosition(worldLocation);
		circle.setFillColor(sf::Color::Red);

		target->draw(circle);
		exhaustPlume.setPosition(worldLocation);
		exhaustPlume.setRotation(worldRotation+this->rotInDeg);      
        target->draw(exhaustPlume);
		
    }
}


std::string LiquidFuelThruster::getInfoString()
{
	std::stringstream s;
    s << Device::getInfoString();
    s << "\npropellant: " << this->propellant;
    s << "\nMass flow: " << this->massFlow << " kg/second";
    s << "\nExhaust velocity: " << this->exhaustVelocity << " m/second";
    s << "\nresulting net thrust: " << this->exhaustVelocity*this->massFlow << " N";
    s << "\n---------------";
			return s.str();
}