#include "ship/device.h"
#include <iostream>
#include "settings.h"
#include "globals.h"
#include <boost/log/trivial.hpp>
Device::Device()
{
    hasSE = false;
    state = false;

isFuelSrc = false;
isPowerSrc = false;

wasWidgetCreated = false;
configNeedsSendFlag = false;
}

Device::~Device()
{
    //dtor
}

bool Device::hasSubEntity()
{
    return hasSE;
}

tinyxml2::XMLElement* Device::preparse(tinyxml2::XMLElement* deviceNode)
{
	tinyxml2::XMLElement* cursor = deviceNode->FirstChildElement("name");
    std::string name = cursor->GetText();
    this->name = name;

    cursor = cursor->NextSiblingElement("description");
    std::string description = cursor->GetText();
    this->description = description;

    cursor = cursor->NextSiblingElement("mass");
    int mass = cursor->IntAttribute("base");
    this->mass = mass;

    cursor = cursor->NextSiblingElement("price");
    int price = cursor->IntAttribute("base");
    this->price = price;

    cursor = cursor->NextSiblingElement("formfactor");
    std::string formfactor = cursor->GetText();
    this->formfactor = formfactor;

    cursor = cursor->NextSiblingElement("editorimage");
	this->editorTexture.loadFromFile(Settings::getDataFolderBasePath()+"img/devices/editor/" + cursor->GetText());
	this->editorSprite.setRotation(0);
	this->editorSprite.setTexture(this->editorTexture);
	this->editorSprite.setOrigin(this->editorTexture.getSize().x*0.5, this->editorTexture.getSize().y*0.5);

	std::cout << "\n\nTEST " << this->editorTexture.getSize().x*0.5 <<" " <<this->editorTexture.getSize().y*0.5;
	return cursor;
}

int Device::getMass()
{
    return this->mass;
}
void Device::setWorldPosition(sf::Vector2f position)
{
    this->position = position;
}

std::string Device::getInfoString()
{
	std::stringstream str;
	str << this->name << " state: " << this->state;
	//str <<  "\nDevice name: " << this->name << " Description: " << description <<" Mass :" <<mass << " kg" << "\nType: " << type << "\nForm Factor: " << formfactor;
	return str.str();
}
tgui::Widget::Ptr Device::getConfigWidget(sf::Vector2f size, bool forceRebuild)
{
	if (!wasWidgetCreated || forceRebuild)
	{
		tgui::Grid::Ptr configGrid = tgui::Grid::create();
		configGrid->setSize(size);
		// tgui bug
		Globals::getGUI()->add(configGrid);
		Globals::getGUI()->remove(configGrid);
		//
		rotLabel = tgui::Label::create(Settings::getDataFolderBasePath() + "gui/widgets/Editor.conf");
		rotLabel->setSize(size.x*0.75, 60);
		rotLabel->setTextSize(Settings::getGuiTextSize());
		rotLabel->setText("Rotation");

		//rotLabel->getRenderer()->setTextColor(sf::Color::White);

		configGrid->add(rotLabel);
		configGrid->addWidget(rotLabel, 0, 0);
		
		rotSlider = tgui::Slider::create(Settings::getDataFolderBasePath() + "gui/widgets/Black.conf");
		rotSlider->setSize(size.x*0.75, 20);
		rotSlider->setMaximum(360);
		rotSlider->setValue(this->getLocalRotation());
		rotSlider->connect("ValueChanged", &Device::configRotation, this);
		//rotSlider->connect("MouseReleased", &Device::configChanged, this);
		configGrid->add(rotSlider);
		configGrid->addWidget(rotSlider, 1, 0);

		configWidget = configGrid;
		wasWidgetCreated = true;

	}

	return this->configWidget;
}
void Device::configRotation()
{
	setLocalRotation(rotSlider->getValue());
	configNeedsSendFlag = true;
}
void Device::configChanged()
{
	BOOST_LOG_TRIVIAL(trace) << "Config changed";
	this->configNeedsSendFlag = true;
}
void Device::writeParameters(RakNet::BitStream* bitstream)	//writes to a bitstream to persist internals
{
	if(this->state)
		bitstream->Write1();
	else bitstream->Write0();

}
bool Device::readParameters(RakNet::BitStream* bitstream)
{

	this->signal(bitstream->ReadBit());
	return true;
}
void Device::writeConfigToBitstream(RakNet::BitStream* bitstream)	//writes to a bitstream to persist internals
{
	this->setLocalRotation(this->getLocalRotation() + 1);

	bitstream->Write(this->getLocalRotation());
	std::cout << "\nWriting rotInDeg: " << rotInDeg;
}
bool Device::readConfigFromBitstream(RakNet::BitStream* bitstream)
{
	float newRotation;
	bitstream->Read(newRotation);
	this->setLocalRotation(newRotation);
	std::cout << "\nReading rot" << this->getLocalRotation();
	return true;
}
