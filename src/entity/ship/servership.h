#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <iostream>
#include "ship/ship.h"
#include "ship/serverhull.h"
#include "keymap.h"
#include "network/networkEntity.h"
#include "hullfactory.h"
#include "devicefactory.h"
#include <boost/log/trivial.hpp>


#define NUMSLOTS 32
class ServerShip : public PhysicalEntity, Ship {
    friend class ShipFactory;

private:
    ServerHull* hull;
    sf::Clock shipClock;
    float lastUpdate;

public:

    ServerShip();
	bool loadFromFile(std::string filename);
	ServerHull* getHull();
	void setHull(ServerHull* hull){this->hull = hull;}

	bool hasEntities(){return hull->hasEntities();}
	std::vector<PhysicalEntity*> getEntities() {return hull->getEntities();}
	//std::vector<RKEvent> getEvents();
	void clearEntities(){hull->clearEntities();}

	void handleCollision(float linearSpeed, float impactorMass, RakNetGUID impactorGUID);
	bool markForRespawn;
	RakNet::RakNetGUID killingBlow;
	RakNet::RakNetGUID getOwnerOfKillingBlow(){return killingBlow;}

    void updatePhysics(float elapsedTimeInMicroSeconds);

	/* Box2D */

	virtual b2Body* getBody(){return hull->body;}
	virtual b2BodyDef getBodyDefinition(){return hull->bodyDef;}
	virtual b2FixtureDef getFixtureDefinition(){return hull->fixDef;}
	virtual void setBody(b2Body* body){hull->body = body; this->bodySet=true;}
	virtual bool hasBody(){return bodySet;}
	/*/ Replica Implementations /*/
	virtual RakNet::RakString getIdentifier(void) const;

	void WriteAllocationID(RakNet::Connection_RM3 *destinationConnection, RakNet::BitStream *allocationIdBitstream) const;
	virtual RM3ConstructionState QueryConstruction(RakNet::Connection_RM3 *destinationConnection, ReplicaManager3 *replicaManager3);
	virtual bool QueryRemoteConstruction(RakNet::Connection_RM3 *sourceConnection);
	virtual RM3QuerySerializationResult QuerySerialization(RakNet::Connection_RM3 *destinationConnection);
	virtual RM3ActionOnPopConnection QueryActionOnPopConnection(RakNet::Connection_RM3 *droppedConnection) const;
	
	virtual void SerializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *destinationConnection);
	
	virtual bool DeserializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *sourceConnection);
	RM3SerializationResult Serialize(SerializeParameters *serializeParameters);
	// Deserializes device configuration ONLY! 
	void Deserialize(RakNet::DeserializeParameters *deserializeParameters);
	virtual void SerializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *destinationConnection);
	virtual bool DeserializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *sourceConnection);
	void DeallocReplica(RakNet::Connection_RM3 *sourceConnection);
};
