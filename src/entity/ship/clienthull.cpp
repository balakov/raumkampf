#include "ship/clienthull.h"
#include <iostream>
#include "util.h"
#include "ship/hullfactory.h"
#include "settings.h"
#include <boost/log/trivial.hpp>

ClientHull::ClientHull()
{

}

ClientHull::~ClientHull()
{
    //dtor
}

void ClientHull::load(std::string hullModel)
{
		Hull::load(hullModel);
		this->hulltex = new sf::Texture;
        ///tex->setSmooth(true);
		this->hulltex->loadFromFile(this->spriteFile);
        sf::Sprite* sprite = new sf::Sprite;
        sprite->setTexture(*this->hulltex);

        sprite->setOrigin(sf::Vector2f(32,32));

        this->hullsprite = sprite;

        BOOST_LOG_TRIVIAL(info) <<  "\nClient Hull loading finished";
        return;
    

}

void ClientHull::draw(sf::RenderTarget* target)
{
	
    target->draw(*hullsprite);
	for(int i = 0; i < slotPositions.size(); i++)
    {
		if(getDevice(i))
		{
			Device* temp = getDevice(i);
			sf::Vector2f devicePosition=this->position;

			float cosr = std::cos(Util::degToRad(rotation));
			float sinr = std::sin(Util::degToRad(rotation));

			devicePosition.x+=slotPositions[i].x* cosr - slotPositions[i].y* sinr;
			devicePosition.y+=slotPositions[i].x* sinr + slotPositions[i].y* cosr;
			temp->draw(target,devicePosition,rotation);
		}
	}
}

void ClientHull::updateGraphics(float time, sf::Vector2f linVel, float angVel)
{
	/*
	sf::Vector2f extrapolated;
	if (oldPosition.x != -1 && oldPosition.y != -1)
	{		
		extrapolated.x = oldPosition.x + (linVel.x*time);
		extrapolated.y = oldPosition.y + (linVel.y*time);
	}
	//currentPosition = targetPosition;
	*/
	hullsprite->setPosition(position);
	hullsprite->setRotation(rotation);
}

