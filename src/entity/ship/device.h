#pragma once
#include "entity/physicalEntity.h"
#include "Box2D/Box2D.h"
#include <SFML/Graphics.hpp>
#include "tinyxml/tinyxml2.h"
#include "TGUI/TGUI.hpp"

class Device
{
public:
    friend class DeviceFactory;
    Device();
    virtual ~Device();
    tinyxml2::XMLElement* preparse(tinyxml2::XMLElement* deviceNode);
	virtual void config(tinyxml2::XMLElement* configNode)=0;
	virtual void writeConfig(tinyxml2::XMLElement* configNode)=0;
	virtual tgui::Widget::Ptr getConfigWidget(sf::Vector2f size, bool forceRebuild = false);
	virtual void signal(bool state)=0;

    bool hasSubEntity(void);
    virtual PhysicalEntity* pollPhysicalSubEntity()=0;

    virtual b2Vec2 pollImpulseModification(void)=0;
    virtual int pollEnergyModification(void)=0;
    virtual int pollFuelModification(void)=0;
	virtual void setPowered(){};
	virtual void setFuelled(){};
	bool isFuelSource(){return isFuelSrc;}
	bool isPowerSource(){return isPowerSrc;}
    //virtual void printInfo(void);
    virtual void draw(sf::RenderTarget * target, sf::Vector2f worldLocation, float worldRotation)=0;

	void setLocalRotation(float rotInDeg){ this->rotInDeg = rotInDeg; }
	float getLocalRotation(){return rotInDeg;}
	void setWorldPosition(sf::Vector2f);


	bool getState(){return state;}
	virtual void writeConfigToBitstream(RakNet::BitStream* bitstream);
	virtual bool readConfigFromBitstream(RakNet::BitStream* bitstream);
	virtual void writeParameters(RakNet::BitStream* bitstream);	//writes to a bitstream to persist internals
	virtual bool readParameters(RakNet::BitStream* bitstream);  //reconstructs data from bitstream
    virtual int getMass();
	std::string getType(){return type;}
	std::string getName(){return name;}
	std::string getDescription(){return description;}
	virtual std::string getInfoString();
	sf::Sprite* getEditorSprite(){return &editorSprite;}
	bool configNeedsSendFlag;

protected:
    bool state;	// is the device activated?
    bool hasSE;	// does device have a sub Entity to emitt?
	bool isPowered;
	bool isFuelled;
	bool isPowerSrc;
	bool isFuelSrc;
    float rotInDeg;
	sf::Vector2f position;
    int mass;
	std::string type;

	tgui::Widget::Ptr configWidget;
	bool wasWidgetCreated;
	tgui::Slider::Ptr rotSlider;
	tgui::Label::Ptr rotLabel;
	void configRotation();
	void configChanged();
private:
    std::string name;
    std::string description;

    int price;

    std::string formfactor;

	std::vector<int> parameters;
	std::string editorImageFile;
	sf::Texture editorTexture;
	sf::Sprite editorSprite;


};
