#include "ship/hullfactory.h"
#include "settings.h"
#include <boost/log/trivial.hpp>

HullFactory::HullFactory()
{
    //ctor
}

HullFactory::~HullFactory()
{
    //dtor
}

tinyxml2::XMLDocument* HullFactory::getXMLFromHullName(std::string name)
{
	BOOST_LOG_TRIVIAL(trace) << "Loading Hull enumeration";
	tinyxml2::XMLDocument* hullEnum = new tinyxml2::XMLDocument();
	std::string hullEnumFile = Settings::getHullPath().append("hulls.xml");
	hullEnum->LoadFile(hullEnumFile.c_str());
	

	tinyxml2::XMLElement* root = hullEnum->FirstChildElement("hulls");
	root = root->FirstChildElement("hull");
	bool exists = false;
	BOOST_LOG_TRIVIAL(trace) << "Looking up " << name;
	std::string fileName;
	while(root)
	{
		tinyxml2::XMLElement* temp = root->FirstChildElement("name");
		std::string hStr = temp->GetText();
		BOOST_LOG_TRIVIAL(trace) << "hull found: " << hStr;
		if(!hStr.compare(name))
		{
			fileName = temp->NextSiblingElement("file")->GetText();			
			exists = true;
		}
		root = root->NextSiblingElement("hull");
	}
	if(!exists)
	{
		BOOST_LOG_TRIVIAL(error) << "Unknown hull ";
		return nullptr;
	}
	else BOOST_LOG_TRIVIAL(info) << "Hull found";

	tinyxml2::XMLDocument* hullDef = new tinyxml2::XMLDocument();
	std::string hullDefPath = Settings::getHullPath().append(fileName);
	hullDef->LoadFile(hullDefPath.c_str());
	if(!hullDef)
		BOOST_LOG_TRIVIAL(error) << "Could not load " << hullDefPath.c_str();
	else BOOST_LOG_TRIVIAL(info) << "file okay " << hullDefPath.c_str(); 
	return hullDef;
}

std::vector<std::string> HullFactory::getAvailableHulls()
{
	std::vector<std::string> result;
	BOOST_LOG_TRIVIAL(trace) << "Loading Hull enumeration file";
	tinyxml2::XMLDocument* hullEnum = new tinyxml2::XMLDocument();
	std::string hullEnumFile = Settings::getHullPath().append("hulls.xml");
	hullEnum->LoadFile(hullEnumFile.c_str());
	
	tinyxml2::XMLElement* root = hullEnum->FirstChildElement("hulls");
	root = root->FirstChildElement("hull");

	while(root)
	{
		tinyxml2::XMLElement* temp = root->FirstChildElement("name");
		result.push_back(temp->GetText());
		root = root->NextSiblingElement("hull");
	}
	return result;
}