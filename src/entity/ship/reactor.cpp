#include "ship/reactor.h"
//#include <iostream>
#include "entity/serverprojectile.h"
#include "settings.h"
#include <boost/log/trivial.hpp>
Reactor::Reactor()
{
	isPowerSrc=true;
	this->type = "reactor";
	output=-1;
	currentOutput=0;

}
void Reactor::config(tinyxml2::XMLElement* configNode)
{
	BOOST_LOG_TRIVIAL(info) << "Nothing to configure";
}
void Reactor::writeConfig(tinyxml2::XMLElement* configNode)
{
	BOOST_LOG_TRIVIAL(info) << "No config to persist";
}
bool Reactor::createConfigWidget(tgui::Container::Ptr parent, std::string name)
{
	/*
	tgui::Label::Ptr mVLabel(*parent, "Config"+name);
		tgui::Slider::Ptr muzzleVelocity(*parent, "Config"+name);
		muzzleVelocity->load(Settings::getDataFolderBasePath()+"gui/widgets/Editor.conf");
		muzzleVelocity->setCallbackId(987);
		muzzleVelocity->setPosition(10,60);
		muzzleVelocity->setSize(180,20);
		muzzleVelocity->setVerticalScroll(false);
		muzzleVelocity->setMaximum(360);
		muzzleVelocity->setValue(22);
		muzzleVelocity->hide();*/
	return true;
}
void Reactor::signal(bool enabled)
{

}

PhysicalEntity* Reactor::pollPhysicalSubEntity()
{
    return 0;
}
b2Vec2 Reactor::pollImpulseModification()
{
    b2Vec2 tmp(0,0);
    return tmp;
}
int Reactor::pollEnergyModification(void)
{
	return 0;
}
int Reactor::pollFuelModification(void)
{
	return 0;
}

void Reactor::writeParameters(RakNet::BitStream* bitstream)
{
	Device::writeParameters(bitstream); // call super for state
	bitstream->Write(this->currentOutput);
	//std::cout << "write" << fuelLeft;
}

bool Reactor::readParameters(RakNet::BitStream* bitstream)
{
	Device::readParameters(bitstream); // call super for state
	//int bla=0;bitstream->Read(bla);
	bitstream->Read(this->currentOutput);
	//std::cout << "read" << fuelLeft;
	return true;
}
/*
void Reactor::printInfo(void)
{
    Device::printInfo();
	BOOST_LOG_TRIVIAL(info) 
}*/
	std::string Reactor::getInfoString()
	{
		std::stringstream s;
		s << "Power output " << output << " KW";
		return s.str();
	}
bool Reactor::drainPower(int power)
{
	currentOutput+=power;
	return true;
}
void Reactor::resetPower()
{
	currentOutput = 0;
}