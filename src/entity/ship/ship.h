#pragma once

#include <iostream>
#include <Box2D\Box2D.h>
#include <SFML\Graphics.hpp>
#include "hull.h"
#include "keymap.h"
class Ship
{

public:

	bool writeToFile(std::string filename);

	std::string getName();
	void setName(std::string name){ this->name = name; }
    std::string getDescription();
    void printDescription();
    int getMass();
    b2Vec2 getCenterOfMass();
    void setPosition(sf::Vector2f newPosition);
    void setRotation(float newRotation);
    sf::Vector2f getPosition();
    float getRotation();
	virtual Hull* getHull()=0;

protected:
	bool preparse(std::string filename);	// reads name, desc, hullName(!) call before loading Devices
	bool loadDevices(std::string filename);

	std::string definitionFilePath;
	std::string name;
    std::string description;
	std::string hullModelFile;	
};