#pragma once

#include "network/networkEntity.h"
#include "ship/hull.h"
#include "ship/clienthull.h"
#include "hullfactory.h"
#include "devicefactory.h"
#include <boost/log/trivial.hpp>
#include "ship/ship.h"
#include <SFML/System.hpp>
#define NUMSLOTS 32

class ClientShip : public NetworkEntity, public Ship
{
private:
    ClientHull* hull;	
	sf::Vector2f linearVelocityFromServer;
	float angularMomentumFromServer;
	sf::Clock timeSinceLastUpdateFromServer;
public:
	ClientShip();	
	~ClientShip();
	ClientHull* getHull();

	sf::Vector2f getLinearVelocity(){return linearVelocityFromServer;}
	float getAngularMomentum(){return angularMomentumFromServer;}
	void setTargetPosition(sf::Vector2f newPosition);
    void setTargetRotation(float newRotation);
	bool loadFromFile(std::string filename);
	//bool loadKeymap();

    void draw(sf::RenderTarget * target);
	void updateGraphics(float elapsedTimeInMilliSeconds);

	/*/ Replica Implementations /*/
	virtual RakNet::RakString getIdentifier(void) const;

	void WriteAllocationID(RakNet::Connection_RM3 *destinationConnection, RakNet::BitStream *allocationIdBitstream) const;
	virtual RM3ConstructionState QueryConstruction(RakNet::Connection_RM3 *destinationConnection, ReplicaManager3 *replicaManager3);
	virtual bool QueryRemoteConstruction(RakNet::Connection_RM3 *sourceConnection);
	virtual RM3QuerySerializationResult QuerySerialization(RakNet::Connection_RM3 *destinationConnection);
	virtual RM3ActionOnPopConnection QueryActionOnPopConnection(RakNet::Connection_RM3 *droppedConnection) const;
	virtual void SerializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *destinationConnection);
	virtual bool DeserializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *sourceConnection);
	
	RM3SerializationResult Serialize(SerializeParameters *serializeParameters);
	void Deserialize(RakNet::DeserializeParameters *deserializeParameters);
	virtual void SerializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *destinationConnection);
	virtual bool DeserializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *sourceConnection);
	void DeallocReplica(RakNet::Connection_RM3 *sourceConnection);
};