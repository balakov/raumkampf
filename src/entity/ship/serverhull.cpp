#include "ship/serverhull.h"
#include <iostream>
#include "util.h"
#include "tinyxml/tinyxml2.h"
#include "ship/hullfactory.h"
#include <boost/log/trivial.hpp>
#include "ship/tank.h"
#include "ship/reactor.h"

ServerHull::ServerHull()
{
    isBodySet=false;

}

ServerHull::~ServerHull()
{
    //dtor
}

void ServerHull::load(std::string hullModelName)
{
		Hull::load(hullModelName);
		b2Vec2 vertices[7];
		for(int i=0; i < this->collisionShapeVertices.size(); i++)
        {
			vertices[i] = b2Vec2(this->collisionShapeVertices[i].x,this->collisionShapeVertices[i].y) ;
			vertices[i].x -=32;
			vertices[i].y -=32;
			vertices[i].x /=10.0f;
			vertices[i].y /=10.0f;
			vertices[i].y = (-vertices[i].y);
		}

        this->collisionShape.Set(vertices,7);
        this->fixDef.shape = &this->collisionShape;
        this->fixDef.density = 1.0f;

        this->fixDef.friction = 0.5f;
        this->fixDef.restitution = 0.3f;
		this->fixDef.filter.groupIndex=-1;
        this->bodyDef.type = b2_dynamicBody;
        this->bodyDef.angularDamping = 1.1f;
        this->bodyDef.fixedRotation = false;
        this->bodyDef.position.Set(10.0f, 10.0f);

        BOOST_LOG_TRIVIAL(info) <<  "\nHull generation finished";
        return;
}

void ServerHull::updatePhysics(float time)
{	
	fuelDevices();
	powerDevices();
	this->rotation = (Util::radToDeg(this->body->GetAngle()));
	this->position = (Util::toSF(this->body->GetPosition()));

	for (int slot = 0; slot < slotPositions.size(); slot++)
	{
		if(getDevice(slot))
		{
			Device* device = getDevice(slot);			
			b2Vec2 point = body->GetWorldPoint(Util::toB2(slotPositions[slot]));

			b2Vec2 impulse = body->GetWorldVector(device->pollImpulseModification());
			//std::cout << "impulsex " << impulse.x << " impulsey "<< impulse.y << std::endl;
			impulse.x*=100;
			impulse.y*=100;
			//body->ApplyLinearImpulse(impulse, point, true);
			body->ApplyForce(impulse, point,true);

			if(device->hasSubEntity())
			{
				PhysicalEntity* e = device->pollPhysicalSubEntity();
				e->bodyDef.position = point;
				//e->setPosition(Util::toSF(point));

				b2Vec2 loc(e->bodyDef.linearVelocity);
				b2Vec2 glob = body->GetWorldVector(loc);

				e->bodyDef.linearVelocity = glob;
				//b2Vec2 shipImp = body->GetLinearVelocity();
				//sf::Vector2f ship = Util::toSF(shipImp);
				//sf::Vector2f temp(glob.x + ship.x,glob.y+ship.y);

				//e->setImpulse(temp);
				entities.push_back(e);
			}
		}
	}
}

void ServerHull::handleCollision(std::string impactorType, b2Vec2 impactorVelocity, float impactorMass)
{



}
void ServerHull::powerDevices()
{
	std::vector<Device*> powersources = getPowerSources();
	//std::cout << "num fuel src " << fuelsources.size();
	std::vector<Device*> sinks = getDevices();
	//std::cout << "num sinks src " << sinks.size();
	int availableFuel =0;
	for(int i=0; i < powersources.size(); i++)
	{
		Reactor* reactor = (Reactor*) powersources[i];
		reactor->resetPower();
		for(int j=0; j<sinks.size(); j++)
		{
			int needed = sinks[j]->pollEnergyModification();
			//std::cout << "dev" <<j << "needsfuel: " << needed;
			if(reactor->drainPower(needed))
				sinks[j]->setPowered();
		}
	}

}
void ServerHull::fuelDevices()
{
	std::vector<Device*> fuelsources = getFuelSources();
	//std::cout << "num fuel src " << fuelsources.size();
	std::vector<Device*> sinks = getDevices();
	//std::cout << "num sinks src " << sinks.size();
	int availableFuel =0;
	for(int i=0; i < fuelsources.size(); i++)
	{
		Tank* tank = (Tank*) fuelsources[i];
		availableFuel += tank->getFuelLeft();

		for(int j=0; j<sinks.size(); j++)
		{
			int needed = sinks[j]->pollFuelModification();
			//std::cout << "dev" <<j << "needsfuel: " << needed;
			if(tank->drainFuel(needed))
				sinks[j]->setFuelled();
		}
	}


}

