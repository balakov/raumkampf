#include "ship/devicefactory.h"
#include <iostream>
#include "ship/massdriver.h"
#include "ship/liquidfuelthruster.h"
#include "ship/reactor.h"
#include "ship/tank.h"
#include "settings.h"
#include <boost/log/trivial.hpp>
DeviceFactory::DeviceFactory()
{
    //ctor
}
Device* DeviceFactory::get(tinyxml2::XMLElement* deviceNode)
{


	std::string devicefolder = Settings::getDevicePath();

    std::string tmpdev = devicefolder;
    tmpdev.append(deviceNode->FirstChildElement()->GetText());
    BOOST_LOG_TRIVIAL(info) << "Loading Device from: " << tmpdev;
    tinyxml2::XMLDocument* devicedefinitionfile = new tinyxml2::XMLDocument();
    devicedefinitionfile->LoadFile(tmpdev.c_str());

    tinyxml2::XMLElement* base = devicedefinitionfile->FirstChildElement();
    std::string type = base->Name();

    if(type.compare("thruster")==0)
    {
        return loadThruster(base);
    }
    if(type.compare("massdriver")==0)
    {
        return loadMassDriver(base);
    }
    else
    {
		BOOST_LOG_TRIVIAL(error) << "Unknown device type: " << type;
        return nullptr;
    }

}
Device* DeviceFactory::loadThruster(tinyxml2::XMLElement* thrustNode)
{
    //std::cout << "\nGenerating Thruster: ";


    LiquidFuelThruster* device = new LiquidFuelThruster();
    thrustNode = device->preparse(thrustNode);



    thrustNode = thrustNode->NextSiblingElement("propellant");
    std::string propellant = thrustNode->GetText();
    device->propellant = propellant;

    thrustNode = thrustNode->NextSiblingElement("massflow");
    int massflow = thrustNode->IntAttribute("base");
    device->massFlow = massflow;

    thrustNode = thrustNode->NextSiblingElement("exhaustvelocity");
    int exV = thrustNode->IntAttribute("base");
    device->exhaustVelocity = exV;
    return (LiquidFuelThruster*)device;
}
Device* DeviceFactory::loadMassDriver(tinyxml2::XMLElement* drvNode)
{
    //std::cout << "\nGenerating MassDriver: ";
    MassDriver* device = new MassDriver();
    drvNode = device->preparse(drvNode);

    drvNode = drvNode->NextSiblingElement("energyusage");
    int eu = drvNode->IntAttribute("base");
    device->energyUsagePerShot = eu;


    drvNode = drvNode->NextSiblingElement("ammo");

    drvNode = drvNode->NextSiblingElement("muzzlevelocity");
    int exV = drvNode->IntAttribute("base");
    device->muzzleV = exV;

    return (Device*) device;
}
Device* DeviceFactory::loadReactor(tinyxml2::XMLElement* drvNode)
{
   // std::cout << "\nGenerating reactor: ";
    Reactor* device = new Reactor();
    drvNode = device->preparse(drvNode);
	
    drvNode = drvNode->NextSiblingElement("output");
    int eu = drvNode->IntAttribute("kilowatt");
	device->output = eu;

    return (Device*) device;
}

Device* DeviceFactory::loadTank(tinyxml2::XMLElement* drvNode)
{
    //std::cout << "\nGenerating tank: ";
    Tank* device = new Tank();
    drvNode = device->preparse(drvNode);
	
    drvNode = drvNode->NextSiblingElement("capacity");
    int eu = drvNode->IntAttribute("liter");
	device->capacity = eu;
	device->fuelLeft = eu;
    return (Device*) device;
}


Device* DeviceFactory::getByName(std::string name)
{
	BOOST_LOG_TRIVIAL(info) << "Looking up " << name;
	tinyxml2::XMLDocument* deviceEnum = new tinyxml2::XMLDocument();
	std::string deviceEnumFile = Settings::getDevicePath().append("devices.xml");
	deviceEnum->LoadFile(deviceEnumFile.c_str());
	
	tinyxml2::XMLElement* root = deviceEnum->FirstChildElement("devices");
	root = root->FirstChildElement();
	bool exists = false;
	std::string fileName;
	while(root)
	{
		tinyxml2::XMLElement* temp = root->FirstChildElement("name");
		std::string hStr = temp->GetText();
		if(!hStr.compare(name))
		{
			exists = true;
			fileName = temp->NextSiblingElement()->GetText();
			//BOOST_LOG_TRIVIAL(trace) << "Found " << name << " as " << fileName;
		}

		root = root->NextSiblingElement();
	}
	if (!exists)
	{
		BOOST_LOG_TRIVIAL(error) << "Failed to create " << name << " :Unknown device";
		return nullptr;
	}
	
	tinyxml2::XMLDocument* deviceDef = new tinyxml2::XMLDocument();
	std::string deviceDefPath = Settings::getDevicePath().append(fileName);
	//BOOST_LOG_TRIVIAL(trace) << "Loading from "<< deviceDefPath;
	deviceDef->LoadFile(deviceDefPath.c_str());
	
	std::string type = deviceDef->FirstChildElement()->Name();
	tinyxml2::XMLElement* base = deviceDef->FirstChildElement();
    if(type.compare("thruster")==0)
    {
        return loadThruster(base);
    }
    if(type.compare("massdriver")==0)
    {
        return loadMassDriver(base);
    }
    if(type.compare("reactor")==0)
    {
        return loadReactor(base);
    }
	    if(type.compare("tank")==0)
    {
        return loadTank(base);
    }
    else
    {
		BOOST_LOG_TRIVIAL(error) << "Unknown device type: "; type;
        return nullptr;
    }
}

std::vector<std::string> DeviceFactory::getAvailableDevices()
{
	std::vector<std::string> result;

	tinyxml2::XMLDocument* deviceEnum = new tinyxml2::XMLDocument();
	std::string deviceEnumFile = Settings::getDevicePath().append("devices.xml");
	deviceEnum->LoadFile(deviceEnumFile.c_str());
	BOOST_LOG_TRIVIAL(info) << "Enumerating devices from " << deviceEnumFile.c_str();
	if (deviceEnum->Error())
		BOOST_LOG_TRIVIAL(error) << deviceEnum->GetErrorStr1() << " " << deviceEnum->GetErrorStr2();
	tinyxml2::XMLElement* root = deviceEnum->FirstChildElement("devices");
	root = root->FirstChildElement();
	while(root)
	{
		tinyxml2::XMLElement* temp = root->FirstChildElement("name");
		result.push_back(temp->GetText());
		root = root->NextSiblingElement();
	}
	return result;
}