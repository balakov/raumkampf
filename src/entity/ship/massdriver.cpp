#include "ship/massdriver.h"
#include <iostream>
#include "entity/serverprojectile.h"
#include "entity/projectile.h"
#include "settings.h"
MassDriver::MassDriver()
{
	buffer.loadFromFile(Settings::getDataFolderBasePath().append("snd/pew.wav"));
    sound.setBuffer(buffer);
    sound.setVolume(Settings::getSoundVolume());
    hasFired = false;

	this->type = "massdriver";
	isCharged = false;
	maxCharge = 1000;
	currentCharge = 0;
	chargeRate = 20;
	leakage = 1;
}
void MassDriver::config(tinyxml2::XMLElement* configNode)
{
	BOOST_LOG_TRIVIAL(info) << "Nothing to configure";
}
void MassDriver::writeConfig(tinyxml2::XMLElement* configNode)
{
	BOOST_LOG_TRIVIAL(info) << "No config to persist";
}
bool MassDriver::createConfigWidget(tgui::Container::Ptr parent, std::string name)
{

/*	tgui::Label::Ptr mVLabel(*parent, "Config"+name);
		tgui::Slider::Ptr muzzleVelocity(*parent, "Config"+name);
		muzzleVelocity->load(Settings::getDataFolderBasePath()+"gui/widgets/Editor.conf");
		muzzleVelocity->setCallbackId(987);
		muzzleVelocity->setPosition(10,60);
		muzzleVelocity->setSize(180,20);
		muzzleVelocity->setVerticalScroll(false);
		muzzleVelocity->setMaximum(360);
		muzzleVelocity->setValue(22);
		muzzleVelocity->hide();*/
	return true;
}
void MassDriver::signal(bool enabled)
{

    if(enabled==true)
    {
        if(this->state==false)
        {
			if(this->isCharged)
			{
					hasSE = true;
					hasFired = true;
					clock.restart();
			}
            return;
        }
        else
        {
            return;

        }
    }
    else
    {
        //hasSE = false;
        if(this->state==false)
        {

            return;
        }
        else
        {

            state = false;
            ///std::cout << "\nLFE stopped";
        }
    }
}

PhysicalEntity* MassDriver::pollPhysicalSubEntity()
{
    sound.play();
    this->hasSE=false;
	this->currentCharge=0;
	isCharged=false;
    sf::Vector2f tmp;
    tmp.x = cos((rotInDeg)*3.14159265359/180) * (muzzleV)*1000;
    tmp.y = sin((rotInDeg)*3.14159265359/180) * (muzzleV)*1000;
    return new ServerProjectile(position, tmp);
}
b2Vec2 MassDriver::pollImpulseModification()
{
    b2Vec2 tmp(0,0);
    if(hasFired)
    {
        tmp.x = -cos((rotInDeg)*3.14159265359/180) * (muzzleV)*0.01f;
        tmp.y = -sin((rotInDeg)*3.14159265359/180) * (muzzleV)*0.01f;
    }


    hasFired = false;
    return tmp;

}


std::string MassDriver::getInfoString(void)
{
	std::stringstream s;
	s << Device::getInfoString();
    s << "\nenergy usage: " << this->energyUsagePerShot << " MW per Shot";
    s << "\nmuzzle velocity: " << this->muzzleV << " meter/second";
    s << "\n---------------";
			return s.str();
}
int MassDriver::pollEnergyModification(void)
{
	if(this->isCharged)
		return leakage;
	else return chargeRate;
}
int MassDriver::pollFuelModification(void)
{
	return 0;
}
void MassDriver::writeParameters(RakNet::BitStream* bitstream)
{
	Device::writeParameters(bitstream); // call super for state
	bitstream->Write(this->currentCharge);
	//std::cout << "write" << fuelLeft;
}

bool MassDriver::readParameters(RakNet::BitStream* bitstream)
{
	Device::readParameters(bitstream); // call super for state
	//int bla=0;bitstream->Read(bla);
	bitstream->Read(this->currentCharge);
	//std::cout << "read" << fuelLeft;
	return true;
}
void MassDriver::setPowered()
{
	if(this->currentCharge + this->chargeRate >= this->maxCharge)
	{
		this->currentCharge = this->maxCharge;
		isCharged=true;
	}
	else this->currentCharge+=this->chargeRate;

}

float MassDriver::getChargePercent()
{
	return (float)currentCharge/(float)maxCharge;

}