#include "ship/tank.h"
//#include <iostream>
#include "entity/serverprojectile.h"
#include "settings.h"
#include <boost/log/trivial.hpp>
Tank::Tank()
{
	isFuelSrc=true;
	capacity=7777;
}
void Tank::config(tinyxml2::XMLElement* configNode)
{
	BOOST_LOG_TRIVIAL(info) << "Nothing to configure";
}
void Tank::writeConfig(tinyxml2::XMLElement* configNode)
{
	BOOST_LOG_TRIVIAL(info) << "No config to persist";
}
bool Tank::createConfigWidget(tgui::Container::Ptr parent, std::string name)
{

/*	tgui::Label::Ptr mVLabel(*parent, "Config"+name);
		tgui::Slider::Ptr muzzleVelocity(*parent, "Config"+name);
		muzzleVelocity->load(Settings::getDataFolderBasePath()+"gui/widgets/Editor.conf");
		muzzleVelocity->setCallbackId(987);
		muzzleVelocity->setPosition(10,60);
		muzzleVelocity->setSize(180,20);
		muzzleVelocity->setVerticalScroll(false);
		muzzleVelocity->setMaximum(360);
		muzzleVelocity->setValue(22);
		muzzleVelocity->hide();*/
	return true;
}
void Tank::signal(bool enabled)
{

}

PhysicalEntity* Tank::pollPhysicalSubEntity()
{
    return 0;
}
b2Vec2 Tank::pollImpulseModification()
{
    b2Vec2 tmp(0,0);
    return tmp;
}
int Tank::pollEnergyModification(void)
{
	return 0;
}
int Tank::pollFuelModification(void)
{
	return 0;
}

void Tank::writeParameters(RakNet::BitStream* bitstream)
{
	Device::writeParameters(bitstream); // call super for state
	bitstream->Write(this->fuelLeft);
	//std::cout << "write" << fuelLeft;
}

bool Tank::readParameters(RakNet::BitStream* bitstream)
{
	Device::readParameters(bitstream); // call super for state
	//int bla=0;bitstream->Read(bla);
	bitstream->Read(this->fuelLeft);
	//std::cout << "read" << fuelLeft;
	return true;
}
/*
void Tank::printInfo(void)
{
    Device::printInfo();
	BOOST_LOG_TRIVIAL(info) << "Tank capacity: " << capacity << " Ltr";
	BOOST_LOG_TRIVIAL(info) << "Fuel left: " << fuelLeft << " Ltr";
}*/

std::string Tank::getInfoString()
{
	std::stringstream s;
	s << Device::getInfoString();
	s << "Tank capacity: " << capacity << " Ltr";
	s << "\nFuel left: " << fuelLeft << " Ltr";
	return s.str();
}
bool Tank::drainFuel(int amount)
{
	//if(amount!=0)std::cout <<  "\ndraining: " << amount;
	if(fuelLeft>=amount)
	{
		fuelLeft-=amount;
		return true;
	}
	else return false;
}
void Tank::fill()
{
	fuelLeft = capacity;
}

int Tank::getMass()
{
	return this->mass + fuelLeft;
}