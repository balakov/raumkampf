#include "ship/device.h"
#include <SFML/Audio.hpp>
#include "entity/physicalEntity.h"
#include "entity.h"
class Reactor : public Device
{
public:

    friend class DeviceFactory;
	Reactor();
	virtual void config(tinyxml2::XMLElement* configNode);
	virtual void writeConfig(tinyxml2::XMLElement* configNode);
	virtual bool createConfigWidget(tgui::Container::Ptr parent, std::string name);
    void signal(bool state);
	virtual void draw(sf::RenderTarget * target, sf::Vector2f worldLocation, float worldRotation){;}
	PhysicalEntity* pollPhysicalSubEntity();
    b2Vec2 pollImpulseModification(void);
    virtual int pollEnergyModification(void);
    virtual int pollFuelModification(void);

	virtual void writeParameters(RakNet::BitStream* bitstream);
	virtual bool readParameters(RakNet::BitStream* bitstream);

    //virtual void printInfo(void);
	std::string getInfoString();
	float getCurrentLoadPercent(){return (float)currentOutput/(float)output;}
	int getCurrentLoadAbs(){return currentOutput;}
	int getMaxOutput(){return output;}
	bool drainPower(int power);
	void resetPower();
protected:
private:
    sf::Clock clock;
    int temperature;

	int output;
	int currentOutput;
};	

