#include "ship/hull.h"
#include <boost/log/trivial.hpp>
#include "tinyxml/tinyxml2.h"
#include "ship/hullfactory.h"
#include "settings.h"
Hull::Hull()
{
	armor = 3;
	armorLeft = armor;
	bluntForceResilience = 1;
	needsToSendDeviceUpdate = false;
	isDirty = false;
	oldPosition.x = oldPosition.y = -1;
}
Hull::~Hull()
{
	std::vector<Device*>d = getDevices();
	for (Device* dvc : d)
		delete dvc;
}

void Hull::load(std::string hullModel)
{
	BOOST_LOG_TRIVIAL(error) << hullModel;
	tinyxml2::XMLDocument* hulldefinition = HullFactory::getXMLFromHullName(hullModel);	
    tinyxml2::XMLElement* root = hulldefinition->FirstChildElement();

    std::string temp(root->Name());
    if(temp.compare("hull")!=0)
    {
		BOOST_LOG_TRIVIAL(error) << "Not a hull file";
        return;
    }
    else
    {
        tinyxml2::XMLElement* tmpNode = root->FirstChildElement();
        std::string name = tmpNode->FirstChild()->Value();
		this->hullModelName = name;
		BOOST_LOG_TRIVIAL(info) << "Hull name: "<<name;

        tmpNode = tmpNode->NextSiblingElement();
        int basemass = tmpNode->IntAttribute("base");
        this->mass = basemass;
        BOOST_LOG_TRIVIAL(info) << "Base mass: " << this->mass <<" tons";

        tmpNode = tmpNode->NextSiblingElement();
        int cmx = tmpNode->IntAttribute("x");
        int cmy = tmpNode->IntAttribute("y");
		this->centerOfMass = sf::Vector2f(cmx,cmy);
        BOOST_LOG_TRIVIAL(info) << "\nCenter of mass at: " << cmx <<":"<<cmy<<" relative";


        tmpNode = tmpNode->NextSiblingElement();
        BOOST_LOG_TRIVIAL(info) <<  "\nDescription: " << tmpNode->FirstChild()->Value();
		this->description = tmpNode->FirstChild()->Value();

		tmpNode = tmpNode->NextSiblingElement();
        tinyxml2::XMLElement* imgNode = tmpNode;
        imgNode = imgNode->FirstChildElement();
		this->spriteFile = Settings::getHullImagePath() + imgNode->FirstChild()->Value();
        BOOST_LOG_TRIVIAL(info) <<  "\nSprite: " << spriteFile ;

        imgNode = imgNode->NextSiblingElement();
        std::string hullschematicfile = imgNode->FirstChild()->Value();
        this->schematicFile = hullschematicfile;
		BOOST_LOG_TRIVIAL(info) <<  "\nSchematic: " << schematicFile;

		tmpNode = tmpNode->NextSiblingElement();
        tinyxml2::XMLElement* sltNode = tmpNode;
        int numSlots = sltNode->IntAttribute("num");
        BOOST_LOG_TRIVIAL(info) <<  "\n" << numSlots << " Slots available";

        sltNode = sltNode->FirstChildElement();
        for(int i=0; i<numSlots; i++)
        {
			float x = sltNode->FloatAttribute(("x"));
            float y = sltNode->FloatAttribute(("y"));
			x-=32;
			y-=32;
            BOOST_LOG_TRIVIAL(info) <<  " " << i << " x: " << x << ":" << y;
            sltNode = sltNode->NextSiblingElement();
            this->slotPositions.push_back(sf::Vector2f(x,y));
        }

        /// loading collision shape
        tmpNode = tmpNode->NextSiblingElement();
        int numVert = tmpNode->IntAttribute("points");
        tinyxml2::XMLElement* pntNode = tmpNode->FirstChildElement();
        
		b2Vec2 vertices[7];
		
        BOOST_LOG_TRIVIAL(info) <<  "Vertices in hull shape: "<<numVert;
        for(int i=0; i < numVert; i++)
        {
            float x = pntNode->IntAttribute("x");
            float y = pntNode->IntAttribute("y");
			BOOST_LOG_TRIVIAL(info) <<  "Vertex #"<<i<<": " <<x<<","<<y;
			this->collisionShapeVertices.push_back(sf::Vector2f(x,y));
			/*
            x-=32;  ///convert to center
            y-=32;
            x/=10.0f;  ///scale for b2d
            y/=10.0f;
            vertices[i].Set(x,-y);  /// inverting y for b2 coordinate system*/
            pntNode = pntNode->NextSiblingElement();
        }
        BOOST_LOG_TRIVIAL(info) <<  "\nHull generation finished";
        return;
    }


}
void Hull::setPosition(sf::Vector2f position)
{
	this->oldPosition = this->position;
	this->position = position;
}
void Hull::setRotation(float rotation)
{
	this->rotation = rotation;
}	
sf::Vector2f Hull::getPosition()
{
	return position;
}
float Hull::getRotation()
{
	return this->rotation;
}
std::string Hull::getModelName()
{
	return this->hullModelName;
}
std::string Hull::getModelDescription()
{
	return this->description;
}
sf::Vector2f Hull::getCenterOfMass()
{
	return this->centerOfMass;
}
int Hull::getMass()
{
	return this->mass;
}

std::vector<Device*> Hull::getDevices()
{
	std::vector<Device*> values;
	for (std::map<int,Device*>::iterator it=devices.begin(); it!=devices.end(); ++it) 
	values.push_back( it->second );
	return values;
}
Device* Hull::getDevice(int slotNumber)
{
	if(devices.empty())
	{
        BOOST_LOG_TRIVIAL(warning) << "Hull has no devices";
		return nullptr;
	}
	std::map<int,Device*>::iterator it = devices.find(slotNumber);
	if(it != devices.end())
		return it->second;
	else {
		//BOOST_LOG_TRIVIAL(warning) << "Slot "<<slotNumber<<" is empty";
		return nullptr;
	}
}
sf::Vector2f Hull::getSlotPosition(int slotNumber)
{
	if(slotNumber<=slotPositions.size()&&slotNumber>=0)
	{
		return slotPositions[slotNumber];
	}
	else {
		BOOST_LOG_TRIVIAL(warning) << "Slot "<<slotNumber<<" does not exist";
		return sf::Vector2f(0,0);
	}
}
void Hull::setDevice(Device* device, int slotNumber)
{
	removeDevice(slotNumber);
	devices.insert(std::pair<int, Device*>(slotNumber, device));
}
void Hull::removeAllDevices()
{
	int numdel = 0;
	for (int i = 0; i < this->getNumberOfSlots(); i++)
	{
		if (this->getDevice(i) != nullptr)
		{
			numdel++;
			this->removeDevice(i);
		}
	}
	std::cout << "Removed " << numdel << " devices";

}
void Hull::removeDevice(int slotNumber)
{
	Device* d = this->getDevice(slotNumber);
	delete d;
	d = 0;
	int i = devices.erase(slotNumber);
	if (i > 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "Removed a device";
	}
	else BOOST_LOG_TRIVIAL(debug) << "Tried to remove a device, but slot " <<slotNumber <<" was empty";
}
bool Hull::setDeviceState(int slot, bool state)
{
	std::map<int,Device*>::iterator it = devices.find(slot);
	if(it==devices.end())
	{
		BOOST_LOG_TRIVIAL(warning) << "Slot "<<slot<<" does not exist, cannot set state";
		return false;
	}
	else {
		it->second->signal(state);
		return true;
	}
}
void Hull::clearDeviceState()
{
	std::vector<Device*>d = getDevices();
	for(int i=0; i < d.size(); i++)
	{
		d[i]->signal(false);
	}
}
int Hull::getNumberOfSlots()
{
	return this->slotPositions.size();
}

std::string Hull::getSchematicFile()
{
	return this->schematicFile;
}

std::vector<Device*> Hull::getPowerSources()
{
	std::vector<Device*> result;
	std::vector<Device*>d = getDevices();
	for(int i=0; i < d.size(); i++)
	{
		if(d[i]->isPowerSource())
		{
				result.push_back(d[i]);
		}
	
	}
	return result;
}

std::vector<Device*> Hull::getFuelSources()
{
	std::vector<Device*> result;
	std::vector<Device*>d = getDevices();
	for(int i=0; i < d.size(); i++)
	{
		if(d[i]->isFuelSource())
		{
				result.push_back(d[i]);
		}
	
	}
	return result;
}

bool Hull::handleProjectileImpact()
{
	std::cout << "Handle project impact";
	this->armorLeft--;
	if(armorLeft<=0)
		return true;
	else return false;
}

void Hull::reset()
{
	armorLeft = armor;

}