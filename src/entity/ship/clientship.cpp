#include "ship/clientship.h"

#include "settings.h"
#include "util.h"
#include "globals.h"
#include "container\clientgame.h"
ClientShip::ClientShip()
{

}
ClientShip::~ClientShip()
{
	delete this->hull;

}
void ClientShip::updateGraphics(float timeInSeconds)
{
	hull->updateGraphics(timeSinceLastUpdateFromServer.getElapsedTime().asSeconds() , this->linearVelocityFromServer, this->angularMomentumFromServer);
}


void ClientShip::draw(sf::RenderTarget * target)
{
    hull->draw(target);
}
void ClientShip::setTargetPosition(sf::Vector2f newPosition)
{

    hull->setPosition(newPosition);
}
void ClientShip::setTargetRotation(float newRotation)
{
    hull->setRotation(newRotation);
}

ClientHull* ClientShip::getHull()
{
	if(hull!=nullptr)
		return hull;
	else return nullptr;
}

bool ClientShip::loadFromFile(std::string filename)
{
	Ship::preparse(filename);
	this->hull = new ClientHull();
	this->hull->load(this->hullModelFile);
	Ship::loadDevices(filename);
	//this->loadKeymap();
	return true;
}
/*
bool ClientShip::loadKeymap()
{
	BOOST_LOG_TRIVIAL(info) << "Loading Keymap: " << this->keyMapFile;
	this->keymap = new KeyMap(this->keyMapFile);
	//this->keymap = new KeyMap(Settings::getShipPath()+this->keyMapFile);
	return true;
}*/
	/*/ Replica Implementations /*/
	RakNet::RakString ClientShip::getIdentifier(void) const {return "Ship";}

	void ClientShip::WriteAllocationID(RakNet::Connection_RM3 *destinationConnection, RakNet::BitStream *allocationIdBitstream) const {
		allocationIdBitstream->Write(getIdentifier());
	}
	RM3ConstructionState ClientShip::QueryConstruction(RakNet::Connection_RM3 *destinationConnection, ReplicaManager3 *replicaManager3) {

		return QueryConstruction_ServerConstruction(destinationConnection,false);
	}
	bool ClientShip::QueryRemoteConstruction(RakNet::Connection_RM3 *sourceConnection) {
		return QueryRemoteConstruction_ServerConstruction(sourceConnection,false);
	}
	RM3QuerySerializationResult ClientShip::QuerySerialization(RakNet::Connection_RM3 *destinationConnection) {
		return RakNet::RM3QuerySerializationResult::RM3QSR_CALL_SERIALIZE;// QuerySerialization_ServerSerializable(destinationConnection, false);
	}
	RM3ActionOnPopConnection ClientShip::QueryActionOnPopConnection(RakNet::Connection_RM3 *droppedConnection) const {
		return QueryActionOnPopConnection_Server(droppedConnection);
	}
	void ClientShip::SerializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *destinationConnection)
	{
		BOOST_LOG_TRIVIAL(warning) << "why is client serializing ship construction?";
	}
	bool ClientShip::DeserializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *sourceConnection)
	{
		BOOST_LOG_TRIVIAL(info) << "Deserializing Construction";


		RakString shipName, shipDesc;
		constructionBitstream->Read(shipName);
		constructionBitstream->Read(shipDesc);
		BOOST_LOG_TRIVIAL(info) << "Name: " << shipName << " Desc: " << shipDesc;
		this->name = shipName;
		this->description = shipDesc;

		RakString name;
		constructionBitstream->Read(name);
		BOOST_LOG_TRIVIAL(info) << "Received Hull name " << name;
		this->hull = new ClientHull;
		this->hull->load(name.C_String());
		constructionBitstream->Read(owner); 
		for(int idx=0; idx<this->getHull()->getNumberOfSlots();idx++)
		{
			RakString deviceName;
			constructionBitstream->Read(deviceName);
			if(deviceName=="EMPTYSLOT")
				continue;
			else {
				BOOST_LOG_TRIVIAL(info) << "Receiving " << deviceName << " for slot " << idx;;
				Device* device = DeviceFactory::getByName(deviceName.C_String());
				float rot;constructionBitstream->Read(rot);
				device->setLocalRotation(rot);
				this->getHull()->setDevice(device, idx);
			}
		}
		BOOST_LOG_TRIVIAL(info) << "linking ClientShip to owner";
		if (ClientGame::getInstance()->getUserByGUID(this->owner)!=nullptr)
			ClientGame::getInstance()->getUserByGUID(this->owner)->setShip(this);
		else BOOST_LOG_TRIVIAL(info) << "Owner not found. Editor mode?";
		BOOST_LOG_TRIVIAL(info) << "Deserialization complete";
		return true;
	}
	// serializes device configuration ONLY
	RM3SerializationResult ClientShip::Serialize(SerializeParameters *serializeParameters)	{
		/*BOOST_LOG_TRIVIAL(warning) << "why is client trying to serialize ship?";

			for (int i = 0; i < NUMSLOTS; i++)
			{
				//bool b = deserializeParameters->serializationBitstream->ReadBit();
				if (hull->getDevice(i))
				{
					std::cout << "\nClientship serialize device ["<<i<<"] " << hull->getDevice(i)->getLocalRotation();
					hull->getDevice(i)->writeConfigToBitstream(serializeParameters->outputBitstream);
					//hull->setDeviceState(i,b);
				}
			}*/
			return RM3SR_DO_NOT_SERIALIZE;
	}
	void ClientShip::Deserialize(RakNet::DeserializeParameters *deserializeParameters) {
			sf::Vector2f pos;
			deserializeParameters->serializationBitstream->Read(pos);
			this->setTargetPosition(pos);
			float rot;
			deserializeParameters->serializationBitstream->Read(rot);
			this->setTargetRotation(rot);
			int armorLeft;
			deserializeParameters->serializationBitstream->Read(armorLeft);
			this->getHull()->setArmorLeft(armorLeft);

			float x,y;
			deserializeParameters->serializationBitstream->Read(x);
			deserializeParameters->serializationBitstream->Read(y);
			this->linearVelocityFromServer.x = x;
			this->linearVelocityFromServer.y = y;

			deserializeParameters->serializationBitstream->Read(angularMomentumFromServer);

			bool dvcupd;
			deserializeParameters->serializationBitstream->Read(dvcupd);

			if (dvcupd)
			{
				BOOST_LOG_TRIVIAL(info) << "Receiving device update";
				this->getHull()->isDirty = true;
				this->getHull()->removeAllDevices();
				for (int idx = 0; idx<this->getHull()->getNumberOfSlots(); idx++)
				{
					RakString deviceName;
					deserializeParameters->serializationBitstream->Read(deviceName);
					if (deviceName == "EMPTYSLOT")
						continue;
					else {
						BOOST_LOG_TRIVIAL(info) << "Receiving " << deviceName << " for slot " << idx;;
						Device* device = DeviceFactory::getByName(deviceName.C_String());
						float rot; deserializeParameters->serializationBitstream->Read(rot);
						device->setLocalRotation(rot);
						this->getHull()->setDevice(device, idx);
					}
				}
			}
			
		for(int i=0; i < NUMSLOTS; i++)
		{
			//bool b = deserializeParameters->serializationBitstream->ReadBit();
			if(hull->getDevice(i))
			{
				hull->getDevice(i)->readParameters(deserializeParameters->serializationBitstream);
				//hull->setDeviceState(i,b);
			}
		}
			//BOOST_LOG_TRIVIAL(trace) << "Position update: " << name;

		this->timeSinceLastUpdateFromServer.restart();
	}

	void ClientShip::SerializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *destinationConnection){}
	bool ClientShip::DeserializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *sourceConnection){return true;}
	void ClientShip::DeallocReplica(RakNet::Connection_RM3 *sourceConnection) {
		delete this;
	}
