#ifndef LIQUIDFUELTHRUSTER_H
#define LIQUIDFUELTHRUSTER_H

#include "device.h"

#include <SFML/Audio.hpp>
class LiquidFuelThruster : public Device
{
public:
    friend class DeviceFactory;
    LiquidFuelThruster();
    virtual ~LiquidFuelThruster();
	virtual void config(tinyxml2::XMLElement* configNode);
	virtual void writeConfig(tinyxml2::XMLElement* configNode);
	virtual tgui::Widget::Ptr getConfigWidget(sf::Vector2f size, bool forceRebuild = false);
    void signal(bool state);
    PhysicalEntity* pollPhysicalSubEntity();
    b2Vec2 pollImpulseModification();
    int pollEnergyModification(void);
    int pollFuelModification(void);


	void setFuelled();

    virtual void draw(sf::RenderTarget * target, sf::Vector2f worldLocation, float worldRotation);
    //virtual void printInfo(void);
	std::string getInfoString();
protected:
private:
    sf::Clock clock;
	int burnTimeSinceLastPoll;
    float exhaustVelocity; /// meter/seconds
    float massFlow; /// kg/seconds
    std::string propellant;
    b2Vec2 accumulatedForce;

    sf::Vector2f parentNodeWorldPosition;
    sf::Vector2f parentNodeWorldRotation;
    bool hasPlume;
    sf::Texture exhaustTex;
    sf::Sprite exhaustPlume;

    sf::SoundBuffer* buffer;
    sf::Sound* sound;


};

#endif // LIQUIDFUELTHRUSTER_H
