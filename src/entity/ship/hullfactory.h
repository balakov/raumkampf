#ifndef HULLFACTORY_H
#define HULLFACTORY_H

#include <iostream>
#include "tinyxml/tinyxml2.h"
#include "util.h"

class HullFactory
{
public:
    HullFactory();
    ~HullFactory();
	static std::vector<std::string> getAvailableHulls(); 
	static tinyxml2::XMLDocument* getXMLFromHullName(std::string name);

protected:
private:
	
};

#endif // HULLFACTORY_H
