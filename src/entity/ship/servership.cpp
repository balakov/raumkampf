#include "ship/servership.h"
#include "util.h"
ServerShip::ServerShip()
{
	markForRespawn = false;
	this->bodySet = false;
}
bool ServerShip::loadFromFile(std::string filename)
{
	Ship::preparse(filename);
	this->hull = new ServerHull;
	hull->load(this->hullModelFile); 
	Ship::loadDevices(filename);
	return true;
}
void ServerShip::updatePhysics(float timeInSeconds)
{
	b2MassData* md = new b2MassData;
	 hull->getBody()->GetMassData(md);
	 //std::cout << "\n" << md->mass << " " << md->I << " " << md->center.x << "|" << md->center.y << "own: " << this->getMass();
	 md->mass = this->getMass()/1000;
	 hull->getBody()->SetMassData(md);
	hull->updatePhysics(timeInSeconds);
}

void ServerShip::handleCollision(float linearSpeed, float impactorMass, RakNetGUID impactorGUID)
{

	if(linearSpeed>10)
	{
		markForRespawn = this->getHull()->handleProjectileImpact();
		killingBlow = impactorGUID;

		BOOST_LOG_TRIVIAL(info) << "Ship impacts at "<<linearSpeed<<" m/s by " << impactorGUID.ToString();
		BOOST_LOG_TRIVIAL(info) << "KLONK by " << killingBlow.ToString();
	}

}

ServerHull* ServerShip::getHull()
{
    return hull;
}

/*/ Replica Implementations /*/
RakNet::RakString ServerShip::getIdentifier(void) const { return "Ship"; }

void ServerShip::WriteAllocationID(RakNet::Connection_RM3 *destinationConnection, RakNet::BitStream *allocationIdBitstream) const {
	allocationIdBitstream->Write(getIdentifier());
}
RM3ConstructionState ServerShip::QueryConstruction(RakNet::Connection_RM3 *destinationConnection, ReplicaManager3 *replicaManager3) {

	return QueryConstruction_ServerConstruction(destinationConnection, true);
}
bool ServerShip::QueryRemoteConstruction(RakNet::Connection_RM3 *sourceConnection) {
	return QueryRemoteConstruction_ServerConstruction(sourceConnection, true);
}
RM3QuerySerializationResult ServerShip::QuerySerialization(RakNet::Connection_RM3 *destinationConnection) {
	return QuerySerialization_ServerSerializable(destinationConnection, true);
}
RM3ActionOnPopConnection ServerShip::QueryActionOnPopConnection(RakNet::Connection_RM3 *droppedConnection) const {
	return QueryActionOnPopConnection_Server(droppedConnection);
}
void ServerShip::SerializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *destinationConnection)
{
	BOOST_LOG_TRIVIAL(info) << "Serializing Construction";

	RakString shipName = this->getName().c_str();
	constructionBitstream->Write(shipName);

	RakString shipDesc = this->getDescription().c_str();
	constructionBitstream->Write(shipDesc);

	RakString name = hull->getModelName().c_str();
	constructionBitstream->Write(name);
	constructionBitstream->Write(this->owner);



	for (int idx = 0; idx<this->getHull()->getNumberOfSlots(); idx++)
	{
		BOOST_LOG_TRIVIAL(info) << "Slot " << this->getHull()->getNumberOfSlots() << "/" << idx;
		if (!this->getHull()->getDevice(idx))
		{
			BOOST_LOG_TRIVIAL(info) << "Empty Slot";
			constructionBitstream->Write("EMPTYSLOT");
		}
		else {
			RakString devicename = this->getHull()->getDevice(idx)->getName().c_str();
			constructionBitstream->Write(devicename);
			constructionBitstream->Write(this->getHull()->getDevice(idx)->getLocalRotation());
			BOOST_LOG_TRIVIAL(info) << "Serializing " << devicename;
		}
	}
	BOOST_LOG_TRIVIAL(info) << "Serializing Construction complete";
}
bool ServerShip::DeserializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *sourceConnection)
{
	BOOST_LOG_TRIVIAL(info) << "Why is server deserializing ship construction";
	return true;
}

RM3SerializationResult ServerShip::Serialize(SerializeParameters *serializeParameters)	{


	serializeParameters->pro[0].reliability = RELIABLE_SEQUENCED;
	serializeParameters->outputBitstream->Write(this->getHull()->getPosition());
	serializeParameters->outputBitstream->Write(this->getHull()->getRotation());
	serializeParameters->outputBitstream->Write(this->getHull()->getArmorLeft());



	float linVelX = 0;
	float linVelY = 0;
	float angMom = 0;

	if (this->hasBody()) // may fail when ship is constructed but not added to map where it gets a body
	{
		//BOOST_LOG_TRIVIAL(info) << this->getHull()->body->GetLinearVelocity().x << "|"<<this->getHull()->body->GetLinearVelocity().y;
		linVelX = this->getHull()->body->GetLinearVelocity().x;
		linVelY = this->getHull()->body->GetLinearVelocity().y;
		angMom = this->getHull()->body->GetAngularVelocity();
	}
	serializeParameters->outputBitstream->Write(linVelX);
	serializeParameters->outputBitstream->Write(linVelY);
	serializeParameters->outputBitstream->Write(angMom);

	if (this->hull->needsToSendDeviceUpdate)
	{
		BOOST_LOG_TRIVIAL(info) << "device update flag set";
		serializeParameters->outputBitstream->Write1();
		for (int idx = 0; idx<this->getHull()->getNumberOfSlots(); idx++)
		{

			if (!this->getHull()->getDevice(idx))
			{
				BOOST_LOG_TRIVIAL(info) << "Empty Slot";
				serializeParameters->outputBitstream->Write("EMPTYSLOT");
			}
			else {
				RakString devicename = this->getHull()->getDevice(idx)->getName().c_str();
				serializeParameters->outputBitstream->Write(devicename);
				serializeParameters->outputBitstream->Write(this->getHull()->getDevice(idx)->getLocalRotation());
				BOOST_LOG_TRIVIAL(info) << "Serializing " << devicename;
			}
		}
		this->hull->needsToSendDeviceUpdate = false;
	}
	else serializeParameters->outputBitstream->Write0();
	for (int i = 0; i < NUMSLOTS; i++)
	{
		//BOOST_LOG_TRIVIAL(info) << "Serializing device "<<i;
		if (hull->getDevice(i))
		{
			hull->getDevice(i)->writeParameters(serializeParameters->outputBitstream);
			/*
			//Write state
			if(hull->getDevice(i)->getState())
			serializeParameters->outputBitstream->Write1();
			else serializeParameters->outputBitstream->Write0();
			//Write parameters*/
			//BOOST_LOG_TRIVIAL(info) << "Serializing device type"<<hull->getDevice(i)->getType();
		}
		//else serializeParameters->outputBitstream->Write0();
	}

	return RM3SR_SERIALIZED_ALWAYS;
}
// Deserializes device configuration ONLY! 
void ServerShip::Deserialize(RakNet::DeserializeParameters *deserializeParameters) {
	BOOST_LOG_TRIVIAL(warning) << "Why is server deserializing a ship?";
	for (int i = 0; i < NUMSLOTS; i++)
	{
		//bool b = deserializeParameters->serializationBitstream->ReadBit();
		if (hull->getDevice(i))
		{
			hull->getDevice(i)->readConfigFromBitstream(deserializeParameters->serializationBitstream);
			//hull->setDeviceState(i,b);
		}
	}
}

void ServerShip::SerializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *destinationConnection){}
bool ServerShip::DeserializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *sourceConnection){ return true; }
void ServerShip::DeallocReplica(RakNet::Connection_RM3 *sourceConnection) {
	delete this;
}

