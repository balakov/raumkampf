#pragma once
#include <SFML/Graphics.hpp>
#include "ship/Device.h"

#define MAXDEVICES 32

class Hull
{
private:


protected:
	std::string hullModelName;
	std::string description;
    int mass;
	float damage;
	sf::Vector2f centerOfMass;
	sf::Vector2f oldPosition; // where it was last time
	sf::Vector2f position;	// where it should be
	float rotation;
	std::map<int,Device*> devices;
	std::map<int,sf::Vector2f>slots;
    std::vector<sf::Vector2f>slotPositions;
	std::vector<sf::Vector2f> collisionShapeVertices;
	std::string schematicFile;
	std::string spriteFile;
	int armor;
	int armorLeft;
	float bluntForceResilience;

public:
    Hull();
    ~Hull();
	bool needsToSendDeviceUpdate; //server hull needs to resend devices
	bool isDirty; //client needs to repaint/whatever
	void load(std::string hullModel);
    void setPosition(sf::Vector2f position);
	void setRotation(float rotation);	
	sf::Vector2f getPosition();
	float getRotation();
	std::string getModelName();
	std::string getModelDescription();
	sf::Vector2f getCenterOfMass();
    int getMass();
	std::string getSchematicFile();
	std::vector<Device*> getDevices();
	std::vector<Device*> getPowerSources();
	std::vector<Device*> getFuelSources();
	float getDamagePercent();
	int getArmor(){return armor;}
	int getArmorLeft(){return armorLeft;}
	void setArmorLeft(int armorLeft){this->armorLeft = armorLeft;}

	bool handleProjectileImpact(); //true if hull got destroyed
    Device* getDevice(int slotNumber);
    sf::Vector2f getSlotPosition(int slotNumber);
    void setDevice(Device* device, int slotNumber);
	void removeDevice(int slotNumber);
	void removeAllDevices();
	bool setDeviceState(int slot, bool state);
	void clearDeviceState();
    int getNumberOfSlots();
	void reset();
};
