#ifndef MASSDRIVER_H
#define MASSDRIVER_H

#include "ship/device.h"
#include <SFML/Audio.hpp>
#include "entity/physicalEntity.h"
#include "entity.h"
class MassDriver : public Device
{
public:

    friend class DeviceFactory;
    MassDriver();
	virtual void config(tinyxml2::XMLElement* configNode);
	virtual void writeConfig(tinyxml2::XMLElement* configNode);
	virtual bool createConfigWidget(tgui::Container::Ptr parent, std::string name);
    void signal(bool state);
	virtual void draw(sf::RenderTarget * target, sf::Vector2f worldLocation, float worldRotation){;}
	PhysicalEntity* pollPhysicalSubEntity();
    b2Vec2 pollImpulseModification(void);

	virtual void writeParameters(RakNet::BitStream* bitstream);
	virtual bool readParameters(RakNet::BitStream* bitstream);

    virtual int pollEnergyModification(void);
    virtual int pollFuelModification(void);
    virtual void setPowered();
	std::string getInfoString();

	float getChargePercent();
protected:
private:
    sf::SoundBuffer buffer;
    sf::Sound sound;
    int energyUsagePerShot;
    sf::Clock clock;
    int rateOfFire;
    int temperature;
    float muzzleV;
    int cooldowninmillis;
    bool hasFired;
	
	bool isCharged;
	int maxCharge;
	int currentCharge;
	int chargeRate;
	int leakage;
};

#endif // MASSDRIVER_H
