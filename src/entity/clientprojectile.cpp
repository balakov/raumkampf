#include "entity/clientprojectile.h"
#include <iostream>

ClientProjectile::ClientProjectile()
{
    circle = sf::CircleShape(2, 4);
    circle.setOrigin(1,1);
    circle.setFillColor(sf::Color::Yellow);
    lastUpdate=0;
}

ClientProjectile::~ClientProjectile()
{
    //dtor
}

void ClientProjectile::draw(sf::RenderTarget* target)
{
    circle.setPosition(position);
    target->draw(circle);
}

void ClientProjectile::update(float deltaT)
{

    position.x = position.x + (impulse.x * deltaT);
    position.y = position.y + (impulse.y * deltaT);

}
