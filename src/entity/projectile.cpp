#include "entity/projectile.h"
#include <iostream>

Projectile::Projectile(sf::Vector2f start, sf::Vector2f muzzle)
{
    this->position = start;
    this->impulse = muzzle;
    circle = sf::CircleShape(2, 4);
    circle.setOrigin(1,1);
    circle.setFillColor(sf::Color::Yellow);
    lastUpdate=0;
}

Projectile::~Projectile()
{
    //dtor
}

void Projectile::draw(sf::RenderTarget* target)
{
    circle.setPosition(position);
    target->draw(circle);
}

void Projectile::update(float deltaT)
{

    position.x = position.x + (impulse.x * deltaT);
    position.y = position.y + (impulse.y * deltaT);

}
