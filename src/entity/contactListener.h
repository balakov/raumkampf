#pragma once
#include "Box2D/Box2D.h"
#include "entity/physicalEntity.h"
#include "ship/servership.h"
#include <iostream>
#include <boost/log/trivial.hpp>

class ContactListener : public b2ContactListener
  {
    void BeginContact(b2Contact* contact) {


	  RakNetGUID ownerA, ownerB;
	  RakString idA, idB;
	  b2Vec2 lin = contact->GetFixtureB()->GetBody()->GetLinearVelocity()-contact->GetFixtureA()->GetBody()->GetLinearVelocity();
	  float linearSpeed = lin.Length();

	  /*prepare data */
      void* bodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
      if ( bodyUserData )
	  {
		  PhysicalEntity* e =  static_cast<PhysicalEntity*>( bodyUserData );
		  idA = e->getIdentifier();
		  ownerA = e->owner;
	  }
      bodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();
      if ( bodyUserData )
	  {
		  PhysicalEntity* e =  static_cast<PhysicalEntity*>( bodyUserData );
		  idB = e->getIdentifier();
		  ownerB = e->owner;
	  }
	  std::cout << "\nidA: " << idA << ", owner: " << ownerA.ToString() <<  "idB: " << idA << ", owner: " << ownerB.ToString();
	  /* Case something hits map */
	  if(ownerA == UNASSIGNED_RAKNET_GUID || ownerB == UNASSIGNED_RAKNET_GUID)
	  {
		  if(idA == "Ship" || idB =="Ship")
		  {
			  if(idA == "Ship")
			  {
				ServerShip* ship =  static_cast<ServerShip*>(contact->GetFixtureA()->GetBody()->GetUserData());;
				ship->handleCollision(linearSpeed,0,ownerB);

			  }
			  else {
				  ServerShip* ship =  static_cast<ServerShip*>(contact->GetFixtureB()->GetBody()->GetUserData());;
				  ship->handleCollision(linearSpeed,0, ownerA);
			  }
		  }
		  if(idA == "Projectile" || idB =="Projectile")
			{
				void* bodyUserData;
				if(idA == "Projectile")
					bodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
				else if (idB == "Projectile")
					bodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();

				PhysicalEntity* e =  static_cast<PhysicalEntity*>( bodyUserData );
				e->markForDeletion = true;
				std::cout << "\nBOINK (projectile hits terrain)";
				
		  }
		  return;
	  }

	  /* Case Projectile hits own ship */
	  if(ownerA==ownerB)
	  {
		  if(idA == "Ship" && idB == "Projectile")
			  std::cout << "Projectile hit own ship (Case A)";
		  else if(idB == "Ship" && idA == "Projectile")
		      std::cout << "Projectile hit own ship (Case B)";
		  else std::cout << "Same owner, but not frome same ship.. strange things going on";
		  
	  }

	  /*case Projectile hits another ship */
	  else if(ownerA!=ownerB)
	  {

			  std::cout << "\nownerA != owner b\n";
		  if(idA == "Ship" && idB == "Projectile")
		  {

			  ServerShip* ship =  static_cast<ServerShip*>(contact->GetFixtureA()->GetBody()->GetUserData());
			  PhysicalEntity* e =  static_cast<PhysicalEntity*>(contact->GetFixtureB()->GetBody()->GetUserData());

			  ship->handleCollision(linearSpeed,0, ownerB);
		  }
		  else if(idB == "Ship" && idA == "Projectile")
		  {
			  ServerShip* ship =  static_cast<ServerShip*>(contact->GetFixtureB()->GetBody()->GetUserData());
			  PhysicalEntity* e =  static_cast<PhysicalEntity*>(contact->GetFixtureA()->GetBody()->GetUserData());

		      ship->handleCollision(linearSpeed,0, ownerA);
		  }
		  else std::cout << "Different owners, but not ship<->projectile. strange things going on";
		  
	  }

    }
  
    void EndContact(b2Contact* contact) {

      void* bodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
      if ( bodyUserData )
		  static_cast<PhysicalEntity*>( bodyUserData )->getIdentifier();
  

      bodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();
      if ( bodyUserData )
		  static_cast<PhysicalEntity*>( bodyUserData )->getIdentifier();
  
    }


	void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse)
	{
		int numPoints = contact->GetManifold()->pointCount;
		b2WorldManifold collisionPoint;
		contact->GetWorldManifold(&collisionPoint);
		if (numPoints > 0)
		{
			
			b2Vec2 p2 = collisionPoint.points[1];
			for (int i = 0; i < numPoints; i++)
			{
				if (impulse->normalImpulses[i] > 50)
				{

					b2Vec2 p = collisionPoint.points[i];
					std::cout << "collision at point: " << p.x * 10 << "|" << p.y * 10;

				}

			}
		}
	}
  };