#include "profile.h"
#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>         // streaming operators etc.
#include "tinyxml/tinyxml2.h"
#include <boost/log/trivial.hpp>
#include "settings.h"


Profile::Profile(std::string profileFolder)
{
	this->pathToThisProfile = profileFolder;
	reload();
}
Profile::~Profile()
{

}
std::string Profile::getName()
{
	return this->profileName;
}
std::string Profile::getRank()
{
	return this->rank;
}
std::string Profile::getPath()
{
	return this->pathToThisProfile;
}
bool Profile::reload()
{
	BOOST_LOG_TRIVIAL(info) << "Loading Profile ";
	tinyxml2::XMLDocument* profile = new tinyxml2::XMLDocument();
	std::string completepath = Settings::getDataFolderBasePath().append("save/").append(this->pathToThisProfile + "/general.xml");
	profile->LoadFile(completepath.c_str());
	if(profile->Error())
	{
		BOOST_LOG_TRIVIAL(error) << "Could not open " << completepath << "Error: " << profile->GetErrorStr1();
		return false;
	}

    tinyxml2::XMLElement* root;
    root = profile->FirstChildElement("profile");

    if(!root)
    {
        BOOST_LOG_TRIVIAL(error) << "Not a profile file ";
        return false;
    }
	tinyxml2::XMLElement* temp = root->FirstChildElement("name");
	this->profileName = temp->GetText();
	temp = temp->NextSiblingElement("rank");
	this->rank = temp->GetText();
	return true;
}