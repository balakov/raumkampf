#include "clientuser.h"	
#include <iostream>
#include "container\clientgame.h"


	void ClientUser::setShip(ClientShip* ship)
	{
		this->ship = ship;
		hasShip=true;
	}

	/*/ Replica Implementations /*/
	RakNet::RakString ClientUser::getIdentifier(void) const {return "User";}

	void ClientUser::WriteAllocationID(RakNet::Connection_RM3 *destinationConnection, RakNet::BitStream *allocationIdBitstream) const {
		allocationIdBitstream->Write(getIdentifier());
	}
	RM3ConstructionState ClientUser::QueryConstruction(RakNet::Connection_RM3 *destinationConnection, ReplicaManager3 *replicaManager3) {

		return QueryConstruction_ServerConstruction(destinationConnection,false);
	}
	bool ClientUser::QueryRemoteConstruction(RakNet::Connection_RM3 *sourceConnection) {
		return QueryRemoteConstruction_ServerConstruction(sourceConnection,false);
	}
	RM3QuerySerializationResult ClientUser::QuerySerialization(RakNet::Connection_RM3 *destinationConnection) {
		return QuerySerialization_ServerSerializable(destinationConnection,false);
	}
	RM3ActionOnPopConnection ClientUser::QueryActionOnPopConnection(RakNet::Connection_RM3 *droppedConnection) const {
		return QueryActionOnPopConnection_Server(droppedConnection);
	}
	void ClientUser::SerializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *destinationConnection)
	{
		constructionBitstream->Write(_name);
		constructionBitstream->Write(guid);
		constructionBitstream->Write(gameSlot);
		constructionBitstream->Write(isReady);
	}
	bool ClientUser::DeserializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *sourceConnection)
	{
		BOOST_LOG_TRIVIAL(info) << "Client User Deserialize";	
		constructionBitstream->Read(_name);
		constructionBitstream->Read(guid);
		constructionBitstream->Read(gameSlot);
		constructionBitstream->Read(isReady);
		print();
		ClientGame::getInstance()->addUser(this);
		return true;
	}
	
	RM3SerializationResult ClientUser::Serialize(SerializeParameters *serializeParameters)	{
		return RM3SR_NEVER_SERIALIZE_FOR_THIS_CONNECTION ;
	}
	void ClientUser::Deserialize(RakNet::DeserializeParameters *deserializeParameters) {
		deserializeParameters->serializationBitstream->Read(kills);
		deserializeParameters->serializationBitstream->Read(crashed);
		deserializeParameters->serializationBitstream->Read(shotdown);
		deserializeParameters->serializationBitstream->Read(isReady);
		deserializeParameters->serializationBitstream->Read(gameSlot);

	}

	void ClientUser::SerializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *destinationConnection){}
	 bool ClientUser::DeserializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *sourceConnection){return true;}
	void ClientUser::DeallocReplica(RakNet::Connection_RM3 *sourceConnection) {
		delete this;
	}