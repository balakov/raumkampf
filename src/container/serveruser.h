#pragma once
#include "RakPeerInterface.h"
#include "network/networkEntity.h"
#include "ship/servership.h"
#include "user.h"
#include "raknet/RakNetTypes.h"
class ServerUser : public User, public NetworkEntity
{
public:
	ServerUser(RakNet::RakNetGUID guid){
		this->guid = guid;
	}
	ServerShip* ship;

		/*/ Replica Implementations /*/
	virtual RakNet::RakString getIdentifier(void) const {return "User";}

	void WriteAllocationID(RakNet::Connection_RM3 *destinationConnection, RakNet::BitStream *allocationIdBitstream) const {
		allocationIdBitstream->Write(getIdentifier());
	}
	virtual RM3ConstructionState QueryConstruction(RakNet::Connection_RM3 *destinationConnection, ReplicaManager3 *replicaManager3) {

		return QueryConstruction_ServerConstruction(destinationConnection,true);
	}
	virtual bool QueryRemoteConstruction(RakNet::Connection_RM3 *sourceConnection) {
		return QueryRemoteConstruction_ServerConstruction(sourceConnection,true);
	}
	virtual RM3QuerySerializationResult QuerySerialization(RakNet::Connection_RM3 *destinationConnection) {
		return QuerySerialization_ServerSerializable(destinationConnection,true);
	}
	virtual RM3ActionOnPopConnection QueryActionOnPopConnection(RakNet::Connection_RM3 *droppedConnection) const {
		return QueryActionOnPopConnection_Server(droppedConnection);
	}
	virtual void SerializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *destinationConnection)
	{
		constructionBitstream->Write(_name);
		constructionBitstream->Write(guid);
		constructionBitstream->Write(gameSlot);
		constructionBitstream->Write(isReady);
		this->print();
	}
	virtual bool DeserializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *sourceConnection)
	{
		BOOST_LOG_TRIVIAL(info) << "Deserializing User Construction?";


		return true;
	}
	
	RM3SerializationResult Serialize(SerializeParameters *serializeParameters)	{
		serializeParameters->outputBitstream->Write(kills);
		serializeParameters->outputBitstream->Write(crashed);
		serializeParameters->outputBitstream->Write(shotdown);
		serializeParameters->outputBitstream->Write(isReady);
		serializeParameters->outputBitstream->Write(gameSlot);
		return RM3SR_BROADCAST_IDENTICALLY;
	}
	void Deserialize(RakNet::DeserializeParameters *deserializeParameters) {
								BOOST_LOG_TRIVIAL(info) << "Deserializing User? ";

	}

	virtual void SerializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *destinationConnection){}
	virtual bool DeserializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *sourceConnection){return true;}
	void DeallocReplica(RakNet::Connection_RM3 *sourceConnection) {
		delete this;
	}

};