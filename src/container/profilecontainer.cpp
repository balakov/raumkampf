#include "profilecontainer.h"
#include "tinyxml/tinyxml2.h"
#include <boost/log/trivial.hpp>
#include "settings.h"

ProfileContainer* ProfileContainer::instance = 0;

ProfileContainer::ProfileContainer()
{
	active=0;
	reload();
}
std::vector<Profile*> ProfileContainer::listProfiles()
{
	return profiles;
}
bool ProfileContainer::setActiveProfile(int index)
{
	return false;
}
Profile* ProfileContainer::getActiveProfile()
{
	return profiles[active];
}
bool ProfileContainer::saveProfile()
{
	return false;
}

bool ProfileContainer::reload()
{
	BOOST_LOG_TRIVIAL(info) << "Loading available Profiles ";
	tinyxml2::XMLDocument* profileList = new tinyxml2::XMLDocument();
	profileList->LoadFile(Settings::getDataFolderBasePath().append("profiles.xml").c_str());
	if(profileList->Error())
	{
		BOOST_LOG_TRIVIAL(error) << "Could not open " << Settings::getDataFolderBasePath().append("profiles.conf").c_str()<< "Error: " << profileList->GetErrorStr1();
		return false;
	}

    tinyxml2::XMLElement* root;
    root = profileList->FirstChildElement("profiles");

    if(!root)
    {
        BOOST_LOG_TRIVIAL(error) << "Not a profile list file ";
        return false;
    }
	tinyxml2::XMLElement* profile = root->FirstChildElement("path");
	this->profiles.clear();
	while(profile)
	{
		std::string path = profile->GetText();
		BOOST_LOG_TRIVIAL(info) << "Loading profile from "<<path;
		Profile* p = new Profile(path);
		BOOST_LOG_TRIVIAL(info) << "Test: " << p->getName() << " " << p->getRank();
		profiles.push_back(p);
		profile=profile->NextSiblingElement("path");
	}

}

ProfileContainer* ProfileContainer::getInstance()
{
	if(instance==0)
		instance = new ProfileContainer();
	return instance;
}

