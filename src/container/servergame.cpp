#include "servergame.h"

void ServerGame::start()
{
	_hasStarted=true;
	_clock.restart();
}
bool ServerGame::endOfGame()
{
	switch(_endingCondition)
	{
		case SCORE:
			{
				if(getMaxScore()>=_limit)
					return true;
				else return false;
			}
		case TIME: 
		{
			if(_clock.getElapsedTime().asSeconds()>_limit)
				return true;
			else return false;
		}
		default: return false;
	}
}

void ServerGame::pause()
{
	if(!_isPaused && _hasStarted)
	{
		_tmpTime += getDuration();
		_isPaused=true;
		BOOST_LOG_TRIVIAL(trace) << "Game Paused";
	}
	else BOOST_LOG_TRIVIAL(trace) << "Already Paused";
}
void ServerGame::unpause()
{
	if(_isPaused && _hasStarted)
	{
		_clock.restart();
		_isPaused=false;
		BOOST_LOG_TRIVIAL(trace) << "Game Unpaused";
	}
	else BOOST_LOG_TRIVIAL(trace) << "Already Unpaused";
}
sf::Time ServerGame::getDuration()
{
	return _tmpTime + _clock.getElapsedTime();
}
bool ServerGame::join(int slotNumber, RakNet::RakNetGUID guid)
{
	
	// is slot already occupied?
	if(!isEmptySlot(slotNumber))
	{
		BOOST_LOG_TRIVIAL(trace) << "Cannot join, slot is full, " << this->getUserBySlot(slotNumber)->guid.ToString() << " already in slot " << slotNumber;
		return false;
	}


	else if(!hasUser(guid)) // user hasn't joined yet
	{
		BOOST_LOG_TRIVIAL(trace) << "New User joined"; 
		ServerUser* player = new ServerUser(guid);
		player->guid = guid;
		player->gameSlot = slotNumber;
		this->addUser(player);		
		replicaManager->Reference(player);
		return true;
	}
	else { // user wants to switch slot
		BOOST_LOG_TRIVIAL(trace) << "Switching User from slot " << this->getUserByGUID(guid)->gameSlot << " to slot " << slotNumber;
		this->getUserByGUID(guid)->gameSlot = slotNumber;
		return true;
	}
	BOOST_LOG_TRIVIAL(trace) << "Cannot join, unknown error";
	return false;

}

bool ServerGame::remove(ServerUser* user)
{
	if(!hasUser(user->guid))
	{
		BOOST_LOG_TRIVIAL(trace) << "There never was such a user";
		return false;
	}
	else {
		this->removeUser(user->guid);
		this->_currentNumberOfPlayers--;
		return true;
	}

}
bool ServerGame::isEmptySlot(int gameSlot)
{
	for (int i = 0; i < users.size(); i++)
	{
		if (users[i]->gameSlot == gameSlot)
			return false;
	}
	return true;
}


ServerUser* ServerGame::getUserByGUID(RakNet::RakNetGUID guid)
{
	for (int i = 0; i<users.size(); i++)
	{
		if (users[i]->guid == guid)
		{
			return users[i];
		}
	}
	BOOST_LOG_TRIVIAL(error) << "No such user";
	return nullptr;
}

std::vector<ServerUser*>  ServerGame::getUsers()
{
	return users;
}

ServerUser*  ServerGame::getUserBySlot(int gameSlot)
{
	for (int i = 0; i<users.size(); i++)
	{
		if (users[i]->gameSlot == gameSlot)
		{
			return users[i];
		}
	}
	BOOST_LOG_TRIVIAL(error) << "No such user";
	return nullptr;
}

bool ServerGame::hasUser(RakNet::RakNetGUID guid)
{
	for (int i = 0; i<users.size(); i++)
	{
		if (users[i]->guid == guid)
		{
			return true;
		}
	}
	return false;
}
void ServerGame::addUser(ServerUser* newUser)
{
	if (hasUser(newUser->guid))
	{
		BOOST_LOG_TRIVIAL(warning) << "User " << newUser->guid.ToString() << " already in game";
		return;

	}
	BOOST_LOG_TRIVIAL(info) << "New user added with guid" << newUser->guid.ToString();
	users.push_back(newUser);
	_userListmodified = true;
}
void ServerGame::removeUser(RakNet::RakNetGUID guid)
{
	BOOST_LOG_TRIVIAL(error) << "Trying to remove " << guid.ToString() << " from game";
	int pos = -1;
	for (int i = 0; i < users.size(); i++)
	{
		if (users[i]->guid == guid)
			pos = i;
	}
	if (pos >= 0)
	{
		users.erase(users.begin() + pos);
		BOOST_LOG_TRIVIAL(error) << "Deleted User";
	}

}