#pragma once
#include "WindowsIncludes.h"
#include "RakPeerInterface.h"
#include "network/networkEntity.h"
#include "ship/clientship.h"
#include "user.h"
using namespace RakNet;
class ClientUser : public User, public NetworkEntity
{
public:

	ClientShip* ship;

	//ClientUser();
	void setShip(ClientShip* ship);

	virtual RakNet::RakString getIdentifier(void) const;

	void WriteAllocationID(RakNet::Connection_RM3 *destinationConnection, RakNet::BitStream *allocationIdBitstream) const ;
	virtual RM3ConstructionState QueryConstruction(RakNet::Connection_RM3 *destinationConnection, ReplicaManager3 *replicaManager3);
	virtual bool QueryRemoteConstruction(RakNet::Connection_RM3 *sourceConnection);
	virtual RM3QuerySerializationResult QuerySerialization(RakNet::Connection_RM3 *destinationConnection);
	virtual RM3ActionOnPopConnection QueryActionOnPopConnection(RakNet::Connection_RM3 *droppedConnection) const;
	virtual void SerializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *destinationConnection);
	virtual bool DeserializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *sourceConnection);
	
	RM3SerializationResult Serialize(SerializeParameters *serializeParameters);
	void Deserialize(RakNet::DeserializeParameters *deserializeParameters);

	virtual void SerializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *destinationConnection);
	virtual bool DeserializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *sourceConnection);
	void DeallocReplica(RakNet::Connection_RM3 *sourceConnection);
};