#include "util.h"

#define SCALE 10.0f


void Util::startProcess(std::string commandline)
{

	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	LPSTR cmdline = const_cast<char *> (commandline.c_str()); // Fucking horrible

	// Start the child process. 
	bool b = CreateProcess(NULL,   // No module name (use command line)
		cmdline,        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		CREATE_NEW_CONSOLE,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi);           // Pointer to PROCESS_INFORMATION structure

}
enum RoundedCorners
{
    BR = 1,
    BL = 2,
    TL = 4,
    TR = 8,
};


sf::Vector2f Util::toSF(b2Vec2 vec)
{
    return sf::Vector2f(vec.x*SCALE, vec.y*SCALE);
}
b2Vec2 Util::toB2(sf::Vector2f vec)
{
    return b2Vec2(vec.x/SCALE, vec.y/SCALE);
}
float Util::degToRad(float angleInDeg)
{
    return 3.14159265359f/180*angleInDeg;
}
float Util::radToDeg(float angleInRad)
{
    return 180/3.14159265359f * angleInRad;
}
sf::ConvexShape Util::getRRect(
    float P1X, float P1Y,
    float P2X, float P2Y,
    float Radius, int Corners,
    const sf::Color& Col,
    float Outline,
    const sf::Color& OutlineCol)
{
    static const int NbSegments = 15;
    static const float PI_HALF = 3.141592654f / 2.0f;
    static const float SegmentRadian = PI_HALF/ (float)NbSegments;

    // Create the shape's points
    sf::ConvexShape S;
    S.setOutlineThickness(Outline);
    S.setOutlineColor(OutlineCol);
    S.setFillColor(Col);

    S.setPointCount(NbSegments*4+1);
    if((Corners&BR) == BR)
    {

        sf::Vector2f Center(P2X - Radius, P2Y - Radius);
        for (unsigned int i = 0; i <= NbSegments; ++i)
        {
            float Angle = (float)i * SegmentRadian;
            sf::Vector2f Offset(cos(Angle), sin(Angle));
            S.setPoint(i, sf::Vector2f(Center + Offset * Radius));
        }

    }
    else
    {
        //S.setPoint(sf::Vector2f(P2X, P2Y));
    }
    if((Corners&BL) == BL)
    {

        sf::Vector2f Center(P1X + Radius, P2Y - Radius);
        for (unsigned int i = 0; i <= NbSegments; ++i)
        {
            float Angle = (float)i * SegmentRadian + PI_HALF;
            sf::Vector2f Offset(cos(Angle), sin(Angle));

            S.setPoint(i+NbSegments, sf::Vector2f(Center + Offset * Radius));
        }

    }
    else
    {
        //S.AddPoint(sf::Vector2f(P1X, P2Y), Col, OutlineCol);
    }
    if((Corners&TL) == TL)
    {
        sf::Vector2f Center(P1X + Radius, P1Y + Radius);
        for (unsigned int i = 0; i <= NbSegments; ++i)
        {
            float Angle = (float)i * SegmentRadian + 2.0f * PI_HALF;
            sf::Vector2f Offset(cos(Angle), sin(Angle));

            S.setPoint(i+ 2*NbSegments, sf::Vector2f(Center + Offset * Radius));
        }
    }
    else
    {
        //S.AddPoint(sf::Vector2f(P1X, P2Y), Col, OutlineCol);
    }
    if((Corners&TR) == TR)
    {
        sf::Vector2f Center(P2X - Radius, P1Y + Radius);
        for (unsigned int i = 0; i <= NbSegments; ++i)
        {
            float Angle = (float)i * SegmentRadian + 3.0f * PI_HALF;
            sf::Vector2f Offset(cos(Angle), sin(Angle));

            S.setPoint(i+ 3*NbSegments, sf::Vector2f(Center + Offset * Radius));
        }
    }
    else
    {
        //S.AddPoint(sf::Vector2f(P2X, P1Y), Col, OutlineCol);
    }



    return S;
}
