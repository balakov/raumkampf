#include "globals.h"
#include "settings.h"

tgui::Gui* Globals::gui;
sf::RenderWindow* Globals::window;

sf::RenderWindow* Globals::getWindow()
{
	if (window == nullptr)
	{
		window = new sf::RenderWindow();
		resetWindow();
	}
	return window;
}

tgui::Gui* Globals::getGUI()
{
	if (gui == nullptr)
	{
		gui = new tgui::Gui(*Globals::getWindow());
		gui->setGlobalFont(Settings::getDataFolderBasePath() + "font/Fixedsys500c.ttf");
	}
	return gui;

}

bool Globals::resetWindow()
{
	if (Settings::isFullscreen())
	{
		window->create(sf::VideoMode(Settings::getResolution().x, Settings::getResolution().y), "Client Test", sf::Style::Fullscreen);
	}
	else {
		window->create(sf::VideoMode(Settings::getResolution().x, Settings::getResolution().y), "Client Test");
		window->setVerticalSyncEnabled(Settings::isVSync());
		window->setFramerateLimit(Settings::getFramerateLimit());
		//std::cout << "vsync: " << Settings::isVSync() << "limit: " << Settings::getFramerateLimit();
	}
	return true;
}