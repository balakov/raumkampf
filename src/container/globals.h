#pragma once
#include "SFML\Graphics.hpp"
#include "TGUI\TGUI.hpp"

class Globals
{
public:
    Globals();
	static const int RG_FORCE_START = 200;
	static sf::RenderWindow* getWindow();
	static bool resetWindow();
	static tgui::Gui* getGUI();
private:
	static tgui::Gui* gui;
	static sf::RenderWindow* window;
};
