#pragma once
#include "profile.h"
#include <vector>

class ProfileContainer{

public:

static ProfileContainer* getInstance();
bool reload();
std::vector<Profile*> listProfiles();
bool setActiveProfile(int index);
Profile* getActiveProfile();
bool saveProfile();


private:
	static ProfileContainer* instance;
	ProfileContainer();
	~ProfileContainer();
	std::vector<Profile*> profiles;
	int active;
};