#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include "Box2D/Box2D.h"

class Util
{

public:

	static void startProcess(std::string cmdline);
    static sf::Vector2f toSF(b2Vec2 vec);
    static b2Vec2 toB2(sf::Vector2f vec);
    static float degToRad(float angleInDeg);
    static float radToDeg(float angleInRad);

    static sf::ConvexShape getRRect(float P1X, float P1Y,
                                    float P2X, float P2Y,
                                    float Radius, int Corners,
                                    const sf::Color& Col,
                                    float Outline = 1.f,
                                    const sf::Color& OutlineCol = sf::Color::White);

};
