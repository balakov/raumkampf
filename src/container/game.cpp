#include "game.h"	

Game::Game()
{
	this->gameName ="unnamed game";
	this->mapName = "undefined";
	this->_hasMap = false;
	this->numberOfPlayerSlots = 2;
	this->_currentNumberOfPlayers = 0;
	_allowLateJoin = false;
	_userListmodified = true;
	_mode=DOGFIGHT;
	_endingCondition=TIME;
	_limit=10;
}
void Game::print()
{
	BOOST_LOG_TRIVIAL(trace)  << "maxPlayers: " << this->numberOfPlayerSlots  << " gane name: " << this->gameName << " map name: " << this->mapName << " Number of Users in list ";

}

bool Game::setMap(std::string mapIdentifier)
{
	this->mapName = mapIdentifier.c_str();
	return true;

}
int Game::getNumberOfPlayerSlots()
{
		return this->numberOfPlayerSlots;
}


	bool Game::pollUserListMofification()
	{
		if(_userListmodified)
		{
			_userListmodified=false;
			return true;
		}
		else return false;
	}

	bool Game::pollMapModification()
	{
		return false;
	
	}