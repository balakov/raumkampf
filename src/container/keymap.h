#pragma once
#include <string>
#include <vector>
#include <SFML/Graphics.hpp>
#include <map>
#include <set>
class KeyMap
{
public:
    KeyMap(std::string pathname);		// binding file
	std::vector<bool> getSlotStates();
	bool hasKeyStateChanged();				// whether input state has changed since last call
	void addMapping(int slot, std::string key);
	void removeMapping(int slot, std::string key);
	std::vector<int> getActivatedSlotsFromKeyString(std::string key);
	bool write();
	void clear();
	void print();
protected:
private:
	std::string keyFilePathAndName;
	std::vector<bool> states;
	std::map<sf::Keyboard::Key, std::vector<int> > keyToSlot;
	bool modified;
};
