#pragma once
#include <iostream>
#include <vector>
class Profile
{
public:
	Profile(std::string pathname);
	~Profile();
	bool reload();
	std::string getName();
	std::string getRank();
	std::string getPath();
private:
	std::string profileName;
	std::string rank;

	std::string pathToThisProfile;
	std::vector<std::string> createdShips;
};