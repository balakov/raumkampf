#pragma once
#include "RakPeerInterface.h"
#include "network/networkEntity.h"
#include "user.h"
#include <vector>
#include <boost/log/trivial.hpp>
enum MODE{DOGFIGHT,TEAM};
enum LIMIT{SCORE,TIME,NONE};

class Game
{
public:


	int getNumberOfPlayerSlots();


	bool pollUserListMofification();
	bool pollMapModification();

	bool setMap(std::string mapIdentifier);

	RakNet::RakString getMapName(){return mapName;}
	bool hasMap(){return _hasMap;}

	int getMaxScore(){return 10;};
	MODE _mode;
	LIMIT _endingCondition;
	int _limit;
	void print();


protected:

	Game();
	RakNet::RakString gameName;
	RakNet::RakString mapName;
	unsigned int numberOfPlayerSlots;
	

	unsigned int _currentNumberOfPlayers;
	bool _userListmodified;
	bool _allowLateJoin;
	bool _hasStarted;
	bool _isPaused;
	bool _hasMap;
private:
};

