#pragma once
#include "WindowsIncludes.h"
#include "RakPeerInterface.h"
#include "network/networkEntity.h"

#include "clientuser.h"
#include "game.h"
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include "map/map.h"
class ClientGame: public Game, public NetworkEntity
{
public:
	static ClientGame* instance;
	static ClientGame* getInstance(){ if (instance == nullptr) instance = new ClientGame();  return instance; }
	static void reset() {
		BOOST_LOG_TRIVIAL(info) << "resetting game ";
		//delete instance;
		instance = nullptr;
		BOOST_LOG_TRIVIAL(info) << "done";
	}
	std::vector<ClientUser*> getUsers();

	void draw(sf::RenderTarget * target);
	void print()
	{
		BOOST_LOG_TRIVIAL(trace) << "maxPlayers: " << this->numberOfPlayerSlots << " game name: " << this->gameName << " map name: " << this->mapName << " Number of Users in list " << this->clientUsers.size();

	}

	ClientUser* getUserByGUID(RakNet::RakNetGUID guid);
	ClientUser* getUserBySlot(int gameslot);
	bool isEmptySlot(int gameSlot);
	void addUser(ClientUser* user);
	bool hasUser(ClientUser* user);
		/*/ Replica Implementations /*/
	virtual RakNet::RakString getIdentifier(void) const;

	void WriteAllocationID(RakNet::Connection_RM3 *destinationConnection, RakNet::BitStream *allocationIdBitstream) const;
	
	virtual RM3ConstructionState QueryConstruction(RakNet::Connection_RM3 *destinationConnection, ReplicaManager3 *replicaManager3) ;
	virtual bool QueryRemoteConstruction(RakNet::Connection_RM3 *sourceConnection);
	virtual RM3QuerySerializationResult QuerySerialization(RakNet::Connection_RM3 *destinationConnection);
	virtual RM3ActionOnPopConnection QueryActionOnPopConnection(RakNet::Connection_RM3 *droppedConnection) const;
	virtual void SerializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *destinationConnection);
	virtual bool DeserializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *sourceConnection);
	
	RM3SerializationResult Serialize(SerializeParameters *serializeParameters);
	void Deserialize(RakNet::DeserializeParameters *deserializeParameters);
	virtual void SerializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *destinationConnection);
	virtual bool DeserializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *sourceConnection);
	void DeallocReplica(RakNet::Connection_RM3 *sourceConnection);

private:
	std::vector<ClientUser*> clientUsers;
	bool loadMap();
};