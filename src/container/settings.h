#ifndef SETTINGS_H
#define SETTINGS_H
#include <SFML/Graphics.hpp>

class Settings
{
public:
    Settings();
    static bool load(std::string filename);
	static void write();
    static void restore();
    static sf::Vector2i getResolution();
    static bool setResolution(sf::Vector2i resolution);
    static int getMusicVolume();
    static int getSoundVolume();
    static void setSoundVolume(int vol);
    static void setMusicVolume(int vol);
    static void enableVSync(bool state);
    static bool isVSync();
    static void setFramerateLimit(int limit);
    static int getFramerateLimit();
    static void enableFullscreen(bool state);
    static bool isFullscreen();
	static float getPhysicsStep();
	static void setPhysicsStep(float stepTime);

	static std::string getDataFolderBasePath() {return "../../data/";}
	static std::string getHullPath() {return getDataFolderBasePath().append("ship/hull/");}
	static std::string getHullImagePath() {return getDataFolderBasePath().append("img/hulls/");}
	static std::string getDevicePath() {return getDataFolderBasePath().append("ship/device/");}
	static std::string getShipPath() {return getDataFolderBasePath().append("ship/");}
	static int getGuiTextSize() { return 15; };
	static std::vector<sf::Keyboard::Key> getAssignableKeys();
	static std::string getDescriptionFromKeyCode(sf::Keyboard::Key);	//translates KeyCodes into string descriptions
	static sf::Keyboard::Key getKeyFromDescription(std::string);	//translates "r_up", "l_down" et al into sf-keycodes
protected:
private:
    static int frameLimit;
    static int musicVolume;
    static int soundVolume;
    static bool isVS;
    static sf::Vector2i resolution;
    static bool isFS;
	static float step;
	static int speedInHertz;
	static std::string interpolation;
};

#endif // SETTINGS_H
