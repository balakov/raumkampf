#include "clientgame.h"
#include "clientuser.h"
#include "globals.h"
#include "settings.h"

ClientGame* ClientGame::instance;

	std::vector<ClientUser*>  ClientGame::getUsers()
	{
		return clientUsers;
	}

	ClientUser* ClientGame::getUserBySlot(int gameslot)
	{
		for (int i = 0; i < clientUsers.size(); i++)
		{
			if (clientUsers[i]->gameSlot == gameslot)
				return clientUsers[i];
		}
		return 0;
	}

	ClientUser* ClientGame::getUserByGUID(RakNet::RakNetGUID guid)
	{
		for (int i = 0; i < clientUsers.size(); i++)
		{
			if (clientUsers[i]->guid == guid)
				return clientUsers[i];
		}
		return 0;


	}
	bool ClientGame::isEmptySlot(int gameSlot)
	{
		for (int i = 0; i < clientUsers.size(); i++)
		{
			if (clientUsers[i]->gameSlot == gameSlot)
				return false;
		}
		return true;
	}
	void ClientGame::addUser(ClientUser* user)
	{
		if (!hasUser(user))
			clientUsers.push_back(user);

	}
	bool ClientGame::hasUser(ClientUser* user)
	{
		for (int i = 0; i < clientUsers.size(); i++)
		{
			if (clientUsers[i]->guid == user->guid)
				return true;
		}
		return false;
	}
	void ClientGame::draw(sf::RenderTarget * target)
	{


	}

	bool ClientGame::loadMap()
	{

		return true;
	}



	RakNet::RakString ClientGame::getIdentifier(void)const{return "Game";}

	void ClientGame::WriteAllocationID(RakNet::Connection_RM3 *destinationConnection, RakNet::BitStream *allocationIdBitstream) const {
		allocationIdBitstream->Write("Game");
	}
	RM3ConstructionState ClientGame::QueryConstruction(RakNet::Connection_RM3 *destinationConnection, ReplicaManager3 *replicaManager3) {

		return QueryConstruction_ServerConstruction(destinationConnection,false);
	}
	bool ClientGame::QueryRemoteConstruction(RakNet::Connection_RM3 *sourceConnection) {
		return QueryRemoteConstruction_ServerConstruction(sourceConnection,false);
	}
	RM3QuerySerializationResult ClientGame::QuerySerialization(RakNet::Connection_RM3 *destinationConnection) {
		return QuerySerialization_ServerSerializable(destinationConnection,false);
	}
	RM3ActionOnPopConnection ClientGame::QueryActionOnPopConnection(RakNet::Connection_RM3 *droppedConnection) const {
		return QueryActionOnPopConnection_Server(droppedConnection);
	}
	void ClientGame::SerializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *destinationConnection)
	{
		constructionBitstream->Write(this->gameName);
	}
	bool ClientGame::DeserializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *sourceConnection)
	{
		BOOST_LOG_TRIVIAL(info) << "Received Game, deserializing..";
		constructionBitstream->Read(this->gameName);
		RakNet::RakString mapName;
		constructionBitstream->Read(mapName);
		if(mapName == "undefined")
			this->_hasMap = false;
		else this->_hasMap = true;
		constructionBitstream->Read(this->numberOfPlayerSlots);
		BOOST_LOG_TRIVIAL(info) << "max players: " << this->getNumberOfPlayerSlots();
		constructionBitstream->Read(_currentNumberOfPlayers);
			BOOST_LOG_TRIVIAL(info) << "curr num players: " << this->_currentNumberOfPlayers;	
		for(int i=0; i < _currentNumberOfPlayers; i++)
		{
			RakNet::RakNetGUID temp;
			constructionBitstream->Read(temp);
		}

		constructionBitstream->Read(_mode);
		constructionBitstream->Read(_endingCondition);
		BOOST_LOG_TRIVIAL(info) << "Client Game Construction Complete";	
		this->print();
		return true;
	}
	
	RM3SerializationResult ClientGame::Serialize(SerializeParameters *serializeParameters)	{
		return RM3SR_NEVER_SERIALIZE_FOR_THIS_CONNECTION ;
	}
	void ClientGame::Deserialize(RakNet::DeserializeParameters *deserializeParameters) {
		BOOST_LOG_TRIVIAL(info) << "Client Game Deserialize";	
		deserializeParameters->serializationBitstream->Read(this->gameName);
				RakNet::RakString mapName;

		deserializeParameters->serializationBitstream->Read(this->mapName);
		if(mapName == "undefined")
			this->_hasMap = false;
		else this->_hasMap = true;
		deserializeParameters->serializationBitstream->Read(this->numberOfPlayerSlots);
		deserializeParameters->serializationBitstream->Read(_currentNumberOfPlayers);
		for(int i=0; i < _currentNumberOfPlayers; i++)
		{
			RakNet::RakNetGUID temp;
			deserializeParameters->serializationBitstream->Read(temp);
			std::cout << "player #" << i << " : " << temp.ToString();
		}
		deserializeParameters->serializationBitstream->Read(_mode);
		deserializeParameters->serializationBitstream->Read(_endingCondition);
		BOOST_LOG_TRIVIAL(info) << "Client Game Deserialize Complete";

	}

	void ClientGame::SerializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *destinationConnection){}
	bool ClientGame::DeserializeDestruction(RakNet::BitStream *destructionBitstream, RakNet::Connection_RM3 *sourceConnection){return true;}
	void ClientGame::DeallocReplica(RakNet::Connection_RM3 *sourceConnection) {
		delete this;
	}