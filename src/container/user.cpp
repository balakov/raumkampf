#include "user.h"
#include <boost/log/trivial.hpp>


User::User()
{
	hasShip=false; 
	isReady = false;
	kills=0;
	crashed=0;
	shotdown=0;
	_name="unknown user";
	gameSlot = -100;

}

void User::print()
{
	BOOST_LOG_TRIVIAL(info) <<"\nname: " << _name << "\nguid: " << guid.ToString() << "\nkills: "<<kills<<" crashed: " << crashed<<" shotdown: "<< shotdown << "gameslot: " << gameSlot;
}