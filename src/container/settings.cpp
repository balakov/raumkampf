#include "settings.h"
#include "tinyxml/tinyxml2.h"
#include <iostream>
#include <boost/log/trivial.hpp>

bool Settings::isVS = false;
int Settings::frameLimit = 120;
int Settings::musicVolume = 50;
int Settings::soundVolume = 50;
sf::Vector2i Settings::resolution=sf::Vector2i(1024,768);
bool Settings::isFS = false;
float Settings::step = 0.16;
int Settings::speedInHertz =60;
std::string Settings::interpolation = "none";
Settings::Settings()
{

}
bool Settings::load(std::string filename)
{
   	BOOST_LOG_TRIVIAL(trace) << "Loading config from " << filename;
    tinyxml2::XMLDocument doc;
    doc.LoadFile(filename.c_str());
    tinyxml2::XMLElement* root = doc.FirstChildElement("config");
    tinyxml2::XMLElement* general = root->FirstChildElement("general");
    tinyxml2::XMLElement* video= general->NextSiblingElement("video");
    tinyxml2::XMLElement* audio = video->NextSiblingElement("audio");


	// general
    tinyxml2::XMLElement* tmp = general->FirstChildElement("speed");
    speedInHertz = tmp->IntAttribute("hertz");
    BOOST_LOG_TRIVIAL(trace) << "Simulation Speed:  " << speedInHertz;
	tmp = tmp->NextSiblingElement("interpolation");
	interpolation = tmp->GetText();
    BOOST_LOG_TRIVIAL(trace) << "Interpolation method " << interpolation;


	// video
    tmp = video->FirstChildElement("vsync");
    std::string temp = tmp->GetText();

    if(temp.compare("true")==0)
        isVS = true;
    else isVS = false;

    BOOST_LOG_TRIVIAL(trace) << "Vsync:  " << isVS;
    tmp = tmp->NextSiblingElement("framelimit");
    frameLimit = tmp->IntAttribute("limit");
    BOOST_LOG_TRIVIAL(trace) << "Target fps: " << frameLimit;
    tmp = tmp->NextSiblingElement("fullscreen");
    temp = tmp->GetText();

    if(temp.compare("true")==0)
    {
        isFS = true;
        BOOST_LOG_TRIVIAL(trace) << "Fullscreen: enabled";
    }
    else
    {
        isFS = false;
        BOOST_LOG_TRIVIAL(trace) << "Fullscreen: disabled";
    }

    tmp = tmp->NextSiblingElement("resolution");
    resolution.x = tmp->IntAttribute("x");
    resolution.y = tmp->IntAttribute("y");
    BOOST_LOG_TRIVIAL(trace) << "Resolution: " << resolution.x << ":" << resolution.y;

/// AUDIO
    tmp = audio->FirstChildElement("sound");
    soundVolume = tmp->IntAttribute("volume");
    tmp = tmp->NextSiblingElement("music");
    musicVolume = tmp->IntAttribute("volume");
    BOOST_LOG_TRIVIAL(trace) << "Snd volume: " << soundVolume << " Msc Volume: " << musicVolume;
    BOOST_LOG_TRIVIAL(trace) << "Config finished";
    return true;

}
void Settings::write()
{
  tinyxml2::XMLDocument doc;
  std::string fl = (Settings::getDataFolderBasePath()+"config.xml").c_str();
  BOOST_LOG_TRIVIAL(info) << "writing Settings to " << fl;
  if (doc.LoadFile(fl.c_str()) == tinyxml2::XML_SUCCESS)
   {
	   doc.Clear();
      tinyxml2::XMLNode *root = doc.NewElement("config");
	  doc.InsertFirstChild(root);

      tinyxml2::XMLElement *general = doc.NewElement("general");
		tinyxml2::XMLElement *speed = doc.NewElement("speed");
		speed->SetAttribute("hertz",speedInHertz);

		general->InsertFirstChild(speed);
		tinyxml2::XMLElement *interpol = doc.NewElement("interpolation");
		tinyxml2::XMLText* method = doc.NewText(interpolation.c_str());
		interpol->InsertEndChild(method);

		general->InsertEndChild(interpol);


	  tinyxml2::XMLElement *video = doc.NewElement("video");
		tinyxml2::XMLElement *vsync = doc.NewElement("vsync");
		tinyxml2::XMLText* vs;
		if(Settings::isVSync())vs = doc.NewText("true");
		else vs = doc.NewText("false");
		vsync->InsertEndChild(vs);
		video->InsertFirstChild(vsync);

		tinyxml2::XMLElement *frl = doc.NewElement("framelimit");
		frl->SetAttribute("limit",Settings::getFramerateLimit());
		video->InsertEndChild(frl);
		
		tinyxml2::XMLText* isf;
		tinyxml2::XMLElement *fs = doc.NewElement("fullscreen");
		if(Settings::isFullscreen())isf = doc.NewText("true");
		else isf = doc.NewText("false");
		fs->InsertEndChild(isf);
		video->InsertEndChild(fs);
		tinyxml2::XMLElement *res = doc.NewElement("resolution");
		res->SetAttribute("x",Settings::getResolution().x);
		res->SetAttribute("y",Settings::getResolution().y);
		video->InsertEndChild(res);

	  tinyxml2::XMLElement *audio = doc.NewElement("audio");
	  	tinyxml2::XMLElement *snd = doc.NewElement("sound");
		snd->SetAttribute("volume",Settings::getSoundVolume());
		audio->InsertEndChild(snd);
	  	tinyxml2::XMLElement *msc = doc.NewElement("music");
		msc->SetAttribute("volume",Settings::getMusicVolume());
		audio->InsertEndChild(msc);

	  root->InsertFirstChild(general);
	  root->InsertAfterChild(general,video);
	  root->InsertEndChild(audio);
	  std::string outfile = Settings::getDataFolderBasePath()+"config.xml";
	  doc.SaveFile(outfile.c_str());
   }
  else 	BOOST_LOG_TRIVIAL(error) << "Could not open config.xml for writing";
  BOOST_LOG_TRIVIAL(info) << "Write complete";
}

void Settings::restore()
{
    load("defaultconfig.xml");
}
sf::Vector2i Settings::getResolution()
{
    return resolution;
}
bool Settings::setResolution(sf::Vector2i resolution)
{
    Settings::resolution = resolution;
	return true;
}

int Settings::getMusicVolume()
{
    return musicVolume;
}

int Settings::getSoundVolume()
{
    return soundVolume;
}

void Settings::setSoundVolume(int vol)
{
	BOOST_LOG_TRIVIAL(trace) << "Sound Volume set to:  " << vol;
	soundVolume = vol;
}

void Settings::setMusicVolume(int vol)
{
	BOOST_LOG_TRIVIAL(trace) << "Music Volume set to:  " << vol;
	musicVolume = vol;
}

void Settings::enableVSync(bool state)
{
	isVS=state;
}

bool Settings::isVSync()
{
    return isVS;
}

void Settings::setFramerateLimit(int limit)
{
	frameLimit=limit;
}

int Settings::getFramerateLimit()
{
    return frameLimit;
}

void Settings::enableFullscreen(bool state)
{
	isFS=state;
}

bool Settings::isFullscreen()
{
    return isFS;
}
float Settings::getPhysicsStep()
{
	return step;
}
void Settings::setPhysicsStep(float stepTime)
{
	step = stepTime;
}
std::vector<sf::Keyboard::Key> Settings::getAssignableKeys()
{
	std::vector<sf::Keyboard::Key> assignableKeys;
	assignableKeys.push_back(sf::Keyboard::Up);
	assignableKeys.push_back(sf::Keyboard::Left);
	assignableKeys.push_back(sf::Keyboard::Down);
	assignableKeys.push_back(sf::Keyboard::Right);

	assignableKeys.push_back(sf::Keyboard::W);
	assignableKeys.push_back(sf::Keyboard::A);
	assignableKeys.push_back(sf::Keyboard::S);
	assignableKeys.push_back(sf::Keyboard::D);

	assignableKeys.push_back(sf::Keyboard::Space);
	assignableKeys.push_back(sf::Keyboard::Return);

	return assignableKeys;
}
sf::Keyboard::Key Settings::getKeyFromDescription(std::string name)
{
	if(!name.compare("r_up"))
		return sf::Keyboard::Up;
	if(!name.compare("r_down"))
		return sf::Keyboard::Down;
	if(!name.compare("r_left"))
		return sf::Keyboard::Left;
	if(!name.compare("r_right"))
		return sf::Keyboard::Right;

	if(!name.compare("l_up"))
		return sf::Keyboard::W;
	if(!name.compare("l_down"))
		return sf::Keyboard::S;
	if(!name.compare("l_left"))
		return sf::Keyboard::A;
	if(!name.compare("l_right"))
		return sf::Keyboard::D;

	if(!name.compare("select"))
		return sf::Keyboard::Space;
	if(!name.compare("start"))
		return sf::Keyboard::Return;

	else return sf::Keyboard::Unknown;
}
std::string Settings::getDescriptionFromKeyCode(sf::Keyboard::Key key)
{
	if(sf::Keyboard::Up==key)
		return "r_up";
	if(sf::Keyboard::Down==key)
		return "r_down";
	if(sf::Keyboard::Left==key)
		return "r_left";
	if(sf::Keyboard::Right==key)
		return "r_right";

	if(sf::Keyboard::W==key)
		return "l_up";
	if(sf::Keyboard::S==key)
		return "l_down";
	if(sf::Keyboard::A==key)
		return "l_left";
	if(sf::Keyboard::D==key)
		return "l_right";

	if(sf::Keyboard::Space==key)
		return "select";
	if(sf::Keyboard::Return==key)
		return "start";

	else return "unknown key";


}