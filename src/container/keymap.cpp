#include "keymap.h"
#include "tinyxml/tinyxml2.h"
#include <iostream>
#include "settings.h"
#include "boost/log/trivial.hpp"

#define NUMSLOTS 32
namespace logging = boost::log;

KeyMap::KeyMap(std::string filename)
{  
	modified = false;
	this->keyFilePathAndName = filename;
	for(int i=0; i < NUMSLOTS; i++)
		states.push_back(false);

	BOOST_LOG_TRIVIAL(info) << "Loading Key binding from "<<filename;
	modified = false;
	tinyxml2::XMLDocument* binding = new tinyxml2::XMLDocument();
	if (binding->LoadFile(filename.c_str()) != tinyxml2::XML_SUCCESS)
		BOOST_LOG_TRIVIAL(error) << "Error loading KeyMap";

	BOOST_LOG_TRIVIAL(info) << "Root element is: "<< binding->FirstChildElement()->Name();
	tinyxml2::XMLElement* node = binding->FirstChildElement("keymap");
	node = node->FirstChildElement();
	while(node)
	{
		std::string keystring = node->Name();
		BOOST_LOG_TRIVIAL(trace) << "Key: "<< keystring;

		tinyxml2::XMLElement* temp = node->FirstChildElement();
		std::vector<int> slots;
		while(temp)
		{				
			slots.push_back(temp->IntAttribute("n"));
			temp = temp->NextSiblingElement();
		} 
		keyToSlot.insert(std::pair<sf::Keyboard::Key, std::vector<int>>(Settings::getKeyFromDescription(keystring),slots));
		slots.clear();
		node = node->NextSiblingElement();
	}

}
std::vector<bool> KeyMap::getSlotStates()
{
	modified = false;
	return states;
}

bool KeyMap::hasKeyStateChanged()
{
	modified = false;
	std::vector<bool> currentState;
	for (int i = 0; i < NUMSLOTS; i++)
		currentState.push_back(false);

	for (std::map<sf::Keyboard::Key, std::vector<int>>::iterator it = keyToSlot.begin(); it != keyToSlot.end(); ++it)
	{
		if (sf::Keyboard::isKeyPressed(it->first))
		{
			for (int idx = 0; idx < it->second.size(); idx++)
			{
				currentState[it->second[idx]] = true;
			}
		}
	}
	for (int i = 0; i < NUMSLOTS; i++)
	{
		if (currentState[i] != states[i])
			modified = true;
	}

	states = currentState;
	return modified;
}

void KeyMap::addMapping(int slot, std::string key)
{ 
	sf::Keyboard::Key k = Settings::getKeyFromDescription(key);
	std::map<sf::Keyboard::Key,std::vector<int> >::iterator it = keyToSlot.find(k);
	if(it==keyToSlot.end())		
	{
		std::vector<int> as;
		as.push_back(slot);
		keyToSlot.insert(std::pair<sf::Keyboard::Key, std::vector<int>>(k,as));
	}
	else
	{
		if (std::find(it->second.begin(), it->second.end(), slot) != it->second.end()) {
			BOOST_LOG_TRIVIAL(warning) << "Mapping from " << key << "to slot " << slot << "already exists";
		}
		else {
			it->second.push_back(slot);
			BOOST_LOG_TRIVIAL(info) << "Mapping  " << key << "to slot " << slot;
		}
		
	}
}
void KeyMap::removeMapping(int slot, std::string key)
{
	BOOST_LOG_TRIVIAL(info) << "trying to remove " << key << " from slot " << slot;
	sf::Keyboard::Key k = Settings::getKeyFromDescription(key);
	std::map<sf::Keyboard::Key, std::vector<int> >::iterator it = keyToSlot.find(k);
	if (it == keyToSlot.end())
	{
		BOOST_LOG_TRIVIAL(warning) << key << "was never mapped to slot " << slot;
	}
	else
	{
		BOOST_LOG_TRIVIAL(info) << "Removing " << it->second.size();
		for (int i = 0; i < it->second.size(); i++)
		{
			if (it->second[i] == slot)
			{
				BOOST_LOG_TRIVIAL(info) << "Removing mapping from slot "<<slot<< " at pos " << i;
				it->second.erase(it->second.begin() + i);
				return;
			}
		}
	}
}
std::vector<int> KeyMap::getActivatedSlotsFromKeyString(std::string key)
{
	std::vector<int> ret;
	sf::Keyboard::Key k = Settings::getKeyFromDescription(key);
	std::map<sf::Keyboard::Key,std::vector<int> >::iterator it = keyToSlot.find(k);
	if(it==keyToSlot.end())
	return ret;
	else return it->second;

}
bool KeyMap::write()
{
	tinyxml2::XMLDocument binding;	
	//std::string bFile = pathname;
	std::string bFile = this->keyFilePathAndName;
  BOOST_LOG_TRIVIAL(info) << "writing key binding to " << bFile ;
  if (binding.LoadFile(bFile.c_str()) == tinyxml2::XML_SUCCESS)
   {
	   binding.Clear();
	   tinyxml2::XMLText* txt; 
      tinyxml2::XMLNode *root = binding.NewElement("keymap");
	  binding.InsertFirstChild(root);

	 // for(int i = 0; i < Settings::getAssignableKeys().size(); i++)
	 // {
		  //std::string tmp = Settings::getDescriptionFromKeyCode(Settings::getAssignableKeys().at(i));
		  //tinyxml2::XMLElement *name = binding.NewElement(tmp.c_str());
		  	for (std::map<sf::Keyboard::Key, std::vector<int>>::iterator it = keyToSlot.begin(); it != keyToSlot.end(); ++it)
			{
				tinyxml2::XMLElement *key = binding.NewElement(Settings::getDescriptionFromKeyCode(it->first).c_str());
				for(int z=0; z < it->second.size(); z++)
					{
						tinyxml2::XMLElement *slot = binding.NewElement("slot");
						slot->SetAttribute("n",it->second[z]);
						key->InsertEndChild(slot);
				}
				root->InsertEndChild(key);
			}
			binding.SaveFile(bFile.c_str());
		  //root->InsertFirstChild(name);
	//  }
		
  }
  binding.SaveFile(bFile.c_str());
	return true;
}
void KeyMap::clear()
{
	keyToSlot.clear();
}

void KeyMap::print()
{
	std::vector<sf::Keyboard::Key> keys = Settings::getAssignableKeys();
	for(int i=0; i < keys.size(); i++)
	{
		std::vector<int> sl = this->getActivatedSlotsFromKeyString(Settings::getDescriptionFromKeyCode(keys[i]));
		std::cout <<"\n"<< Settings::getDescriptionFromKeyCode(keys[i]) << " activates: ";
		for(int k=0; k<sl.size();k++)
			std::cout << " " <<sl[k];
	}
}